package com.silensoft.outfitter.actions.ad;

import android.support.annotation.NonNull;

import com.google.android.gms.ads.AdView;

public interface BannerAdActions {
    void showAd(@NonNull final AdView adView);
}
