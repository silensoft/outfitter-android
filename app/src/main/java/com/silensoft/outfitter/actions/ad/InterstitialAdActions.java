package com.silensoft.outfitter.actions.ad;

import android.content.Context;

public interface InterstitialAdActions {
    void initialize(Context context);
    void showAd();
}
