package com.silensoft.outfitter.actions.ad.impl;

import android.support.annotation.NonNull;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.silensoft.outfitter.actions.ad.BannerAdActions;

public class GoogleBannerAdActionsImpl implements BannerAdActions {
    @Override
    public void showAd(@NonNull final AdView adView) {
        final AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }
}
