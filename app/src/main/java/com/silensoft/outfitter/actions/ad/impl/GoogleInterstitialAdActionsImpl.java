package com.silensoft.outfitter.actions.ad.impl;

import android.content.Context;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.silensoft.outfitter.actions.ad.InterstitialAdActions;

public class GoogleInterstitialAdActionsImpl implements InterstitialAdActions {
    private static final String ADMOB_APP_ID = "ca-app-pub-3940256099942544~3347511713";

    private InterstitialAd interstitialAd;

    public void initialize(final Context context) {
        MobileAds.initialize(context, ADMOB_APP_ID);
        interstitialAd = new InterstitialAd(context);
        // TODO: RELEASE, update adunitid
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public void showAd() {
        interstitialAd.show();
    }
}
