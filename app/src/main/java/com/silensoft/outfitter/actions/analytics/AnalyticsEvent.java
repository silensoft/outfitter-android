package com.silensoft.outfitter.actions.analytics;

import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

public enum AnalyticsEvent {
    SHOW_NEXT_RANDOM_OUTFIT("Show next random outfit"),
    SHOW_PREVIOUS_OUTFIT("Show previous outfit"),
    ADD_OUTFIT("Add new outfit"),
    EDIT_OUTFIT("Edit outfit"),
    SHARE_OUTFIT(FirebaseAnalytics.Event.SHARE),
    SEARCH_OUTFITS(FirebaseAnalytics.Event.SEARCH),
    REMOVE_OUTFIT("Remove outfit"),
    REMOVE_SELECTED_OUTFITS("Remove selected outfits"),
    CHANGE_SEASON("Change season"),
    CHANGE_OCCASION("Change occasion"),
    SHOW_ALL_OUTFITS("Show all outfits"),
    CHANGE_SEASONS("Change seasons"),
    CHANGE_OCCASIONS("Change occasions"),
    SELECT_ALL_OUTFITS("Select all outfits"),
    INVITE_FRIEND("Invite friend"),
    INVITE_FRIENDS_FAILURE("Invite friend failure/cancel"),
    APP_RATING_PROMOTION("App rating promotion"),
    APP_RATING("App rating"),
    IN_APP_PURCHASE_PROMOTION("In app purchase promotion"),
    DENY_APP_RATING_PROMOTION("App rating promotion denied"),
    DENY_IN_APP_PURCHASE_PROMOTION("In app purchase promotion denied"),
    WEATHER_FORECAST_REQUESTED("Weather forecast requested");

    private final String analyticsEventName;

    AnalyticsEvent(@NonNull final String analyticsEventName_) {
        analyticsEventName = analyticsEventName_;
    }

    @Override
    public String toString() {
        return analyticsEventName;
    }
}
