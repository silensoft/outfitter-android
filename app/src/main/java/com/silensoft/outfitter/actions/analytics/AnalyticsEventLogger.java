package com.silensoft.outfitter.actions.analytics;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;

public interface AnalyticsEventLogger {
    void logAnalyticsEvent(@NonNull final FirebaseAnalytics firebaseAnalytics, @NonNull final AnalyticsEvent analyticsEvent);
    void logAnalyticsEvent(@NonNull final FirebaseAnalytics firebaseAnalytics, @NonNull final AnalyticsEvent analyticsEvent, @NonNull final String value);
    void logAnalyticsEvent(@NonNull final FirebaseAnalytics firebaseAnalytics, @NonNull final AnalyticsEvent analyticsEvent, final Bundle bundle);
}
