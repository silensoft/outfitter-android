package com.silensoft.outfitter.actions.analytics;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;

public class FirebaseAnalyticsEventLoggerImpl implements AnalyticsEventLogger {
    @Override
    public void logAnalyticsEvent(@NonNull final FirebaseAnalytics firebaseAnalytics, @NonNull final AnalyticsEvent analyticsEvent) {
        firebaseAnalytics.logEvent(analyticsEvent.toString(), null);
    }

    @Override
    public void logAnalyticsEvent(@NonNull FirebaseAnalytics firebaseAnalytics, @NonNull AnalyticsEvent analyticsEvent, @NonNull String value) {
        final Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.VALUE, value);
        firebaseAnalytics.logEvent(analyticsEvent.toString(), bundle);
    }

    @Override
    public void logAnalyticsEvent(@NonNull final FirebaseAnalytics firebaseAnalytics, @NonNull final AnalyticsEvent analyticsEvent, final Bundle bundle) {
        firebaseAnalytics.logEvent(analyticsEvent.toString(), null);
    }
}
