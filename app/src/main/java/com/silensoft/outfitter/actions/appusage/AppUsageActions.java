package com.silensoft.outfitter.actions.appusage;

import android.content.Context;
import android.support.annotation.NonNull;

public interface AppUsageActions {
    void updateAppUsageInfo(@NonNull final Context context);
}
