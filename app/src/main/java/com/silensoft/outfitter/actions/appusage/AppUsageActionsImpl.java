package com.silensoft.outfitter.actions.appusage;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.appusageinfo.AppUsageInfo;

public class AppUsageActionsImpl implements AppUsageActions  {
    @SuppressWarnings("LawOfDemeter")
    private final AppUsageInfo appUsageInfo = DependencyManager.getInstance().getImplementationFor(AppUsageInfo.class);

    @Override
    public void updateAppUsageInfo(@NonNull final Context context) {
        appUsageInfo.registerAppFirstLaunchTimestampIfNeeded(context);
        appUsageInfo.incrementAppMainActivityStartedCount(context);
    }
}
