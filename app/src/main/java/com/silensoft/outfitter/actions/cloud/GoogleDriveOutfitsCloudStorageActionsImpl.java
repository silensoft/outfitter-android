package com.silensoft.outfitter.actions.cloud;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.drive.Drive;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.appusageinfo.AppUsageInfo;
import com.silensoft.outfitter.model.dao.OutfitCloudDao;
import com.silensoft.outfitter.model.dao.OutfitPhotoDao;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.dao.ToBeRemovedFromCloudOutfitDao;
import com.silensoft.outfitter.model.dao.ToBeSyncedToCloudOutfitDao;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.ui.presenters.ToastPresenter;
import com.silensoft.outfitter.ui.presenters.impl.OnlyOnceToastPresenter;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.FailureReporter;
import com.silensoft.outfitter.utils.NetworkUtils;

import java.util.List;

public class GoogleDriveOutfitsCloudStorageActionsImpl implements OutfitsCloudStorageActions {
    private static final int REQUEST_SIGN_IN = 3;

    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final ToBeRemovedFromCloudOutfitDao toBeRemovedFromCloudOutfitDao = DependencyManager.getInstance().getImplementationFor(ToBeRemovedFromCloudOutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final ToBeSyncedToCloudOutfitDao toBeSyncedToCloudOutfitDao = DependencyManager.getInstance().getImplementationFor(ToBeSyncedToCloudOutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitCloudDao outfitCloudDao = DependencyManager.getInstance().getImplementationFor(OutfitCloudDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final AppUsageInfo appUsageInfo = DependencyManager.getInstance().getImplementationFor(AppUsageInfo.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitPhotoDao outfitPhotoDao = DependencyManager.getInstance().getImplementationFor(OutfitPhotoDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final NetworkUtils networkUtils = DependencyManager.getInstance().getImplementationFor(NetworkUtils.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    public void initialize(@NonNull final Activity activity) {
        final int connectionResult = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity);
        if (connectionResult != ConnectionResult.SUCCESS) {
            final Dialog errorDialog = GoogleApiAvailability.getInstance().getErrorDialog(activity, connectionResult, 1);
            errorDialog.show();
        }
    }

    @Override public boolean isSignedIn(@NonNull final Activity activity) {
        return outfitCloudDao.isSignedIn(activity);
    }

    @Override
    public void startSignInActivity(@NonNull final Activity activity) {
        if (!networkUtils.hasInternetConnection(activity)) {
            return;
        }

        final GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .requestScopes(Drive.SCOPE_APPFOLDER)
                .build();
        final GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(activity, signInOptions);

        try {
            activity.startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_SIGN_IN);
        }
        catch (final ActivityNotFoundException exception) {
            failureReporter.reportFailure(exception);
        }
    }

    @Override
    public void syncOutfitsWithCloudAfterSuccessfulSignIn(@NonNull final Activity activity, final int requestCode, final int resultCode, final Intent intent) {
        if (requestCode == REQUEST_SIGN_IN && resultCode == Activity.RESULT_OK) {
            outfitCloudDao.setSignedIn(activity);
            syncOutfitsWithCloudAfterSuccessfulSignIn(activity);
        }
    }

    @Override
    public void syncOutfitsWithCloudAfterSuccessfulSignIn(@NonNull final Activity activity) {
        if (isSignedIn(activity)) {
            syncOutfitsFromCloud(activity);
            removeOutfitsFromCloud(activity);
            syncOutfitsToCloud(activity);
        } else {
         startSignInActivity(activity);
        }
    }

    private void syncOutfitsFromCloud(@NonNull final Activity activity) {
        if (networkUtils.hasInternetConnection(activity)) {
            final long lastCloudSyncTimestampInMilliSecs = appUsageInfo.getLastCloudSyncTimestampUtcInMilliSecs(activity);
            @SuppressWarnings("LocalVariableOfConcreteClass")
            final ToastPresenter toastPresenter = new OnlyOnceToastPresenter();
            outfitCloudDao.fetchOutfitsUpdatedAfter(activity, lastCloudSyncTimestampInMilliSecs, (fetchedOutfit) -> {
                toastPresenter.presentToast(activity, R.string.started_sync_from_cloud, Toast.LENGTH_SHORT);
                outfitActions.addOutfitFromCloud(activity, fetchedOutfit);
                appUsageInfo.setLastCloudSyncTimestampUtcInMilliSecs(activity, fetchedOutfit.getLastModifiedTimeInMillis());
            });
        }
    }

    private void syncOutfitsToCloud(@NonNull final Activity activity) {
        final List<Outfit> toBeSyncedToCloudOutfits = toBeSyncedToCloudOutfitDao.findAllOutfitsToBeSyncedToCloud(activity);
        for (final Outfit toBeSyncedToCloudOutfit : toBeSyncedToCloudOutfits) {
            Toast.makeText(activity, R.string.started_sync_to_cloud, Toast.LENGTH_SHORT).show();
            toBeSyncedToCloudOutfitDao.deleteOutfitToBeSyncedToCloud(activity, toBeSyncedToCloudOutfit.getUuidStr());
            final Optional<Bitmap> outfitPhotoFullSizeBitmap = outfitPhotoDao.getToBeSyncedToCloudFullSizeOutfitPhoto(toBeSyncedToCloudOutfit);
            saveOutfitPhotoToCloud(activity, outfitPhotoFullSizeBitmap, toBeSyncedToCloudOutfit);
        }
    }

    private void removeOutfitsFromCloud(@NonNull final Activity activity) {
        final List<Outfit> toBeRemovedFromCloudOutfits = toBeRemovedFromCloudOutfitDao.findAllOutfitsToBeRemovedFromCloud(activity);
        for (final Outfit toBeRemovedFromCloudOutfit : toBeRemovedFromCloudOutfits) {
            Toast.makeText(activity, R.string.started_sync_to_cloud, Toast.LENGTH_SHORT).show();
            toBeSyncedToCloudOutfitDao.deleteOutfitToBeSyncedToCloud(activity, toBeRemovedFromCloudOutfit.getUuidStr());
            deleteOutfitPhotoFromCloud(activity, toBeRemovedFromCloudOutfit);
        }
    }

    @SuppressWarnings({"LawOfDemeter", "FeatureEnvy"}) // Optional access
    @Override
    public void saveOutfitPhotoToCloud(@NonNull final Context context, @NonNull final Optional<Bitmap> outfitPhotoFullSizeBitmap, @NonNull final Outfit outfit) {
         outfitPhotoFullSizeBitmap.ifPresent(outfitBitmap ->
                         outfitCloudDao.saveOutfitPhotoOrElse(context, outfitBitmap, outfit, () ->
                                 outfitActions.addOutfitToBeSyncedToCloud(context, outfitBitmap, outfit)));
    }

    @Override
    public void deleteOutfitPhotoFromCloud(@NonNull final Context context, @NonNull final Outfit outfit) {
                outfitCloudDao.deleteOutfitPhotoOrElse(context, outfit, () -> outfitActions.addOutfitToBeRemovedFromCloud(context, outfit));
    }
}
