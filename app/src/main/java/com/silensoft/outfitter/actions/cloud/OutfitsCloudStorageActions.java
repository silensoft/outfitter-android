package com.silensoft.outfitter.actions.cloud;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.types.Optional;

public interface OutfitsCloudStorageActions {
    void initialize(@NonNull final Activity activity);
    boolean isSignedIn(@NonNull final Activity activity);
    void startSignInActivity(@NonNull final Activity activity);
    void syncOutfitsWithCloudAfterSuccessfulSignIn(@NonNull final Activity activity, final int requestCode, final int resultCode, final Intent intent);
    void syncOutfitsWithCloudAfterSuccessfulSignIn(@NonNull final Activity activity);
    void saveOutfitPhotoToCloud(@NonNull final Context context, @NonNull final Optional<Bitmap> outfitPhotoBitmap, @NonNull final Outfit outfit);
    void deleteOutfitPhotoFromCloud(@NonNull final Context activity, @NonNull final Outfit outfit);
}
