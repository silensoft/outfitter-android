package com.silensoft.outfitter.actions.geolocation;


import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Optional;

public interface GeoLocationActions {
    void startListeningGeoLocationChanges(@NonNull final Context context);
    void stopListeningGeoLocationChanges();
    Optional<Location> getGeoLocation();
}
