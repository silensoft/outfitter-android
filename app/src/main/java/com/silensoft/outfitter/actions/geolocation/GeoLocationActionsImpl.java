package com.silensoft.outfitter.actions.geolocation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class GeoLocationActionsImpl implements GeoLocationActions {
    private static final long LOCATION_UPDATE_INTERVAL_IN_MILLISECS = 10L * 60000L;
    private static final float LOCATION_UPDATE_INTERVAL_IN_METERS = 1000.0F;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Optional<Location> location;

    @Override
    public void startListeningGeoLocationChanges(@NonNull final Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location_) {
                location = OptionalImpl.ofNullable(location_);
            }

            @Override
            public void onStatusChanged(final String s, final int i, final Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(final String s) {

            }

            @Override
            public void onProviderDisabled(final String s) {

            }
        };

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_UPDATE_INTERVAL_IN_MILLISECS, LOCATION_UPDATE_INTERVAL_IN_METERS, locationListener);
            location = OptionalImpl.ofNullable(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
        }
    }

    @Override
    public void stopListeningGeoLocationChanges() {
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public Optional<Location> getGeoLocation() {
        return location;
    }
}
