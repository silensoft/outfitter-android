package com.silensoft.outfitter.actions.inappbilling;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.inappbilling.InAppBillingSku;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.types.Consumer;
import com.silensoft.outfitter.types.Optional;

import java.util.List;

public interface InAppBillingActions {
    void fetchPromotableSku(@NonNull final Consumer<InAppBillingSku> skuConsumer);
    void fetchSellableSkus(@NonNull final Consumer<List<InAppBillingSku>> sellableSkusConsumer);
    void hasPurchasedAnyOf(@NonNull final List<InAppBillingSkuType> inAppBillingSkuTypes,
                           @NonNull final Optional<Action> hasPurchasedAction, @NonNull final Optional<Action> hasNotPurchasedAction);
    void doOnPurchasesUpdated(@NonNull final Activity activity, @NonNull final Action onPurchasesUpdatedAction);
    void stopListeningPurchaseUpdates(@NonNull final Activity activity);
    void purchase(@NonNull final Context context, @NonNull final InAppBillingSku sku);
    void setupInAppBilling(@NonNull final Activity activity, @NonNull final Action actionAfterSetup);
    void terminateInAppBilling();
}
