package com.silensoft.outfitter.actions.inappbilling;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuImpl;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSku;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.libraries.inappbilling.IabException;
import com.silensoft.outfitter.libraries.inappbilling.IabHelper;
import com.silensoft.outfitter.libraries.inappbilling.Inventory;
import com.silensoft.outfitter.libraries.inappbilling.SkuDetails;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.types.Consumer;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.FailureReporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InAppBillingActionsImpl implements InAppBillingActions {
    private static final int REQUEST_PURCHASE = 10001;

    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    private Optional<IabHelper> optInAppBillingManager;
    private Activity purchaseActivity;
    private final Map<Activity, BroadcastReceiver> purchasesUpdatedBroadcastReceivers = new HashMap<>(2);

    @SuppressWarnings("LocalVariableOfConcreteClass")
    @Override
    public void setupInAppBilling(@NonNull final Activity purchaseActivity_, @NonNull final Action actionAfterSetup) {
        purchaseActivity = purchaseActivity_;
        final IabHelper inAppBillingManager = new IabHelper(purchaseActivity, ""); // TODO: RELEASE: supply correct key
        inAppBillingManager.startSetup(result -> {
            if (result.isSuccess()) {
                optInAppBillingManager = OptionalImpl.of(inAppBillingManager);
                actionAfterSetup.perform();
            } else {
                failureReporter.reportFailure(new IabException(result));
            }
        });
    }

    @Override
    public void terminateInAppBilling() {
        optInAppBillingManager.ifPresent(inAppBillingManager -> {
            try {
                inAppBillingManager.dispose();
            } catch (final IabHelper.IabAsyncInProgressException exception) {
                failureReporter.reportFailure(exception);
            }
            optInAppBillingManager = OptionalImpl.empty();
        });
    }

    @SuppressWarnings({"FeatureEnvy", "LocalVariableOfConcreteClass"}) // Consumer is interface
    @Override
    public void fetchPromotableSku(@NonNull final Consumer<InAppBillingSku> promotableSkuConsumer) {
        optInAppBillingManager.ifPresent(inAppBillingManager -> {
            try {
                inAppBillingManager.queryInventoryAsync((result, inventory) -> {
                    if (result.isFailure()) {
                        failureReporter.reportFailure(new IabException(result));
                    } else {
                        if (inventory.hasPurchase(InAppBillingSkuType.PLUS_PACKAGE.toSkuString())) {
                            final SkuDetails skuDetails = inventory.getSkuDetails(InAppBillingSkuType.PRO_SUBSCRIPTION.toSkuString());
                            promotableSkuConsumer.consume(new InAppBillingSkuImpl(InAppBillingSkuType.PRO_SUBSCRIPTION, skuDetails));
                        } else if (inventory.hasPurchase(InAppBillingSkuType.PRO_SUBSCRIPTION.toSkuString())) {
                            promotableSkuConsumer.consume(new InAppBillingSkuImpl());
                        } else {
                            final SkuDetails skuDetails = inventory.getSkuDetails(InAppBillingSkuType.PLUS_PACKAGE.toSkuString());
                            promotableSkuConsumer.consume(new InAppBillingSkuImpl(InAppBillingSkuType.PLUS_PACKAGE, skuDetails));
                        }
                    }
                });

            } catch (final IabHelper.IabAsyncInProgressException exception) {
                failureReporter.reportFailure(exception);
            }
        });
        promotableSkuConsumer.consume(new InAppBillingSkuImpl());
    }

    @SuppressWarnings("LocalVariableOfConcreteClass")
    @Override
    public void fetchSellableSkus(@NonNull final Consumer<List<InAppBillingSku>> sellableSkusConsumer) {
        final List<InAppBillingSku> sellableSkus = new ArrayList<>(2);
        optInAppBillingManager.ifPresent(inAppBillingManager -> {
            try {
                inAppBillingManager.queryInventoryAsync((result, inventory) -> {
                    if (result.isFailure()) {
                        failureReporter.reportFailure(new IabException(result));
                    } else {
                        if (inventory.hasPurchase(InAppBillingSkuType.PLUS_PACKAGE.toSkuString())) {
                            addSellableSku(sellableSkus, inventory, InAppBillingSkuType.PRO_SUBSCRIPTION);
                        } else if (!inventory.hasPurchase(InAppBillingSkuType.PRO_SUBSCRIPTION.toSkuString())) {
                            addSellableSku(sellableSkus, inventory, InAppBillingSkuType.PLUS_PACKAGE);
                            addSellableSku(sellableSkus, inventory, InAppBillingSkuType.PRO_SUBSCRIPTION);
                        }
                    }
                });

            } catch (final IabHelper.IabAsyncInProgressException exception) {
                failureReporter.reportFailure(exception);
            }
        });

        sellableSkusConsumer.consume(sellableSkus);
    }

    @SuppressWarnings("FeatureEnvy") // Optional access
    @Override
    public void hasPurchasedAnyOf(@NonNull final List<InAppBillingSkuType> inAppBillingSkuTypes, @NonNull final Optional<Action> hasPurchasedAction,
                                  @NonNull final Optional<Action> hasNotPurchasedAction) {
        optInAppBillingManager.ifPresent(inAppBillingManager -> {
            try {
                inAppBillingManager.queryInventoryAsync((result, inventory) -> {
                    if (result.isFailure()) {
                        failureReporter.reportFailure(new IabException(result));
                    } else {
                        boolean hasPurchasedAny = false;
                        for (final InAppBillingSkuType inAppBillingSkuType : inAppBillingSkuTypes) {
                            hasPurchasedAny |= inventory.hasPurchase(inAppBillingSkuType.toSkuString());
                        }
                        if (hasPurchasedAny) {
                            hasPurchasedAction.ifPresent(Action::perform);
                        }
                    }
                });
            } catch (final IabHelper.IabAsyncInProgressException exception) {
                failureReporter.reportFailure(exception);
            }
        });

        hasNotPurchasedAction.ifPresent(Action::perform);
    }

    @Override
    public void doOnPurchasesUpdated(@NonNull final Activity activity, @NonNull final Action onPurchasesUpdatedAction) {
        final IntentFilter purchasesUpdatedIntentFilter = new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
        final BroadcastReceiver purchasesUpdatedBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                onPurchasesUpdatedAction.perform();
            }
        };
        purchasesUpdatedBroadcastReceivers.put(activity, purchasesUpdatedBroadcastReceiver);
        activity.registerReceiver(purchasesUpdatedBroadcastReceiver, purchasesUpdatedIntentFilter);
    }

    @Override
    public void stopListeningPurchaseUpdates(@NonNull final Activity activity) {
        final BroadcastReceiver purchasesUpdatedBroadcastReceiver = purchasesUpdatedBroadcastReceivers.get(activity);
        activity.unregisterReceiver(purchasesUpdatedBroadcastReceiver);
    }

    @SuppressWarnings("FeatureEnvy") // sku is enum
    @Override
    public void purchase(@NonNull final Context context, @NonNull final InAppBillingSku sku) {
        optInAppBillingManager.ifPresent(inAppBillingManager -> {
            try {
                inAppBillingManager.launchPurchaseFlow(purchaseActivity, sku.getSkuString(), sku.getItemType(), null, REQUEST_PURCHASE, (result, purchase) -> {
                            if (result.isFailure()) {
                                failureReporter.reportFailure(new IabException(result));
                                Toast.makeText(context, R.string.purchase_failed, Toast.LENGTH_SHORT).show();
                            }
                            else if (purchase.getSku().equals(sku.getSkuString())) {
                                Toast.makeText(context, R.string.purchase_successful, Toast.LENGTH_SHORT).show();
                            }
                        },
                        ""
                );
            } catch (final IabHelper.IabAsyncInProgressException exception) {
                failureReporter.reportFailure(exception);
            }
        });
    }

    @SuppressWarnings({"MethodParameterOfConcreteClass", "LocalVariableOfConcreteClass"})
    private static void addSellableSku(@NonNull final List<InAppBillingSku> sellableSkus, final Inventory inventory, @NonNull final InAppBillingSkuType inAppBillingSkuType) {
        final SkuDetails skuDetails = inventory.getSkuDetails(inAppBillingSkuType.toSkuString());
        sellableSkus.add(new InAppBillingSkuImpl(inAppBillingSkuType, skuDetails));
    }
}
