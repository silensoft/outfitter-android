package com.silensoft.outfitter.actions.outfit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.types.ProgressConsumer;

import java.util.List;

public interface OutfitActions {
    String EDIT_OUTFIT_KEY = "edit";

    void pickOutfitPhotoFromCamera(@NonNull final Activity activity);
    void pickOutfitPhotoFromGallery(@NonNull final Activity activity);

    void startAddOutfitActivity(@NonNull final Activity activity, final int requestCode, final int resultCode, @NonNull final Intent intent);
    void startEditOutfitActivity(@NonNull final Context context, @NonNull final Optional<Outfit> outfit);
    void startSingleOutfitViewActivity(@NonNull final Context context, @NonNull final Outfit outfit);

    void addOutfitFromCloud(@NonNull final Context context, @NonNull final Outfit outfit);
    void addOutfitFromIntent(@NonNull final Context context, final int requestCode, final int resultCode, final Intent intent);

    void addOutfitToBeSyncedToCloud(@NonNull final Context context, @NonNull final Bitmap outfitPhotoFullSizeBitmap, @NonNull final Outfit outfit);
    void addOutfitToBeRemovedFromCloud(@NonNull final Context context, @NonNull final Outfit outfit);

    void shareOutfit(@NonNull final Context context, @NonNull final Optional<Outfit> outfit);
    void removeOutfit(@NonNull final Context context, @NonNull final Optional<Outfit> outfit, final boolean showToast);

    void removeOutfitsAndUpdateProgress(@NonNull final Context context, @NonNull final List<Outfit> outfits, @NonNull final ProgressConsumer progressConsumer);

    void handleRequestPermissionResults(@NonNull final Activity activity, final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults);
    void handleMenuItemId(@NonNull final Context context, final int menuItemId, @NonNull final Optional<Outfit> outfit, @NonNull final FirebaseAnalytics firebaseAnalytics);
}
