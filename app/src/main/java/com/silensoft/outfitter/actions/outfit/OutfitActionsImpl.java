package com.silensoft.outfitter.actions.outfit;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitActions;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.dao.OutfitPhotoDao;
import com.silensoft.outfitter.actions.security.PermissionRequestActions;
import com.silensoft.outfitter.actions.security.PermissionRequestResultActions;
import com.silensoft.outfitter.model.dao.ToBeRemovedFromCloudOutfitDao;
import com.silensoft.outfitter.model.dao.ToBeSyncedToCloudOutfitDao;
import com.silensoft.outfitter.model.persistableimage.PersistableImageImpl;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.ProgressConsumer;
import com.silensoft.outfitter.ui.activities.AddOrEditOutfitActivity;
import com.silensoft.outfitter.actions.cloud.OutfitsCloudStorageActions;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.dao.OutfitDao;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.ui.activities.SingleOutfitViewActivity;
import com.silensoft.outfitter.ui.dialogs.DeleteOutfitConfirmationDialog;
import com.silensoft.outfitter.utils.FailureReporter;
import com.silensoft.outfitter.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

public class OutfitActionsImpl implements OutfitActions {
    private static final int REQUEST_CAMERA_IMAGE = 1;
    private static final int REQUEST_GALLERY_IMAGE = 2;

    private int readStoragePermissionRequestId = -1;
    private int writeStoragePermissionRequestId = -1;

    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitDao outfitDao = DependencyManager.getInstance().getImplementationFor(OutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitsCloudStorageActions outfitsCloudStorageActions = DependencyManager.getInstance().getImplementationFor(OutfitsCloudStorageActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitPhotoDao outfitPhotoDao = DependencyManager.getInstance().getImplementationFor(OutfitPhotoDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitOccasionActions randomOutfitOccasionActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitOccasionActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitSeasonActions randomOutfitSeasonActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitSeasonActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final PermissionRequestActions permissionRequestActions = DependencyManager.getInstance().getImplementationFor(PermissionRequestActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final PermissionRequestResultActions permissionRequestResultActions = DependencyManager.getInstance().getImplementationFor(PermissionRequestResultActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitActions randomOutfitActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final ToBeSyncedToCloudOutfitDao toBeSyncedToCloudOutfitDao = DependencyManager.getInstance().getImplementationFor(ToBeSyncedToCloudOutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final ToBeRemovedFromCloudOutfitDao toBeRemovedFromCloudOutfitDao = DependencyManager.getInstance().getImplementationFor(ToBeRemovedFromCloudOutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);
    @SuppressWarnings("LawOfDemeter")
    private final DeleteOutfitConfirmationDialog deleteOutfitConfirmationDialog = DependencyManager.getInstance().getImplementationFor(DeleteOutfitConfirmationDialog.class);

    public void pickOutfitPhotoFromCamera(@NonNull final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            writeStoragePermissionRequestId = permissionRequestActions.requestPermissionOrPerformAction(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, () -> startGetOutfitPhotoWithCameraActivity(activity));
        }
    }

    public void pickOutfitPhotoFromGallery(@NonNull final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            readStoragePermissionRequestId = permissionRequestActions.requestPermissionOrPerformAction(activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE, () -> startGetOutfitPhotoFromGalleryActivity(activity));
        }
    }

    public void handleRequestPermissionResults(@NonNull final Activity activity, final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        if (requestCode == writeStoragePermissionRequestId) {
            permissionRequestResultActions.handlePermissionRequestResult(activity, grantResults,
                    () -> startGetOutfitPhotoWithCameraActivity(activity), R.string.permission_denied_camera);
        } else if (requestCode == readStoragePermissionRequestId) {
            permissionRequestResultActions.handlePermissionRequestResult(activity, grantResults,
                    () -> startGetOutfitPhotoFromGalleryActivity(activity), R.string.permission_denied_gallery);
        }
    }

    @Override
    public void startAddOutfitActivity(@NonNull final Activity activity, final int requestCode, final int resultCode, @NonNull final Intent intent) {
        if (requestCode == REQUEST_CAMERA_IMAGE && resultCode == Activity.RESULT_OK) {
            startAddOutfitActivity(activity, outfitPhotoDao.getTemporaryOutfitPhotoScaledToScreenSizeBitmap(activity));
        } else if (requestCode == REQUEST_GALLERY_IMAGE && resultCode == Activity.RESULT_OK) {
            final Optional<Bitmap> outfitPhotoBitmap = finishGetOutfitPhotoFromGalleryActivity(activity, intent);
            startAddOutfitActivity(activity, outfitPhotoBitmap);
        }
    }

    private void addOutfit(@NonNull final Context context, @NonNull final Outfit outfit) {
        if (outfitPhotoDao.savePermanentOutfitPhoto(outfit)) {
            addOutfitPhotoToGallery(context, outfit);
            if (outfitDao.insertOutfit(context, outfit)) {
                outfitsCloudStorageActions.saveOutfitPhotoToCloud(context, outfitPhotoDao.getTemporaryOutfitPhotoBitmap(), outfit);
                Toast.makeText(context, R.string.outfit_added_successfully, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Toast.makeText(context, R.string.cannot_add_outfit, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addOutfitFromCloud(@NonNull final Context context, @NonNull final Outfit outfitFromCloud) {
        if (outfitDao.databaseAlreadyHasOutfit(context, outfitFromCloud)) {
            return;
        }

        if (outfitPhotoDao.savePermanentOutfitPhoto(outfitFromCloud)){
            addOutfitPhotoToGallery(context, outfitFromCloud);
            if (outfitDao.insertOutfit(context, outfitFromCloud)) {
                return;
            }
        }

        Toast.makeText(context, R.string.cannot_add_outfits_from_cloud, Toast.LENGTH_SHORT).show();
    }

    @SuppressWarnings("LawOfDemeter")
    @Override
    public void shareOutfit(@NonNull final Context context, @NonNull final Optional<Outfit> optOutfit) {
        optOutfit.ifPresent(outfit -> {
            final Intent shareOutfitIntent = new Intent(Intent.ACTION_SEND);
            shareOutfitIntent.setType("image/*");
            outfitPhotoDao.getPermanentOutfitPhotoFileUri(context, optOutfit).ifPresent(permanentOutfitPhotoFileUri -> {
                    shareOutfitIntent.putExtra(Intent.EXTRA_STREAM, permanentOutfitPhotoFileUri);
                    try {
                        context.startActivity(shareOutfitIntent);
                    } catch (final ActivityNotFoundException exception) {
                        failureReporter.reportFailure(exception);
                    }
            });
        });
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public void removeOutfit(@NonNull final Context context, @NonNull final Optional<Outfit> optOutfit, final boolean showToast) {
        optOutfit.ifPresent(outfit -> {
            outfitDao.deleteOutfit(context, outfit);
            outfitsCloudStorageActions.deleteOutfitPhotoFromCloud(context, outfit);
            outfit.getPermanentPhotoFilePath().ifPresent(FileUtils::deleteFile);
            if (showToast) {
                Toast.makeText(context, R.string.outfit_removed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public void removeOutfitsAndUpdateProgress(@NonNull final Context context, @NonNull final List<Outfit> outfits, @NonNull final ProgressConsumer progressConsumer) {
        final int outfitCount = outfits.size();
        progressConsumer.consumeProgress(0, outfitCount);
        Observable.range(0, outfitCount).forEach(outfitIndex -> {
            removeOutfit(context, OptionalImpl.of(outfits.get(outfitIndex)), true);
            progressConsumer.consumeProgress(outfitIndex + 1, outfitCount);
        }).dispose();
    }

    @Override
    public void addOutfitFromIntent(@NonNull final Context context, final int requestCode, final int resultCode, final Intent intent) {
        if (resultCode == Activity.RESULT_OK && intent != null && intent.getParcelableExtra(Outfit.DATA_KEY_OUTFIT_PHOTO_BITMAP) != null) {
            final boolean editOutfit = intent.getBooleanExtra(EDIT_OUTFIT_KEY, false);
            final Outfit outfit = outfitFactory.createOutfitFromIntent(intent);
            if (editOutfit) {
                removeOutfit(context, OptionalImpl.of(outfit), false);
            }
            addOutfit(context, outfit);
            randomOutfitActions.addOutfit(outfit);
        }
    }

    @Override
    public void addOutfitToBeSyncedToCloud(@NonNull final Context context, @NonNull final Bitmap outfitPhotoFullSizeBitmap, @NonNull final Outfit outfit) {
        toBeSyncedToCloudOutfitDao.insertOutfitToBeSyncedToCloud(context, outfit);
        outfitPhotoDao.saveToBeSyncedToCloudFullSizeOutfitPhoto(new PersistableImageImpl(outfitPhotoFullSizeBitmap, UUID.fromString(outfit.getUuidStr())));
    }

    @Override
    public void addOutfitToBeRemovedFromCloud(@NonNull final Context context, @NonNull final Outfit outfit) {
        toBeRemovedFromCloudOutfitDao.insertOutfitToBeRemovedFromCloud(context, outfit);
    }

    @Override
    public void startEditOutfitActivity(@NonNull final Context context, @NonNull final Optional<Outfit> optOutfit) {
        optOutfit.ifPresent(outfit -> {
            final Intent editOutfitIntent = new Intent(context.getApplicationContext(), AddOrEditOutfitActivity.class);
            outfit.persistToIntent(editOutfitIntent);
            editOutfitIntent.putExtra(EDIT_OUTFIT_KEY, true);

            try {
                context.startActivity(editOutfitIntent);
            } catch (final ActivityNotFoundException exception) {
                failureReporter.reportFailure(exception);
            }
        });
    }

    @Override
    public void startSingleOutfitViewActivity(@NonNull final Context context, @NonNull final  Outfit outfit) {
        final Intent singleOutfitViewIntent = new Intent(context.getApplicationContext(), SingleOutfitViewActivity.class);
        outfit.persistToIntent(singleOutfitViewIntent);
        try {
            context.startActivity(singleOutfitViewIntent);
        } catch (final ActivityNotFoundException exception) {
            failureReporter.reportFailure(exception);
        }
    }

    public void handleMenuItemId(@NonNull final Context context, final int menuItemId, @NonNull final Optional<Outfit> outfit, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        switch (menuItemId) {
            case R.id.shareOutfit:
                analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.SHARE_OUTFIT);
                shareOutfit(context, outfit);
                break;
            case R.id.editOutfit:
                startEditOutfitActivity(context, outfit);
                break;
            case R.id.removeOutfit:
                deleteOutfitConfirmationDialog.showAndOnPositiveSelected(context, () -> {
                    analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.REMOVE_OUTFIT);
                    removeOutfit(context, outfit, true);
                });
                break;
            default:
        }
    }

    @SuppressWarnings("LawOfDemeter")
    private void startGetOutfitPhotoWithCameraActivity(@NonNull final Activity activity) {
        final Intent takeOutfitPictureWithCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takeOutfitPictureWithCameraIntent.resolveActivity(activity.getPackageManager()) != null) {
            outfitPhotoDao.createTemporaryOutfitPhotoFileUri(activity).ifPresentOrElse(temporaryOutfitPhotoUri -> {
                    takeOutfitPictureWithCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, temporaryOutfitPhotoUri);
                    try {
                        activity.startActivityForResult(takeOutfitPictureWithCameraIntent, REQUEST_CAMERA_IMAGE);
                    } catch (final ActivityNotFoundException exception) {
                        failureReporter.reportFailure(exception);
                    }
            }, () -> Toast.makeText(activity, R.string.cannot_add_outfit, Toast.LENGTH_SHORT).show());
        }
    }

    private void startGetOutfitPhotoFromGalleryActivity(@NonNull final Activity activity) {
        final Intent pickOutfitPictureFromGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        try {
            activity.startActivityForResult(pickOutfitPictureFromGalleryIntent, REQUEST_GALLERY_IMAGE);
        } catch (final ActivityNotFoundException exception) {
            failureReporter.reportFailure(exception);
        }
    }

    private void startAddOutfitActivity(@NonNull final Context context, @NonNull final Optional<Bitmap> optOutfitPhotoBitmap) {
        optOutfitPhotoBitmap.ifPresentOrElse(outfitPhotoBitmap -> {
            final Intent addOutfitIntent = new Intent(context.getApplicationContext(), AddOrEditOutfitActivity.class);
            addOutfitIntent.putExtra(Outfit.DATA_KEY_OUTFIT_PHOTO_BITMAP, outfitPhotoBitmap);
            addOutfitIntent.putExtra(Outfit.METADATA_KEY_OCCASIONS, new ArrayList<>(Collections.singletonList(randomOutfitOccasionActions.getCurrentOccasionIndex())));
            addOutfitIntent.putExtra(Outfit.METADATA_KEY_SEASONS, new ArrayList<>(Collections.singletonList(randomOutfitSeasonActions.getCurrentSeason().toString())));
            addOutfitIntent.putExtra("edit", false);
            try {
                context.startActivity(addOutfitIntent);
            } catch (final ActivityNotFoundException exception) {
                failureReporter.reportFailure(exception);
            }
        }, () -> Toast.makeText(context, R.string.cannot_add_outfit, Toast.LENGTH_SHORT).show());
    }

    @SuppressWarnings("LawOfDemeter")
    private Optional<Bitmap> finishGetOutfitPhotoFromGalleryActivity(@NonNull final Activity activity, @NonNull final Intent intent) {
        try {
            final Uri selectedOutfitPhotoUri = intent.getData();
            final Optional<Bitmap> selectedOutfitPhotoBitmap = OptionalImpl.ofNullable(MediaStore.Images.Media.getBitmap(activity.getContentResolver(), selectedOutfitPhotoUri));
            selectedOutfitPhotoBitmap.ifPresent(outfitPhotoBitmap -> outfitPhotoDao.saveTemporaryOutfitPhoto(activity, outfitPhotoBitmap));
            return outfitPhotoDao.getTemporaryOutfitPhotoScaledToScreenSizeBitmap(activity);
        } catch (final IOException exception) {
            return OptionalImpl.emptyFailedWith(exception);
        }
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    private static void addOutfitPhotoToGallery(@NonNull final Context context, @NonNull final Outfit outfit) {
        outfit.getPermanentPhotoFilePath().ifPresent(outfitPhotoFilePath -> {
            final Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final File f = new File(outfitPhotoFilePath);
            final Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);
        });
    }
}
