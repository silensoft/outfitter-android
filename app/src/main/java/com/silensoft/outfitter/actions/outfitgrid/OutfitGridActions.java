package com.silensoft.outfitter.actions.outfitgrid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;

public interface OutfitGridActions {
    boolean handleMenuItemSelection(@NonNull final AppCompatActivity activity, final MenuItem menuItem, @NonNull final FirebaseAnalytics firebaseAnalytics);
    void showOutfitsByHashTags(@NonNull final AppCompatActivity activity, final Intent intent, @NonNull final FirebaseAnalytics firebaseAnalytics);
    void showOutfitsByOccasions(@NonNull final AppCompatActivity activity, @NonNull final FirebaseAnalytics firebaseAnalytics);
    void showOutfitsBySeasons(@NonNull final AppCompatActivity activity, @NonNull final FirebaseAnalytics firebaseAnalytics);
}
