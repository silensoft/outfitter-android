package com.silensoft.outfitter.actions.outfitgrid;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.ui.adapters.OutfitGridAdapterImpl;

import java.util.List;

public interface OutfitGridLoaderManager extends LoaderManager.LoaderCallbacks<Cursor>  {
    @SuppressWarnings("MethodParameterOfConcreteClass")
    void initialize(@NonNull final AppCompatActivity activity, @NonNull final OutfitGridAdapterImpl outfitGridAdapter_,
                    @NonNull final SearchView searchView, @NonNull final Outfit prototypeOutfit_);
    void restart();
    @NonNull Outfit getPrototypeOutfit();
    void persistPrototypeOutfitToBundle(@NonNull final Bundle bundle);
    void showOutfitsFrom(@NonNull final Outfit prototypeOutfit);
    void showOutfitsWithHashTags(@NonNull final String hashTagsStr);
    void showAllOutfits();
    void selectAllOutfits();
    @NonNull Optional<Outfit> getOutfitAtGridPosition(final int positionIndex);
    int getOutfitCount();
    boolean isOutfitSelected(@NonNull final Outfit outfit);
    void toggleOutfitSelected(@NonNull final Outfit outfit);
    @NonNull List<Outfit> getSelectedOutfits();
}
