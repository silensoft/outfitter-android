package com.silensoft.outfitter.actions.outfitgrid.impl;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridActions;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridLoaderManager;
import com.silensoft.outfitter.actions.search.OutfitHashTagsSearchRecentSuggestionsProvider;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.ui.dialogs.ClearSearchHistoryDialog;
import com.silensoft.outfitter.ui.dialogs.DeleteSelectedOutfitsDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionsSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonsSelectorDialog;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;

public class OutfitGridActionsImpl implements OutfitGridActions {
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger =  DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitGridLoaderManager outfitGridLoaderManager = DependencyManager.getInstance().getImplementationFor(OutfitGridLoaderManager.class);
    @SuppressWarnings("LawOfDemeter")
    private final DeleteSelectedOutfitsDialog deleteSelectedOutfitsDialog = DependencyManager.getInstance().getImplementationFor(DeleteSelectedOutfitsDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitOccasionsSelectorDialog outfitOccasionsSelectorDialog = DependencyManager.getInstance().getImplementationFor(OutfitOccasionsSelectorDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitSeasonsSelectorDialog outfitSeasonsSelectorDialog = DependencyManager.getInstance().getImplementationFor(OutfitSeasonsSelectorDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final ClearSearchHistoryDialog clearSearchHistoryDialog = DependencyManager.getInstance().getImplementationFor(ClearSearchHistoryDialog.class);

    @Override
    public boolean handleMenuItemSelection(@NonNull final AppCompatActivity activity, final MenuItem menuItem, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        switch (menuItem.getItemId()) {
            case R.id.show_all:
                showAllOutfits(firebaseAnalytics);
                return true;
            case R.id.change_occasions:
                showOutfitsByOccasions(activity, firebaseAnalytics);
                return true;
            case R.id.change_seasons:
                showOutfitsBySeasons(activity, firebaseAnalytics);
                return true;
            case R.id.select_all:
                selectAllOutfits(firebaseAnalytics);
                return true;
            case R.id.delete_selected:
                deleteSelectedOutfits(activity, firebaseAnalytics);
                return true;
            case R.id.clear_search_history:
                clearSearchHistory(activity);
            default:
                return false;
        }
    }

    @Override
    public void showOutfitsByHashTags(@NonNull final AppCompatActivity activity, final Intent intent, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String hashTagsStr = intent.getStringExtra(SearchManager.QUERY);
            final SearchRecentSuggestions searchRecentSuggestions = new SearchRecentSuggestions(activity,
                    OutfitHashTagsSearchRecentSuggestionsProvider.AUTHORITY, OutfitHashTagsSearchRecentSuggestionsProvider.MODE);
            searchRecentSuggestions.saveRecentQuery(hashTagsStr, null);
            final Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, hashTagsStr);
            analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.SEARCH_OUTFITS, bundle);
            outfitGridLoaderManager.showOutfitsWithHashTags(hashTagsStr);
        }
    }

    @Override
    public void showOutfitsByOccasions(@NonNull final AppCompatActivity activity, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        final Outfit prototypeOutfit = outfitGridLoaderManager.getPrototypeOutfit();
        outfitOccasionsSelectorDialog.show(activity, prototypeOutfit);
        outfitGridLoaderManager.showOutfitsFrom(prototypeOutfit);
        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.CHANGE_OCCASIONS);
    }

    @Override
    public void showOutfitsBySeasons(@NonNull final AppCompatActivity activity, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        final Outfit prototypeOutfit = outfitGridLoaderManager.getPrototypeOutfit();
        outfitSeasonsSelectorDialog.show(activity, prototypeOutfit);
        outfitGridLoaderManager.showOutfitsFrom(prototypeOutfit);
        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.CHANGE_SEASONS);
    }

    private void showAllOutfits(@NonNull final FirebaseAnalytics firebaseAnalytics) {
        outfitGridLoaderManager.showAllOutfits();
        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.SHOW_ALL_OUTFITS);
    }

    private void selectAllOutfits(@NonNull final FirebaseAnalytics firebaseAnalytics) {
        outfitGridLoaderManager.selectAllOutfits();
        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.SELECT_ALL_OUTFITS);
    }

    private void deleteSelectedOutfits(@NonNull final AppCompatActivity activity, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        deleteSelectedOutfitsDialog.showAndOnPositiveSelected(activity, () -> {
            analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.REMOVE_SELECTED_OUTFITS);
            outfitActions.removeOutfitsAndUpdateProgress(activity, outfitGridLoaderManager.getSelectedOutfits(), (currentProgress, maxProgress) -> {
                final ProgressBar progressBar = activity.findViewById(R.id.progressBar);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setMax(maxProgress);
                progressBar.setProgress(currentProgress);
                if (currentProgress == maxProgress) {
                    progressBar.setVisibility(View.GONE);
                }
            });
            outfitGridLoaderManager.restart();
        });
    }

    private void clearSearchHistory(@NonNull final AppCompatActivity activity) {
        clearSearchHistoryDialog.showAndOnPositiveSelected(activity, () -> {
            final SearchRecentSuggestions searchRecentSuggestions = new SearchRecentSuggestions(activity,
                    OutfitHashTagsSearchRecentSuggestionsProvider.AUTHORITY, OutfitHashTagsSearchRecentSuggestionsProvider.MODE);
            searchRecentSuggestions.clearHistory();
            Toast.makeText(activity, R.string.search_history_cleared, Toast.LENGTH_SHORT).show();
        });
    }
}
