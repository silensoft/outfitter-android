package com.silensoft.outfitter.actions.outfitgrid.impl;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;

import com.google.common.collect.ImmutableList;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.model.dao.OutfitDao;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.ui.adapters.OutfitGridAdapterImpl;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridLoaderManager;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.Observable;

public class OutfitGridLoaderManagerImpl implements OutfitGridLoaderManager {
    private static final String WHITESPACE = "[\\s\\xA0]+";
    private static final Pattern COMPILED_WHITESPACE_PATTERN = Pattern.compile(WHITESPACE);

    @SuppressWarnings("LawOfDemeter")
    private final OutfitDao outfitDao = DependencyManager.getInstance().getImplementationFor(OutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);

    private static final int OUTFITTER_LOADER_ID = 1;

    private OutfitGridAdapterImpl outfitGridAdapter;
    private AppCompatActivity activity;
    private SearchView searchView;
    private Outfit prototypeOutfit;
    private Optional<Cursor> optCursor;
    private List<Outfit> selectedOutfits = new ArrayList<>(50);

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public void initialize(@NonNull final AppCompatActivity activity_, @NonNull final OutfitGridAdapterImpl outfitGridAdapter_,
                           @Nullable final SearchView searchView_, @NonNull final Outfit prototypeOutfit_) {
        activity = activity_;
        outfitGridAdapter = outfitGridAdapter_;
        searchView = searchView_;
        prototypeOutfit = prototypeOutfit_;
        activity.getSupportLoaderManager().initLoader(OUTFITTER_LOADER_ID, null, this);
    }

    @Override
    public void restart() {
        activity.getSupportLoaderManager().restartLoader(OUTFITTER_LOADER_ID, null, this);
    }

    @NonNull
    @Override
    public Outfit getPrototypeOutfit() {
        return outfitFactory.createOutfit(prototypeOutfit);
    }

    @Override
    public void persistPrototypeOutfitToBundle(@NonNull final Bundle bundle) {
        prototypeOutfit.persistToBundle(bundle);
    }

    @Override
    public void showOutfitsFrom(@NonNull final Outfit prototypeOutfit_) {
        deselectAllOutfits();
        prototypeOutfit = prototypeOutfit_;
        restart();
    }

    @Override
    public void showOutfitsWithHashTags(@NonNull final String hashTagsStr) {
        final List<String> hashTags = Observable.fromArray(COMPILED_WHITESPACE_PATTERN.split(hashTagsStr)).map(String::trim).toList().blockingGet();
        prototypeOutfit.setHashTags(hashTags);
        restart();
    }

    @Override
    public void showAllOutfits() {
        deselectAllOutfits();
        clearHashTagsSearch();
        prototypeOutfit.setOccasions(OutfitOccasion.getValues());
        prototypeOutfit.setSeasons(OutfitSeason.getValues());
        restart();
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public void selectAllOutfits() {
        optCursor.ifPresent(cursor -> {
            selectedOutfits = Observable.range(0, cursor.getCount()).map(position -> getOutfitAtGridPosition(position).get()).toList().blockingGet();
        });
    }

    @NonNull
    @Override
    public Optional<Outfit> getOutfitAtGridPosition(final int positionIndex) {
        return optCursor.flatMap(cursor -> {
            cursor.moveToPosition(positionIndex);
            return outfitDao.getOutfitFromCursor(cursor);
        });
    }

    @Override
    public int getOutfitCount() {
        return optCursor.mapOrElse(Cursor::getCount, 0);
    }


    @Override
    public boolean isOutfitSelected(@NonNull final Outfit outfit) {
        return selectedOutfits.contains(outfit);
    }

    @Override
    public void toggleOutfitSelected(@NonNull final Outfit outfit) {
        if (selectedOutfits.contains(outfit)) {
            selectedOutfits.remove(outfit);
        } else {
            selectedOutfits.add(outfit);
        }
    }

    @NonNull
    @Override
    public List<Outfit> getSelectedOutfits() {
        return ImmutableList.copyOf(selectedOutfits);
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        return outfitDao.findOutfitsBasedOnPrototypeOutfitSortedByLastModifiedTimeDescending(activity, prototypeOutfit);
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor cursor_) {
        optCursor.ifPresent(Cursor::close);
        optCursor = OptionalImpl.ofNullable(cursor_);
        optCursor.ifPresent(cursor -> {
            cursor.moveToFirst();
            outfitGridAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onLoaderReset(final Loader loader) {
    }

    private void deselectAllOutfits() {
       selectedOutfits.clear();
    }

    private void clearHashTagsSearch() {
        if (searchView != null) {
            searchView.setQuery("", false);
            searchView.setIconified(true);
        }
    }
}
