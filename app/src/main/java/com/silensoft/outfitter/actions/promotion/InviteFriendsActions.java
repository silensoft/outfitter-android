package com.silensoft.outfitter.actions.promotion;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

public interface InviteFriendsActions {
    void inviteFriends(@NonNull final Activity activity);
    void logInvitedFriends(final int requestCode, final int resultCode, final Intent intent, @NonNull final FirebaseAnalytics firebaseAnalytics);
}
