package com.silensoft.outfitter.actions.promotion;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

public interface PromoteAppRatingActions {
    void showRateAppDialogWhenAppropriate(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics);
}
