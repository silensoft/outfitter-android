package com.silensoft.outfitter.actions.promotion;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

public interface PromoteInAppPurchaseActions {
    void showInAppPurchaseDialogWhenAppropriate(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics);
}
