package com.silensoft.outfitter.actions.promotion.impl;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.promotion.InviteFriendsActions;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.utils.FailureReporter;

import static android.app.Activity.RESULT_OK;

public class InviteFriendsActionsImpl implements InviteFriendsActions {
    private static final int REQUEST_INVITE_FRIENDS = 4;

    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    @Override
    public void inviteFriends(@NonNull final Activity activity) {
        final Intent intent = new AppInviteInvitation.IntentBuilder(activity.getString(R.string.invitation_title))
                .setMessage(activity.getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(activity.getString(R.string.invitation_deep_link)))
                .setCustomImage(Uri.parse(activity.getString(R.string.invitation_custom_image)))
                .setCallToActionText(activity.getString(R.string.invitation_call_to_action_text))
                // TODO: RELEASE: set OAUTH client id of ios app
                // .setOtherPlatformsTargetApplication(AppInviteInvitation.IntentBuilder.PlatformMode.PROJECT_PLATFORM_IOS, "<oauthclientid>")
                .build();

        try {
            activity.startActivityForResult(intent, REQUEST_INVITE_FRIENDS);
        } catch (final ActivityNotFoundException exception) {
            failureReporter.reportFailure(exception);
        }
    }

    @Override
    public void logInvitedFriends(final int requestCode, final int resultCode, final Intent intent, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        if (requestCode == REQUEST_INVITE_FRIENDS) {
            if (resultCode == RESULT_OK) {
                final String[] ids = AppInviteInvitation.getInvitationIds(RESULT_OK, intent);
                for (final String ignored : ids) {
                    analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.INVITE_FRIEND);
                }
            } else {
                analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.INVITE_FRIENDS_FAILURE);
            }
        }
    }
}
