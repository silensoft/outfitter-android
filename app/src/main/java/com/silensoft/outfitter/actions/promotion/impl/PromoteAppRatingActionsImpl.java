package com.silensoft.outfitter.actions.promotion.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.actions.promotion.PromoteAppRatingActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.appusageinfo.AppUsageInfo;
import com.silensoft.outfitter.ui.dialogs.PromoteAppRatingDialog;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;

public class PromoteAppRatingActionsImpl implements PromoteAppRatingActions {
    public static final String GOOGLE_PLAY_STORE_MARKET_DETAILS_BASE_URI = "market://details?id=";
    public static final String GOOGLE_PLAY_STORE_WEB_SITE_BASE_URI = "http://play.google.com/store/apps/details?id=";

    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final AppUsageInfo appUsageInfo = DependencyManager.getInstance().getImplementationFor(AppUsageInfo.class);
    @SuppressWarnings("LawOfDemeter")
    private final PromoteAppRatingDialog promoteAppRatingDialog = DependencyManager.getInstance().getImplementationFor(PromoteAppRatingDialog.class);

    @Override
    public void showRateAppDialogWhenAppropriate(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics) {
       if (appUsageInfo.isAppRatingPromotionAppropriate(context)) {
           analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.APP_RATING_PROMOTION);
           promoteAppRatingDialog.showAndOnPositiveOrNegativeSelected(context,
                   () -> launchGooglePlayStore(context, firebaseAnalytics),
                   () -> {
                       analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.DENY_APP_RATING_PROMOTION);
                       appUsageInfo.denyAppRatingPromotion(context);
                   });
        }
    }

    private void launchGooglePlayStore(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        final Uri appPageInGooglePlayStoreUri = Uri.parse(GOOGLE_PLAY_STORE_MARKET_DETAILS_BASE_URI + context.getPackageName());
        final Intent launchGooglePlayStore = new Intent(Intent.ACTION_VIEW, appPageInGooglePlayStoreUri);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            launchGooglePlayStore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        } else {
            launchGooglePlayStore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }

        try {
            context.startActivity(launchGooglePlayStore);
        } catch (final ActivityNotFoundException exception) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_WEB_SITE_BASE_URI + context.getPackageName())));
        }

        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.APP_RATING);
    }
}
