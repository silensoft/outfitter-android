package com.silensoft.outfitter.actions.promotion.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.actions.promotion.PromoteInAppPurchaseActions;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.model.appusageinfo.AppUsageInfo;
import com.silensoft.outfitter.ui.activities.SettingsActivity;
import com.silensoft.outfitter.ui.dialogs.PromoteInAppPurchaseDialog;
import com.silensoft.outfitter.utils.FailureReporter;

public class PromoteInAppPurchaseActionsImpl implements PromoteInAppPurchaseActions {
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final AppUsageInfo appUsageInfo =  DependencyManager.getInstance().getImplementationFor(AppUsageInfo.class);
    @SuppressWarnings("LawOfDemeter")
    private final InAppBillingActions inAppBillingActions =  DependencyManager.getInstance().getImplementationFor(InAppBillingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final PromoteInAppPurchaseDialog promoteInAppPurchaseDialog =  DependencyManager.getInstance().getImplementationFor(PromoteInAppPurchaseDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    @Override
    public void showInAppPurchaseDialogWhenAppropriate(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        inAppBillingActions.fetchPromotableSku(sku -> {
            if (sku.getType() != InAppBillingSkuType.NONE && appUsageInfo.isInAppPurchasePromotionAppropriate(context)) {
                analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.IN_APP_PURCHASE_PROMOTION);

                promoteInAppPurchaseDialog.showAndOnPositiveOrNegativeSelected(context, sku,
                        () -> {
                            inAppBillingActions.purchase(context, sku);
                            if (sku.getType() == InAppBillingSkuType.PRO_SUBSCRIPTION) {
                                final Intent settingsActivityIntent = new Intent(context, SettingsActivity.class);
                                try {
                                    context.startActivity(settingsActivityIntent);
                                } catch (final ActivityNotFoundException exception) {
                                    failureReporter.reportFailure(exception);
                                }
                            }
                        },
                        () -> {
                            analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.DENY_IN_APP_PURCHASE_PROMOTION);
                            appUsageInfo.denyInAppPurchasePromotion(context);
                        });
            }
        });
    }
}
