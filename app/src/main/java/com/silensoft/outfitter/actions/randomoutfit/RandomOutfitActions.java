package com.silensoft.outfitter.actions.randomoutfit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.types.Optional;

public interface RandomOutfitActions {
    void restartPresenting();
    void stopPresenting();

    int getOutfitCount(@NonNull final Context context);
    @NonNull Optional<Outfit> getCurrentOutfit();

    Optional<Outfit> getNextRandomOutfit(@NonNull final Context context);
    Optional<Outfit> getPreviousViewedOutfit(@NonNull final Context context);

    void addOutfit(@NonNull final Outfit outfit);
    void removeCurrentOutfit();

    void saveCurrentSeasonAndOccasionTo(@NonNull final Bundle bundle);
    void setCurrentSeasonAndOccasionFrom(@NonNull final Bundle bundle);
}
