package com.silensoft.outfitter.actions.randomoutfit;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.OutfitOccasion;

public interface RandomOutfitOccasionActions {
    OutfitOccasion getCurrentOccasion();
    int getCurrentOccasionIndex();
    @NonNull String getCurrentOccasionStr(@NonNull final Context context);
    void setCurrentOccasion(final OutfitOccasion outfitOccasion);
}
