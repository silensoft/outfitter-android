package com.silensoft.outfitter.actions.randomoutfit;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.OutfitSeason;

public interface RandomOutfitSeasonActions {
    OutfitSeason getCurrentSeason();
    int getCurrentSeasonIndex();
    @NonNull String getCurrentSeasonStr(@NonNull final Context context);
    void setCurrentSeason(final OutfitSeason outfitOccasion);
}
