package com.silensoft.outfitter.actions.randomoutfit.impl;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.model.dao.OutfitDao;
import com.silensoft.outfitter.types.Functions;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.reactivex.Observable;

public class RandomOutfitActionsImpl implements RandomOutfitActions {
    public static final int VIEWED_OUTFITS_INITIAL_CAPACITY = 50;

    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitOccasionActions randomOutfitOccasionActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitOccasionActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitSeasonActions randomOutfitSeasonActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitSeasonActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitDao outfitDao = DependencyManager.getInstance().getImplementationFor(OutfitDao.class);

    private final Map<OutfitOccasion, Map<OutfitSeason, Optional<Integer>>> occasionAndSeasonToCurrentOutfitIndexMap = new EnumMap<>(OutfitOccasion.class);
    private final Map<OutfitOccasion, Map<OutfitSeason, List<Outfit>>> occasionAndSeasonToViewedOutfitsMap = new EnumMap<>(OutfitOccasion.class);
    private final Map<OutfitOccasion, Map<OutfitSeason, Optional<Cursor>>> occasionAndSeasonToDatabaseCursorMap = new EnumMap<>(OutfitOccasion.class);

    public RandomOutfitActionsImpl() {
        restartPresenting();
    }

    @Override
    public final void restartPresenting() {
        for (final OutfitOccasion outfitOccasion : OutfitOccasion.values()) {
            for (final OutfitSeason outfitSeason : OutfitSeason.values()) {
                initializeCurrentOutfitIndexMap(outfitOccasion, outfitSeason);
                initializeViewedOutfitsMap(outfitOccasion, outfitSeason);
                initializeDatabaseCursorMap(outfitOccasion, outfitSeason);
            }
        }
    }

    @Override
    public void stopPresenting() {
        for (final OutfitOccasion outfitOccasion : OutfitOccasion.values()) {
            for (final OutfitSeason outfitSeason : OutfitSeason.values()) {
                occasionAndSeasonToDatabaseCursorMap.get(outfitOccasion).get(outfitSeason).ifPresent(Cursor::close);
            }
        }
    }

    @SuppressWarnings("LawOfDemeter")
    @Override
    public int getOutfitCount(@NonNull final Context context) {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        return occasionAndSeasonToDatabaseCursorMap.get(outfitOccasion).get(outfitSeason)
                .orElseOptional(() -> {
                    final Optional<Cursor> newCursor = outfitDao.findAllOutfitsByOccasionAndSeason(context, outfitOccasion, outfitSeason);
                    occasionAndSeasonToDatabaseCursorMap.get(outfitOccasion).put(outfitSeason, newCursor);
                    return newCursor;
                }).mapOrElse(Cursor::getCount, 0);
    }

    @NonNull
    @Override
    public Optional<Outfit> getCurrentOutfit() {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        return occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).get(outfitSeason)
                .map((currentOutfitIndex) -> occasionAndSeasonToViewedOutfitsMap.get(outfitOccasion).get(outfitSeason).get(currentOutfitIndex));
    }

    @SuppressWarnings({"LawOfDemeter", "FeatureEnvy"})
    @NonNull
    @Override
    public Optional<Outfit> getNextRandomOutfit(@NonNull final Context context) {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        return occasionAndSeasonToDatabaseCursorMap.get(outfitOccasion).get(outfitSeason)
                .orElseOptional(() -> {
                    final Optional<Cursor> newCursor = outfitDao.findAllOutfitsByOccasionAndSeason(context, outfitOccasion, outfitSeason);
                    occasionAndSeasonToDatabaseCursorMap.get(outfitOccasion).put(outfitSeason, newCursor);
                    return newCursor;
                }).flatMap(cursor -> {
                    final Optional<Outfit> optOutfit = findNextRandomOutfit(cursor);
                    optOutfit.ifPresentOrElse(this::addViewedOutfit, this::restartPresenting);
                    return optOutfit;
                });
    }

    @SuppressWarnings({"FeatureEnvy", "LawOfDemeter"})
    @NonNull
    @Override
    public Optional<Outfit> getPreviousViewedOutfit(@NonNull final Context context) {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        return occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).get(outfitSeason)
                .filter(Functions::isGreaterThanZero)
                .map(Functions::decrement)
                .flatMap(currentOutfitIndex -> {
                    occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).put(outfitSeason, OptionalImpl.of(currentOutfitIndex));
                    return getCurrentOutfit();
                });
    }

    @Override
    public void addOutfit(@NonNull final Outfit outfit) {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        final List<Outfit> viewedOutfits = occasionAndSeasonToViewedOutfitsMap.get(outfitOccasion).get(outfitSeason);
        occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).get(outfitSeason)
                .ifPresent((currentOutfitIndex) -> viewedOutfits.add(currentOutfitIndex, outfit));
    }

    @Override
    public void removeCurrentOutfit() {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        final List<Outfit> viewedOutfits = occasionAndSeasonToViewedOutfitsMap.get(outfitOccasion).get(outfitSeason);
        occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).get(outfitSeason)
                .ifPresent((currentOutfitIndex) -> viewedOutfits.remove(currentOutfitIndex.intValue()));
        occasionAndSeasonToDatabaseCursorMap.get(outfitOccasion).put(outfitSeason, OptionalImpl.empty());

    }

    @Override
    public void saveCurrentSeasonAndOccasionTo(@NonNull final Bundle bundle) {
        bundle.putInt(Outfit.METADATA_KEY_SEASONS, randomOutfitSeasonActions.getCurrentSeasonIndex());
        bundle.putInt(Outfit.METADATA_KEY_OCCASIONS, randomOutfitOccasionActions.getCurrentOccasionIndex());
    }

    @Override
    public void setCurrentSeasonAndOccasionFrom(@NonNull final Bundle bundle) {
        final OutfitSeason outfitSeason = OutfitSeason.fromIndex(bundle.getInt(Outfit.METADATA_KEY_SEASONS));
        randomOutfitSeasonActions.setCurrentSeason(outfitSeason);
        final OutfitOccasion outfitOccasion = OutfitOccasion.fromIndex(bundle.getInt(Outfit.METADATA_KEY_OCCASIONS));
        randomOutfitOccasionActions.setCurrentOccasion(outfitOccasion);
    }

    private void initializeViewedOutfitsMap(@NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason) {
        final Map<OutfitSeason, List<Outfit>> seasonToViewedOutfitsMap = new EnumMap<>(OutfitSeason.class);
        seasonToViewedOutfitsMap.put(outfitSeason, new ArrayList<>(VIEWED_OUTFITS_INITIAL_CAPACITY));
        occasionAndSeasonToViewedOutfitsMap.put(outfitOccasion, seasonToViewedOutfitsMap);
    }

    private void initializeCurrentOutfitIndexMap(@NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason) {
        final Map<OutfitSeason, Optional<Integer>> seasonToCurrentOutfitIndexMap = new EnumMap<>(OutfitSeason.class);
        seasonToCurrentOutfitIndexMap.put(outfitSeason, OptionalImpl.empty());
        occasionAndSeasonToCurrentOutfitIndexMap.put(outfitOccasion, seasonToCurrentOutfitIndexMap);
    }

    private void initializeDatabaseCursorMap(@NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason) {
        final Map<OutfitSeason, Optional<Cursor>> seasonToDatabaseCursorMap = new EnumMap<>(OutfitSeason.class);
        seasonToDatabaseCursorMap.put(outfitSeason, OptionalImpl.empty());
        occasionAndSeasonToDatabaseCursorMap.put(outfitOccasion, seasonToDatabaseCursorMap);
    }

    private void addViewedOutfit(@NonNull final Outfit outfit) {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
        final List<Outfit> viewedOutfits = occasionAndSeasonToViewedOutfitsMap.get(outfitOccasion).get(outfitSeason);
        viewedOutfits.add(outfit);
        final int currentOutfitIndex = occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).get(outfitSeason).orElse(0);
        occasionAndSeasonToCurrentOutfitIndexMap.get(outfitOccasion).put(outfitSeason, OptionalImpl.of(currentOutfitIndex + 1));
    }

    @SuppressWarnings("LawOfDemeter")
    @NonNull
    private Optional<Outfit> findNextRandomOutfit(@NonNull final Cursor cursor) {
        final int numberOfOutfits = outfitDao.getNumberOfOutfits(cursor);
        final Random randomNumberGenerator = new Random();
        Optional<Outfit> optionalOutfit;

        while (true) {
            final int randomOutfitIndex = randomNumberGenerator.nextInt(numberOfOutfits);
            optionalOutfit = outfitDao.getOutfitFromCursorIndex(cursor, randomOutfitIndex);
            if (optionalOutfit.mapOrElse(this::outfitIsNotYetViewed, true)) {
                break;
            }
        }

        return optionalOutfit;
    }

    private boolean isOutfitAlreadyViewed(@NonNull final Outfit outfitToBeViewed) {
        final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
        final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();

        return Observable.fromIterable(occasionAndSeasonToViewedOutfitsMap.get(outfitOccasion).get(outfitSeason))
                .any(viewedOutfit -> outfitToBeViewed.getUuidStr().equals(viewedOutfit.getUuidStr())).blockingGet();
    }

    private boolean outfitIsNotYetViewed(@NonNull final Outfit outfitToBeViewed) {
        return !isOutfitAlreadyViewed(outfitToBeViewed);
    }
}
