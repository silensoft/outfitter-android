package com.silensoft.outfitter.actions.randomoutfit.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;

public class RandomOutfitOccasionActionsImpl implements RandomOutfitOccasionActions {
    private OutfitOccasion outfitOccasion;

    public RandomOutfitOccasionActionsImpl() {
        outfitOccasion = OutfitOccasion.LEISURE;
    }

    @NonNull
    @Override
    public OutfitOccasion getCurrentOccasion() {
        return outfitOccasion;
    }

    @Override
    public int getCurrentOccasionIndex() {
        return outfitOccasion.getIndex();
    }

    @NonNull
    @Override
    public String getCurrentOccasionStr(@NonNull final Context context) {
        return outfitOccasion.toString(context);
    }

    @Override
    public void setCurrentOccasion(@NonNull final OutfitOccasion outfitOccasion_) {
        outfitOccasion = outfitOccasion_;
    }
}
