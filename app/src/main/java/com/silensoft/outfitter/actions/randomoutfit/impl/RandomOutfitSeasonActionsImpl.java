package com.silensoft.outfitter.actions.randomoutfit.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.model.outfit.OutfitSeason;

public class RandomOutfitSeasonActionsImpl implements RandomOutfitSeasonActions {
    private OutfitSeason outfitSeason = OutfitSeason.SUMMER;

    @NonNull
    @Override
    public OutfitSeason getCurrentSeason() {
        return outfitSeason;
    }

    @Override
    public int getCurrentSeasonIndex() {
        return outfitSeason.getIndex();
    }

    @NonNull
    @Override
    public String getCurrentSeasonStr(@NonNull final Context context) {
        return outfitSeason.toString(context);
    }

    @Override
    public void setCurrentSeason(@NonNull final OutfitSeason outfitSeason_) {
        this.outfitSeason = outfitSeason_;
    }
}
