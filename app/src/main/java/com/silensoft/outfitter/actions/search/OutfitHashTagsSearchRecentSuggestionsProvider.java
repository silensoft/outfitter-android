package com.silensoft.outfitter.actions.search;

import android.content.SearchRecentSuggestionsProvider;

public class OutfitHashTagsSearchRecentSuggestionsProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "com.silensoft.outfitter.OutfitHashTagsSearchRecentSuggestionsProvider";
    public static final int MODE = DATABASE_MODE_QUERIES;

    public OutfitHashTagsSearchRecentSuggestionsProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
