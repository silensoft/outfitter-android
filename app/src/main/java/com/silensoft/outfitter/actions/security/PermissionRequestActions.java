package com.silensoft.outfitter.actions.security;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Action;

public interface PermissionRequestActions {
    int requestPermissionOrPerformAction(@NonNull final Activity activity, @NonNull final String permission, @NonNull final Action successAction);
}
