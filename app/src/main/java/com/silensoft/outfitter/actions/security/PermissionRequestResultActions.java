package com.silensoft.outfitter.actions.security;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Action;

public interface PermissionRequestResultActions {
    void handlePermissionRequestResult(@NonNull final Context context, @NonNull final int[] grantResults,
                                       @NonNull final Action successAction, final int failureStringResourceId);
}
