package com.silensoft.outfitter.actions.security.impl;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.silensoft.outfitter.actions.security.PermissionRequestActions;
import com.silensoft.outfitter.types.Action;

public class PermissionRequestActionsImpl implements PermissionRequestActions {
    private static int requestId = 1;

    @Override
    public int requestPermissionOrPerformAction(@NonNull final Activity activity, @NonNull final String permission, @NonNull final Action successAction) {
        if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED) {
            successAction.perform();
            return -1;
        } else {
            ActivityCompat.requestPermissions(activity, new String[] { permission }, requestId);
            final int currentRequestId = requestId;
            requestId++;
            return currentRequestId;
        }
    }
}
