package com.silensoft.outfitter.actions.security.impl;


import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.silensoft.outfitter.actions.security.PermissionRequestResultActions;
import com.silensoft.outfitter.types.Action;

public class PermissionRequestResultActionsImpl implements PermissionRequestResultActions {
    @Override
    public void handlePermissionRequestResult(@NonNull final Context context, @NonNull final int[] grantResults,
                                              @NonNull final Action successAction, final int failureStringResourceId) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            successAction.perform();
        } else {
            Toast.makeText(context, failureStringResourceId, Toast.LENGTH_SHORT).show();
        }
    }
}
