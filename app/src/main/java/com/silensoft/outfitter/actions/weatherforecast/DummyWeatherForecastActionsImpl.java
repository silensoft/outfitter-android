package com.silensoft.outfitter.actions.weatherforecast;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.dependencymanagement.bindings.ModelBindingsImpl;
import com.silensoft.outfitter.model.factory.ThreeHourWeatherForecastFactory;
import com.silensoft.outfitter.model.factory.impl.DummyThreeHourWeatherForecastFactoryImpl;
import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.types.Consumer;

import java.util.List;

import io.reactivex.Observable;

public class DummyWeatherForecastActionsImpl implements WeatherForecastActions {
    @SuppressWarnings("LawOfDemeter")
    private final ThreeHourWeatherForecastFactory dummyThreeHourWeatherForecastFactory =
            DependencyManager.getInstance().getImplementationFor(DummyThreeHourWeatherForecastFactoryImpl.class, ModelBindingsImpl.DUMMY_THREE_HOUR_WEATHER_FORECAST_FACTORY);

    @Override
    public void fetchThreeHourWeatherForecastsForNext15Hours(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics,
                                                             @NonNull final Consumer<List<ThreeHourWeatherForecast>> consumer) {

        final List<ThreeHourWeatherForecast> dummyThreeHourWeatherForecasts = Observable.range(1, 5)
                .map(ignored-> dummyThreeHourWeatherForecastFactory.createWeatherForecast(context, new com.silensoft.outfitter.webapis.openweathermap.model.List()))
                .toList()
                .blockingGet();

        consumer.consume(dummyThreeHourWeatherForecasts);
    }
}
