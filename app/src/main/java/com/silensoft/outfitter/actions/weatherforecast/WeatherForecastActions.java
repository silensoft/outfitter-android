package com.silensoft.outfitter.actions.weatherforecast;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.types.Consumer;

import java.util.List;

public interface WeatherForecastActions {
    void fetchThreeHourWeatherForecastsForNext15Hours(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics,
                                                      @NonNull final Consumer<List<ThreeHourWeatherForecast>> consumer);
}
