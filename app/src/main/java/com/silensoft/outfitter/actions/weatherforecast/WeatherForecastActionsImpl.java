package com.silensoft.outfitter.actions.weatherforecast;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.geolocation.GeoLocationActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.factory.ThreeHourWeatherForecastFactory;
import com.silensoft.outfitter.types.Consumer;
import com.silensoft.outfitter.utils.NetworkUtils;
import com.silensoft.outfitter.webapis.openweathermap.OpenWeatherMapWebApi;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.webapis.openweathermap.model.WeatherForecastData;
import com.silensoft.outfitter.utils.FailureReporter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherForecastActionsImpl implements WeatherForecastActions {
    private static final String BASE_URL = "http://api.openweathermap.org";

    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);
    @SuppressWarnings("LawOfDemeter")
    private final ThreeHourWeatherForecastFactory threeHourWeatherForecastFactory = DependencyManager.getInstance().getImplementationFor(ThreeHourWeatherForecastFactory.class);
    @SuppressWarnings("LawOfDemeter")
    private final GeoLocationActions geoLocationActions = DependencyManager.getInstance().getImplementationFor(GeoLocationActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final NetworkUtils networkUtils = DependencyManager.getInstance().getImplementationFor(NetworkUtils.class);

    private final OpenWeatherMapWebApi openWeatherMapWebApiImpl;

    public WeatherForecastActionsImpl() {
        final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        openWeatherMapWebApiImpl = retrofit.create(OpenWeatherMapWebApi.class);
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    public void fetchThreeHourWeatherForecastsForNext15Hours(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics,
                                                             @NonNull final Consumer<List<ThreeHourWeatherForecast>> consumer) {
        if (networkUtils.hasInternetConnection(context)) {
            return;
        }

        geoLocationActions.getGeoLocation().ifPresent(geoLocation -> {
            final String latitudeStr = String.valueOf(geoLocation.getLatitude());
            final String longitudeStr = String.valueOf(geoLocation.getLongitude());

            openWeatherMapWebApiImpl.getWeatherForecastData(latitudeStr, longitudeStr).enqueue(new Callback<WeatherForecastData>() {
                @Override
                public void onResponse(@NonNull final Call<WeatherForecastData> call, @NonNull final Response<WeatherForecastData> response) {
                    consumer.consume(parseThreeHourWeatherForecastsFrom(context, response));
                }

                @Override
                public void onFailure(@NonNull final Call<WeatherForecastData> call, @NonNull final Throwable throwable) {
                    failureReporter.reportFailure(throwable);
                    Toast.makeText(context, R.string.failed_to_update_weather_forecast, Toast.LENGTH_SHORT).show();
                }
            });

            analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.WEATHER_FORECAST_REQUESTED);
        });
    }

    @SuppressWarnings({"LocalVariableOfConcreteClass", "LawOfDemeter"}) // POJO Access
    @NonNull
    private List<ThreeHourWeatherForecast> parseThreeHourWeatherForecastsFrom(@NonNull final Context context, @NonNull final Response<WeatherForecastData> response) {
        final WeatherForecastData weatherForecastData = response.body();
        final List<ThreeHourWeatherForecast> threeHourWeatherForecasts = new ArrayList<>(5);
        for (int i = 0; i < weatherForecastData.getList().size() && i < 5; i++) {
            final com.silensoft.outfitter.webapis.openweathermap.model.List threeHourWeatherForecast = weatherForecastData.getList().get(i);
            threeHourWeatherForecasts.add(threeHourWeatherForecastFactory.createWeatherForecast(context, threeHourWeatherForecast));
        }
        return threeHourWeatherForecasts;
    }
}
