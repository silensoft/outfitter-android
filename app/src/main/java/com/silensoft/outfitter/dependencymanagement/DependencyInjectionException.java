package com.silensoft.outfitter.dependencymanagement;

import android.support.annotation.Nullable;

public class DependencyInjectionException extends RuntimeException {

    public DependencyInjectionException(@Nullable final String message) {
        super(message);
    }

}
