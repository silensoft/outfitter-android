package com.silensoft.outfitter.dependencymanagement;

import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.silensoft.outfitter.dependencymanagement.bindings.ActionBindingsImpl;
import com.silensoft.outfitter.dependencymanagement.bindings.ModelBindingsImpl;
import com.silensoft.outfitter.dependencymanagement.bindings.UiBindingsImpl;
import com.silensoft.outfitter.dependencymanagement.bindings.UtilBindingsImpl;
import com.silensoft.outfitter.dependencymanagement.implementation.Implementation;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"StaticVariableOfConcreteClass", "MethodReturnOfConcreteClass"})
public class DependencyManager {
    private static Optional<DependencyManager> instance = OptionalImpl.empty();

    private final Map<Class<?>, Implementation> interfaceToImplementationMap = new HashMap<>(100);

    @SuppressWarnings("NonThreadSafeLazyInitialization")  // Should be only called from single thread (UI thread)
    public static DependencyManager getInstance() {
        if (!instance.isPresent()) {
            instance = OptionalImpl.of(new DependencyManager());
        }
        return instance.get();
    }

    @SuppressWarnings("FeatureEnvy")
    protected DependencyManager() {
        interfaceToImplementationMap.putAll(new ModelBindingsImpl().getBindings());
        interfaceToImplementationMap.putAll(new ActionBindingsImpl().getBindings());
        interfaceToImplementationMap.putAll(new UiBindingsImpl().getBindings());
        interfaceToImplementationMap.putAll(new UtilBindingsImpl().getBindings());
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    public <T> T getImplementationFor(@NonNull final Class<T> interfaceClass) {
        Preconditions.checkArgument(interfaceClass.isInterface());
        final Implementation implementation = interfaceToImplementationMap.get(interfaceClass);
        final Optional<Object> implementationInstance = implementation.getImplementationInstance("");
        return castImplementationInstance(implementationInstance, interfaceClass);
    }

    public <T> T getImplementationFor(@NonNull final Class<T> interfaceClass, @NonNull final String name) {
        Preconditions.checkArgument(interfaceClass.isInterface());
        final Implementation implementation = interfaceToImplementationMap.get(interfaceClass);
        final Optional<Object> implementationInstance = implementation.getImplementationInstance(name);
        return castImplementationInstance(implementationInstance, interfaceClass);
    }

    private static <T> T castImplementationInstance(@NonNull final Optional<Object> implementationInstance, @NonNull final Class<T> interfaceClass) {
        if (!implementationInstance.isPresent()) {
            throw new DependencyInjectionException("Missing binding for interface " + interfaceClass.getName());
        }

        try {
            return interfaceClass.cast(implementationInstance.get());
        } catch(final ClassCastException exception) {
            throw new DependencyInjectionException("Invalid binding for interface" + interfaceClass.getName());
        }
    }
}
