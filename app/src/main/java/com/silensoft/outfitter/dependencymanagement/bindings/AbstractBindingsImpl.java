package com.silensoft.outfitter.dependencymanagement.bindings;

import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableMap;
import com.silensoft.outfitter.dependencymanagement.implementation.Implementation;
import com.silensoft.outfitter.dependencymanagement.implementation.NamedImplementationsImpl;
import com.silensoft.outfitter.dependencymanagement.implementation.SingletonImplementationImpl;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractBindingsImpl implements Bindings {

    private final Map<Class<?>, Implementation> interfaceToImplementationMap = new HashMap<>(100);

    public void bindInterfaceToImplementionInstance(@NonNull final Class<?> interfaceClass, @NonNull final Object implementationInstance) {
        interfaceToImplementationMap.put(interfaceClass, new SingletonImplementationImpl(implementationInstance));
    }

    public void bindInterfaceToImplementionInstanceWithName(@NonNull final Class<?> interfaceClass, @NonNull final Object implementationInstance, @NonNull final String name) {
        Implementation implementation = interfaceToImplementationMap.get(interfaceClass);

        if (implementation == null) {
            implementation = new NamedImplementationsImpl();
        }

        implementation.addImplementationInstance(name, implementationInstance);
        interfaceToImplementationMap.put(interfaceClass, implementation);
    }

    @NonNull
    @Override
    public Map<Class<?>, Implementation> getBindings() {
        return ImmutableMap.copyOf(interfaceToImplementationMap);
    }
}
