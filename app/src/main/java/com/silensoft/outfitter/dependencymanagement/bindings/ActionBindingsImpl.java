package com.silensoft.outfitter.dependencymanagement.bindings;


import com.silensoft.outfitter.actions.ad.BannerAdActions;
import com.silensoft.outfitter.actions.ad.InterstitialAdActions;
import com.silensoft.outfitter.actions.ad.impl.GoogleBannerAdActionsImpl;
import com.silensoft.outfitter.actions.ad.impl.GoogleInterstitialAdActionsImpl;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.actions.analytics.FirebaseAnalyticsEventLoggerImpl;
import com.silensoft.outfitter.actions.appusage.AppUsageActions;
import com.silensoft.outfitter.actions.appusage.AppUsageActionsImpl;
import com.silensoft.outfitter.actions.cloud.OutfitsCloudStorageActions;
import com.silensoft.outfitter.actions.cloud.GoogleDriveOutfitsCloudStorageActionsImpl;
import com.silensoft.outfitter.actions.geolocation.GeoLocationActions;
import com.silensoft.outfitter.actions.geolocation.GeoLocationActionsImpl;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActionsImpl;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.actions.outfit.OutfitActionsImpl;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridActions;
import com.silensoft.outfitter.actions.outfitgrid.impl.OutfitGridActionsImpl;
import com.silensoft.outfitter.actions.promotion.InviteFriendsActions;
import com.silensoft.outfitter.actions.promotion.PromoteAppRatingActions;
import com.silensoft.outfitter.actions.promotion.PromoteInAppPurchaseActions;
import com.silensoft.outfitter.actions.promotion.impl.InviteFriendsActionsImpl;
import com.silensoft.outfitter.actions.promotion.impl.PromoteAppRatingActionsImpl;
import com.silensoft.outfitter.actions.promotion.impl.PromoteInAppPurchaseActionsImpl;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.actions.randomoutfit.impl.RandomOutfitActionsImpl;
import com.silensoft.outfitter.actions.randomoutfit.impl.RandomOutfitOccasionActionsImpl;
import com.silensoft.outfitter.actions.randomoutfit.impl.RandomOutfitSeasonActionsImpl;
import com.silensoft.outfitter.actions.security.PermissionRequestActions;
import com.silensoft.outfitter.actions.security.PermissionRequestResultActions;
import com.silensoft.outfitter.actions.security.impl.PermissionRequestActionsImpl;
import com.silensoft.outfitter.actions.security.impl.PermissionRequestResultActionsImpl;
import com.silensoft.outfitter.actions.weatherforecast.DummyWeatherForecastActionsImpl;
import com.silensoft.outfitter.actions.weatherforecast.WeatherForecastActions;
import com.silensoft.outfitter.actions.weatherforecast.WeatherForecastActionsImpl;

public class ActionBindingsImpl extends AbstractBindingsImpl {
    @Override
    public void configureBindings() {
        bindInterfaceToImplementionInstance(InterstitialAdActions.class, new GoogleInterstitialAdActionsImpl());
        bindInterfaceToImplementionInstance(BannerAdActions.class, new GoogleBannerAdActionsImpl());
        bindInterfaceToImplementionInstance(AppUsageActions.class, new AppUsageActionsImpl());
        bindInterfaceToImplementionInstance(InAppBillingActions.class, new InAppBillingActionsImpl());
        bindInterfaceToImplementionInstance(InviteFriendsActions.class, new InviteFriendsActionsImpl());
        bindInterfaceToImplementionInstance(OutfitActions.class, new OutfitActionsImpl());
        bindInterfaceToImplementionInstance(OutfitsCloudStorageActions.class, new GoogleDriveOutfitsCloudStorageActionsImpl());
        bindInterfaceToImplementionInstance(OutfitGridActions.class, new OutfitGridActionsImpl());
        bindInterfaceToImplementionInstance(PermissionRequestActions.class, new PermissionRequestActionsImpl());
        bindInterfaceToImplementionInstance(PermissionRequestResultActions.class, new PermissionRequestResultActionsImpl());
        bindInterfaceToImplementionInstance(PromoteAppRatingActions.class, new PromoteAppRatingActionsImpl());
        bindInterfaceToImplementionInstance(PromoteInAppPurchaseActions.class, new PromoteInAppPurchaseActionsImpl());
        bindInterfaceToImplementionInstance(RandomOutfitOccasionActions.class, new RandomOutfitOccasionActionsImpl());
        bindInterfaceToImplementionInstance(RandomOutfitSeasonActions.class, new RandomOutfitSeasonActionsImpl());
        bindInterfaceToImplementionInstance(RandomOutfitActions.class, new RandomOutfitActionsImpl());
        bindInterfaceToImplementionInstance(WeatherForecastActions.class, new DummyWeatherForecastActionsImpl());
        // TODO: bindInterfaceToImplementionInstance(WeatherForecastActions.class, new WeatherForecastActionsImpl());
        bindInterfaceToImplementionInstance(GeoLocationActions.class, new GeoLocationActionsImpl());
        bindInterfaceToImplementionInstance(AnalyticsEventLogger.class, new FirebaseAnalyticsEventLoggerImpl());
    }
}
