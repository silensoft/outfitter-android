package com.silensoft.outfitter.dependencymanagement.bindings;

import android.support.annotation.NonNull;

import com.silensoft.outfitter.dependencymanagement.implementation.Implementation;

import java.util.Map;

public interface Bindings {
    void configureBindings();
    @NonNull Map<Class<?>, Implementation> getBindings();
}
