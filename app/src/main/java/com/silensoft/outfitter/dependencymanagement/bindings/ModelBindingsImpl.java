package com.silensoft.outfitter.dependencymanagement.bindings;

import com.silensoft.outfitter.model.dao.OutfitCloudDao;
import com.silensoft.outfitter.model.dao.OutfitDao;
import com.silensoft.outfitter.model.dao.OutfitPhotoDao;
import com.silensoft.outfitter.model.dao.ToBeRemovedFromCloudOutfitDao;
import com.silensoft.outfitter.model.dao.ToBeSyncedToCloudOutfitDao;
import com.silensoft.outfitter.model.dao.impl.GoogleDriveOutfitCloudDaoImpl;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.model.factory.ThreeHourWeatherForecastFactory;
import com.silensoft.outfitter.model.dao.impl.SqliteDatabaseOutfitDaoImpl;
import com.silensoft.outfitter.model.dao.impl.SqliteDatabaseToBeRemovedFromCloudOutfitDaoImpl;
import com.silensoft.outfitter.model.dao.impl.SqliteDatabaseToBeSyncedToCloudOutfitDaoImpl;
import com.silensoft.outfitter.model.factory.impl.DummyThreeHourWeatherForecastFactoryImpl;
import com.silensoft.outfitter.model.factory.impl.OutfitFactoryImpl;
import com.silensoft.outfitter.model.factory.impl.ThreeHourWeatherForecastFactoryImpl;
import com.silensoft.outfitter.model.dao.impl.FilesystemOutfitPhotoDaoImpl;
import com.silensoft.outfitter.model.appusageinfo.SharedPreferencesAppUsageInfoImpl;
import com.silensoft.outfitter.model.usersettings.SharedPreferencesUserSettingsStoreImpl;
import com.silensoft.outfitter.model.appusageinfo.AppUsageInfo;
import com.silensoft.outfitter.model.usersettings.UserSettingsStore;

public class ModelBindingsImpl extends AbstractBindingsImpl {
    public static final String DUMMY_THREE_HOUR_WEATHER_FORECAST_FACTORY = "dummyThreeHourWeatherForecastFactory";

    @Override
    public void configureBindings() {
        bindInterfaceToImplementionInstance(OutfitDao.class, new SqliteDatabaseOutfitDaoImpl());
        bindInterfaceToImplementionInstance(ToBeSyncedToCloudOutfitDao.class, new SqliteDatabaseToBeSyncedToCloudOutfitDaoImpl());
        bindInterfaceToImplementionInstance(ToBeRemovedFromCloudOutfitDao.class, new SqliteDatabaseToBeRemovedFromCloudOutfitDaoImpl());
        bindInterfaceToImplementionInstance(OutfitPhotoDao.class, new FilesystemOutfitPhotoDaoImpl());
        bindInterfaceToImplementionInstance(OutfitCloudDao.class, new GoogleDriveOutfitCloudDaoImpl());
        bindInterfaceToImplementionInstance(OutfitFactory.class, new OutfitFactoryImpl());
        bindInterfaceToImplementionInstance(AppUsageInfo.class, new SharedPreferencesAppUsageInfoImpl());
        bindInterfaceToImplementionInstance(ThreeHourWeatherForecastFactory.class, new ThreeHourWeatherForecastFactoryImpl());
        bindInterfaceToImplementionInstance(UserSettingsStore.class, new SharedPreferencesUserSettingsStoreImpl());
        bindInterfaceToImplementionInstanceWithName(ThreeHourWeatherForecastFactory.class, new DummyThreeHourWeatherForecastFactoryImpl(), DUMMY_THREE_HOUR_WEATHER_FORECAST_FACTORY);
    }
}
