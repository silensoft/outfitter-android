package com.silensoft.outfitter.dependencymanagement.bindings;

import com.silensoft.outfitter.actions.outfitgrid.OutfitGridLoaderManager;
import com.silensoft.outfitter.actions.outfitgrid.impl.OutfitGridLoaderManagerImpl;
import com.silensoft.outfitter.ui.dialogs.ChooseOutfitPhotoSourceDialog;
import com.silensoft.outfitter.ui.dialogs.ClearSearchHistoryDialog;
import com.silensoft.outfitter.ui.dialogs.DeleteOutfitConfirmationDialog;
import com.silensoft.outfitter.ui.dialogs.DiscardHashTagChangesDialog;
import com.silensoft.outfitter.ui.dialogs.DiscardOutfitChangesDialog;
import com.silensoft.outfitter.ui.dialogs.NoMoreOutfitsToShowDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionsSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonsSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.PromoteAppRatingDialog;
import com.silensoft.outfitter.ui.dialogs.PromoteInAppPurchaseDialog;
import com.silensoft.outfitter.ui.dialogs.PurchaseSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.impl.ChooseOutfitPhotoSourceDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.ClearSearchHistoryDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.DeleteOutfitConfirmationDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.DiscardHashTagChangesDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.DiscardOutfitChangesDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.NoMoreOutfitsToShowDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.OutfitOccasionSelectorDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.OutfitOccasionsSelectorDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.OutfitSeasonSelectorDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.OutfitSeasonsSelectorDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.PromoteAppRatingDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.PromoteInAppPurchaseDialogImpl;
import com.silensoft.outfitter.ui.dialogs.impl.PurchaseSelectorDialogImpl;
import com.silensoft.outfitter.ui.managers.CustomAppTitleStyleManager;
import com.silensoft.outfitter.ui.managers.FullScreenContentManager;
import com.silensoft.outfitter.ui.managers.SwipeLeftOrRightImageSwitcherManager;
import com.silensoft.outfitter.ui.managers.impl.CustomAppTitleStyleManagerImpl;
import com.silensoft.outfitter.ui.managers.impl.FullScreenContentManagerImpl;
import com.silensoft.outfitter.ui.managers.impl.SwipeLeftOrRightImageSwitcherManagerImpl;
import com.silensoft.outfitter.ui.menus.MenuInflater;
import com.silensoft.outfitter.ui.menus.impl.MenuInflaterImpl;
import com.silensoft.outfitter.ui.presenters.AdPresenter;
import com.silensoft.outfitter.ui.presenters.CurrentOccasionAndSeasonPresenter;
import com.silensoft.outfitter.ui.presenters.RandomOutfitPresenter;
import com.silensoft.outfitter.ui.presenters.WeatherForecastPresenter;
import com.silensoft.outfitter.ui.presenters.impl.CurrentOccasionAndSeasonPresenterImpl;
import com.silensoft.outfitter.ui.presenters.impl.FrequencyBasedAdPresenterImpl;
import com.silensoft.outfitter.ui.presenters.impl.RandomOutfitPresenterImpl;
import com.silensoft.outfitter.ui.presenters.impl.WeatherForecastPresenterImpl;

public class UiBindingsImpl extends AbstractBindingsImpl {
    public static final String NEXT_RANDOM_OUTFIT_AD_PRESENTER = "nextRandomOutfitAdPresenter";
    public static final String PREVIOUS_VIEWED_OUTFIT_AD_PRESENTER = "previousViewedOutfitAdPresenter";

    @Override
    public void configureBindings() {
        bindInterfaceToImplementionInstance(SwipeLeftOrRightImageSwitcherManager.class, new SwipeLeftOrRightImageSwitcherManagerImpl());
        bindInterfaceToImplementionInstance(FullScreenContentManager.class, new FullScreenContentManagerImpl());
        bindInterfaceToImplementionInstance(CustomAppTitleStyleManager.class, new CustomAppTitleStyleManagerImpl());
        bindInterfaceToImplementionInstance(OutfitGridLoaderManager.class, new OutfitGridLoaderManagerImpl());

        bindInterfaceToImplementionInstance(OutfitOccasionSelectorDialog.class, new OutfitOccasionSelectorDialogImpl());
        bindInterfaceToImplementionInstance(OutfitSeasonSelectorDialog.class, new OutfitSeasonSelectorDialogImpl());
        bindInterfaceToImplementionInstance(ChooseOutfitPhotoSourceDialog.class, new ChooseOutfitPhotoSourceDialogImpl());
        bindInterfaceToImplementionInstance(OutfitOccasionsSelectorDialog.class, new OutfitOccasionsSelectorDialogImpl());
        bindInterfaceToImplementionInstance(OutfitSeasonsSelectorDialog.class, new OutfitSeasonsSelectorDialogImpl());
        bindInterfaceToImplementionInstance(DiscardOutfitChangesDialog.class, new DiscardOutfitChangesDialogImpl());
        bindInterfaceToImplementionInstance(DiscardHashTagChangesDialog.class, new DiscardHashTagChangesDialogImpl());
        bindInterfaceToImplementionInstance(NoMoreOutfitsToShowDialog.class, new NoMoreOutfitsToShowDialogImpl());
        bindInterfaceToImplementionInstance(PromoteAppRatingDialog.class, new PromoteAppRatingDialogImpl());
        bindInterfaceToImplementionInstance(PromoteInAppPurchaseDialog.class, new PromoteInAppPurchaseDialogImpl());
        bindInterfaceToImplementionInstance(PurchaseSelectorDialog.class, new PurchaseSelectorDialogImpl());
        bindInterfaceToImplementionInstance(ClearSearchHistoryDialog.class, new ClearSearchHistoryDialogImpl());
        bindInterfaceToImplementionInstance(DeleteOutfitConfirmationDialog.class, new DeleteOutfitConfirmationDialogImpl());

        bindInterfaceToImplementionInstanceWithName(AdPresenter.class, new FrequencyBasedAdPresenterImpl(2, 3), NEXT_RANDOM_OUTFIT_AD_PRESENTER);
        bindInterfaceToImplementionInstanceWithName(AdPresenter.class, new FrequencyBasedAdPresenterImpl(5, 2), PREVIOUS_VIEWED_OUTFIT_AD_PRESENTER);
        bindInterfaceToImplementionInstance(RandomOutfitPresenter.class, new RandomOutfitPresenterImpl());
        bindInterfaceToImplementionInstance(WeatherForecastPresenter.class, new WeatherForecastPresenterImpl());
        bindInterfaceToImplementionInstance(CurrentOccasionAndSeasonPresenter.class, new CurrentOccasionAndSeasonPresenterImpl());

        bindInterfaceToImplementionInstance(MenuInflater.class, new MenuInflaterImpl());
    }
}
