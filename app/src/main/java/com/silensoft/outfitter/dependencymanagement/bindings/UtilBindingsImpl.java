package com.silensoft.outfitter.dependencymanagement.bindings;

import com.silensoft.outfitter.utils.FailureReporter;
import com.silensoft.outfitter.utils.NetworkUtils;
import com.silensoft.outfitter.utils.impl.FailureReporterImpl;
import com.silensoft.outfitter.utils.impl.NoConnectionNetworkUtilsImpl;

public class UtilBindingsImpl extends AbstractBindingsImpl {
    @Override
    public void configureBindings() {
        bindInterfaceToImplementionInstance(FailureReporter.class, new FailureReporterImpl());
        bindInterfaceToImplementionInstance(NetworkUtils.class, new NoConnectionNetworkUtilsImpl());
        // TODO: bindInterfaceToImplementionInstance(NetworkUtils.class, new NetworkUtilsImpl());
    }
}
