package com.silensoft.outfitter.dependencymanagement.implementation;

import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Optional;

public interface Implementation {
    Optional<Object> getImplementationInstance(@NonNull final String name);
    void addImplementationInstance(@NonNull final String name, @NonNull final Object implementationInstance);
}
