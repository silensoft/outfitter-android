package com.silensoft.outfitter.dependencymanagement.implementation;

import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

import java.util.HashMap;
import java.util.Map;

public class NamedImplementationsImpl implements Implementation {
    private final Map<String, Object> nameToImplementationMap = new HashMap<>(10);

    @Override
    public Optional<Object> getImplementationInstance(@NonNull final String name) {
        return OptionalImpl.ofNullable(nameToImplementationMap.get(name));
    }

    @Override
    public void addImplementationInstance(@NonNull final String name, @NonNull final Object implementationInstance) {
        nameToImplementationMap.put(name, implementationInstance);
    }
}
