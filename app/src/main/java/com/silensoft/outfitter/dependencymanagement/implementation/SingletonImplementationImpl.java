package com.silensoft.outfitter.dependencymanagement.implementation;

import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class SingletonImplementationImpl implements Implementation {
    private Optional<Object> singleton = OptionalImpl.empty();

    public SingletonImplementationImpl(@NonNull final Object singleton_) {
        singleton = OptionalImpl.of(singleton_);
    }

    @Override
    public Optional<Object> getImplementationInstance(@NonNull final String name) {
        return singleton;
    }

    @Override
    public void addImplementationInstance(@NonNull final String name, @NonNull final Object implementationInstance) {
        singleton = OptionalImpl.of(implementationInstance);
    }
}
