package com.silensoft.outfitter.model.appusageinfo;

import android.content.Context;
import android.support.annotation.NonNull;

public interface AppUsageInfo {
    void registerAppFirstLaunchTimestampIfNeeded(@NonNull final Context context);
    void incrementAppMainActivityStartedCount(@NonNull final Context context);
    void denyAppRatingPromotion(@NonNull final Context context);
    boolean isAppRatingPromotionAppropriate(@NonNull final Context context);
    void denyInAppPurchasePromotion(@NonNull final Context context);
    boolean isInAppPurchasePromotionAppropriate(@NonNull final Context context);
    void setLastCloudSyncTimestampUtcInMilliSecs(@NonNull final Context context, final long lastCloudSyncTimestampUtc);
    long getLastCloudSyncTimestampUtcInMilliSecs(@NonNull final Context context);
}
