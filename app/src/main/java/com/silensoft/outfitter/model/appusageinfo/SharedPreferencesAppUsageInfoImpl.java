package com.silensoft.outfitter.model.appusageinfo;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class SharedPreferencesAppUsageInfoImpl implements AppUsageInfo {
    private static final String LAST_APP_RATING_PROMOTION_TIMESTAMP_IN_MILLI_SECS = "lastAppRatingPromotionTimestampInMilliSecs";
    private static final String MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_APP_RATING_PROMOTION = "mainActivityStartedCountSinceLastAppRatingPromotion";
    private static final String IS_APP_RATING_PROMOTION_ALLOWED = "isAppRatingPromotionAllowed";
    private static final String APP_RATING_PROMOTION_COUNT = "appRatingPromotionCount";
    private static final long NEEDED_APP_USAGE_TIME_IN_DAYS_FOR_APP_RATING_PROMOTION = 14L;
    private static final long NEEDED_APP_MAIN_ACTIVITY_STARTED_COUNT_FOR_APP_RATING_PROMOTION = 7L;
    private static final long MAX_APP_RATING_PROMOTION_COUNT = 5L;

    private static final String LAST_IN_APP_PURCHASE_PROMOTION_TIMESTAMP_IN_MILLI_SECS = "lastInAppPurchasePromotionTimestampInMilliSecs";
    private static final String MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_IN_APP_PURCHASE_PROMOTION = "mainActivityStartedCountSinceLastInAppPurchasePromotion";
    private static final String IS_IN_APP_PURCHASE_PROMOTION_ALLOWED = "isInAppPurchasePromotionAllowed";
    private static final String IN_APP_PURCHASE_PROMOTION_COUNT = "inAppPurchasePromotionCount";
    private static final long NEEDED_APP_USAGE_TIME_IN_DAYS_FOR_IN_APP_PURCHASE_PROMOTION = 7L;
    private static final long NEEDED_APP_MAIN_ACTIVITY_STARTED_COUNT_FOR_IN_APP_PURCHASE_PROMOTION = 4L;
    private static final long MAX_IN_APP_PURCHASE_PROMOTION_COUNT = 10L;

    private static final String LAST_CLOUD_SYNC_TIMESTAMP_IN_MILLI_SECS = "lastCloudSyncTimestampInMilliSecs";

    private static final long MILLI_SECS_IN_ONE_DAY = 86400000L;
    public static final String SHARED_PREFERENCES_FILE_NAME = "com.silensoft.outfitter.appusageinfo";

    @Override
    public void registerAppFirstLaunchTimestampIfNeeded(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        final long lastAppRatingPromotionTimestampInMilliSecs = sharedPreferences.getLong(LAST_APP_RATING_PROMOTION_TIMESTAMP_IN_MILLI_SECS, 0L);
        if (lastAppRatingPromotionTimestampInMilliSecs == 0L) {
            sharedPreferences.edit().putLong(LAST_APP_RATING_PROMOTION_TIMESTAMP_IN_MILLI_SECS, System.currentTimeMillis()).apply();
            sharedPreferences.edit().putLong(LAST_IN_APP_PURCHASE_PROMOTION_TIMESTAMP_IN_MILLI_SECS, System.currentTimeMillis()).apply();
        }
    }

    @Override
    public void incrementAppMainActivityStartedCount(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

        long mainActivityStartedCountSinceLastAppRatingPromotion = sharedPreferences.getLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_APP_RATING_PROMOTION, 0L);
        mainActivityStartedCountSinceLastAppRatingPromotion++;
        sharedPreferences.edit().putLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_APP_RATING_PROMOTION, mainActivityStartedCountSinceLastAppRatingPromotion).apply();

        long mainActivityStartedCountSinceLastInAppPurchasePromotion = sharedPreferences.getLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_IN_APP_PURCHASE_PROMOTION, 0L);
        mainActivityStartedCountSinceLastInAppPurchasePromotion++;
        sharedPreferences.edit().putLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_APP_RATING_PROMOTION, mainActivityStartedCountSinceLastInAppPurchasePromotion).apply();
    }

    @Override
    public void denyAppRatingPromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(IS_APP_RATING_PROMOTION_ALLOWED, false).apply();
    }

    @Override
    public boolean isAppRatingPromotionAppropriate(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        final boolean isAppRatingPromotionAllowed = sharedPreferences.getBoolean(IS_APP_RATING_PROMOTION_ALLOWED, true);

        if (isAppRatingPromotionAllowed) {
            final long appUsageTimeInDaysSinceLastAppRatingPromotion = getAppUsageTimeInDaysSinceLastAppRatingPromotion(context);
            final long appMainActivityStartedCountSinceLastAppRatingPromotion = getAppMainActivityStartedCountSinceLastAppRatingPromotion(context);
            final long appRatingPromotionCount = sharedPreferences.getLong(APP_RATING_PROMOTION_COUNT, 0L);
            final boolean isAppRatingPromotionAppropriate = appUsageTimeInDaysSinceLastAppRatingPromotion >= NEEDED_APP_USAGE_TIME_IN_DAYS_FOR_APP_RATING_PROMOTION &&
                    appMainActivityStartedCountSinceLastAppRatingPromotion >= NEEDED_APP_MAIN_ACTIVITY_STARTED_COUNT_FOR_APP_RATING_PROMOTION &&
                    appRatingPromotionCount <= MAX_APP_RATING_PROMOTION_COUNT;

            if (isAppRatingPromotionAppropriate) {
                incrementAppRatingPromotionCount(context);
                updateLastAppRatingPromotion(context);
            }

            return isAppRatingPromotionAppropriate;
        }

        return false;
    }

    @Override
    public void denyInAppPurchasePromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(IS_IN_APP_PURCHASE_PROMOTION_ALLOWED, false).apply();
    }

    @Override
    public boolean isInAppPurchasePromotionAppropriate(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        final boolean isInAppPurchasePromotionAllowed = sharedPreferences.getBoolean(IS_IN_APP_PURCHASE_PROMOTION_ALLOWED, true);

        if (isInAppPurchasePromotionAllowed) {
            final long appUsageTimeInDaysSinceLastInAppPurchasePromotion = getAppUsageTimeInDaysSinceLastInAppPurchasePromotion(context);
            final long appMainActivityStartedCountSinceLastInAppPurchasePromotion = getAppMainActivityStartedCountSinceLastInAppPurchasePromotion(context);
            final long inAppPurchasePromotionCount = sharedPreferences.getLong(IN_APP_PURCHASE_PROMOTION_COUNT, 0L);

            final long neededAppUsageTimeInDaysForInAppPurchasePromotion = inAppPurchasePromotionCount > MAX_IN_APP_PURCHASE_PROMOTION_COUNT / 2L ?
                    2L * NEEDED_APP_USAGE_TIME_IN_DAYS_FOR_IN_APP_PURCHASE_PROMOTION :
                    NEEDED_APP_USAGE_TIME_IN_DAYS_FOR_IN_APP_PURCHASE_PROMOTION;

            final long neededAppMainActivityStartedCountForInAppPurchasePromotion = inAppPurchasePromotionCount > MAX_IN_APP_PURCHASE_PROMOTION_COUNT / 2L ?
                    2L * NEEDED_APP_MAIN_ACTIVITY_STARTED_COUNT_FOR_IN_APP_PURCHASE_PROMOTION :
                    NEEDED_APP_MAIN_ACTIVITY_STARTED_COUNT_FOR_IN_APP_PURCHASE_PROMOTION;

            final boolean isInAppPurchasePromotionAppropriate = appUsageTimeInDaysSinceLastInAppPurchasePromotion >= neededAppUsageTimeInDaysForInAppPurchasePromotion &&
                    appMainActivityStartedCountSinceLastInAppPurchasePromotion >= neededAppMainActivityStartedCountForInAppPurchasePromotion &&
                    inAppPurchasePromotionCount <= MAX_IN_APP_PURCHASE_PROMOTION_COUNT ||
                    inAppPurchasePromotionCount == 0L  ;

            if (isInAppPurchasePromotionAppropriate) {
                incrementInAppPurchasePromotionCount(context);
                updateLastInAppPurchasePromotion(context);
            }

            return isInAppPurchasePromotionAllowed;
        }

        return false;
    }

    @Override
    public void setLastCloudSyncTimestampUtcInMilliSecs(@NonNull final Context context, final long lastCloudSyncTimestampUtc) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(LAST_CLOUD_SYNC_TIMESTAMP_IN_MILLI_SECS, lastCloudSyncTimestampUtc).apply();
    }

    @Override
    public long getLastCloudSyncTimestampUtcInMilliSecs(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(LAST_CLOUD_SYNC_TIMESTAMP_IN_MILLI_SECS, 0L);
    }

    private static long getAppUsageTimeInDaysSinceLastAppRatingPromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        final long lastAppRatingPromotionTimestampInMilliSecs = sharedPreferences.getLong(LAST_APP_RATING_PROMOTION_TIMESTAMP_IN_MILLI_SECS, 0L);
        final long currentTimeInMilliSecs = System.currentTimeMillis();
        return lastAppRatingPromotionTimestampInMilliSecs > 0L && lastAppRatingPromotionTimestampInMilliSecs < currentTimeInMilliSecs ?
                (currentTimeInMilliSecs - lastAppRatingPromotionTimestampInMilliSecs) / MILLI_SECS_IN_ONE_DAY :
                0L;
    }

    private static long getAppUsageTimeInDaysSinceLastInAppPurchasePromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        final long lastInAppPurchasePromotionTimestampInMilliSecs = sharedPreferences.getLong(LAST_IN_APP_PURCHASE_PROMOTION_TIMESTAMP_IN_MILLI_SECS, 0L);
        final long currentTimeInMilliSecs = System.currentTimeMillis();
        return lastInAppPurchasePromotionTimestampInMilliSecs > 0L && lastInAppPurchasePromotionTimestampInMilliSecs < currentTimeInMilliSecs ?
                (currentTimeInMilliSecs - lastInAppPurchasePromotionTimestampInMilliSecs) / MILLI_SECS_IN_ONE_DAY :
                0L;
    }

    private static void updateLastAppRatingPromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(LAST_APP_RATING_PROMOTION_TIMESTAMP_IN_MILLI_SECS, System.currentTimeMillis()).apply();
        sharedPreferences.edit().putLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_APP_RATING_PROMOTION, 0L).apply();
    }

    private static void updateLastInAppPurchasePromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(LAST_IN_APP_PURCHASE_PROMOTION_TIMESTAMP_IN_MILLI_SECS, System.currentTimeMillis()).apply();
        sharedPreferences.edit().putLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_IN_APP_PURCHASE_PROMOTION, 0L).apply();
    }

    private static long getAppMainActivityStartedCountSinceLastAppRatingPromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_APP_RATING_PROMOTION, 0L);
    }

    private static long getAppMainActivityStartedCountSinceLastInAppPurchasePromotion(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(MAIN_ACTIVITY_STARTED_COUNT_SINCE_LAST_IN_APP_PURCHASE_PROMOTION, 0L);
    }

    private static void incrementAppRatingPromotionCount(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        long appRatingPromotionCount = sharedPreferences.getLong(APP_RATING_PROMOTION_COUNT, 0L);
        appRatingPromotionCount++;
        sharedPreferences.edit().putLong(APP_RATING_PROMOTION_COUNT, appRatingPromotionCount).apply();
    }

    private static void incrementInAppPurchasePromotionCount(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        long inAppPurchasePromotionCount = sharedPreferences.getLong(IN_APP_PURCHASE_PROMOTION_COUNT, 0L);
        inAppPurchasePromotionCount++;
        sharedPreferences.edit().putLong(APP_RATING_PROMOTION_COUNT, inAppPurchasePromotionCount).apply();
    }
}
