package com.silensoft.outfitter.model.dao;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.types.Consumer;

public interface OutfitCloudDao {
    boolean isSignedIn(@NonNull final Context context);
    void setSignedIn(@NonNull final Context context);
    void fetchOutfitsUpdatedAfter(@NonNull final Context context, long updatedSinceTimestampInMilliSecs, @NonNull final Consumer<Outfit> fetchedOutfitConsumer);

    void saveOutfitPhotoOrElse(@NonNull final Context context, final Bitmap outfitPhotoFullSizeBitmap, @NonNull final Outfit outfit, @NonNull final Action failureAction);

    void deleteOutfitPhotoOrElse(@NonNull final Context context, @NonNull final Outfit outfit, @NonNull final Action failureAction);

}
