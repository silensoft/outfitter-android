package com.silensoft.outfitter.model.dao;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;

public interface OutfitDao {
    @NonNull Optional<Cursor> findAllOutfitsByOccasionAndSeason(@NonNull final Context context, @NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason);
    Loader<Cursor> findOutfitsBasedOnPrototypeOutfitSortedByLastModifiedTimeDescending(@NonNull final Context context, @NonNull final Outfit outfit);
    @NonNull Optional<Outfit> getOutfitFromCursor(@NonNull final Cursor cursor);
    @NonNull Optional<Outfit> getOutfitFromCursorIndex(@NonNull final Cursor cursor, final int index);
    int getNumberOfOutfits(@NonNull final Cursor cursor);
    boolean insertOutfit(@NonNull final Context context, @NonNull final Outfit outfit);
    void deleteOutfit(@NonNull final Context context, @NonNull final Outfit outfit);
    boolean databaseAlreadyHasOutfit(@NonNull final Context context, @NonNull final Outfit outfit);

}
