package com.silensoft.outfitter.model.dao;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;

import java.util.List;
import java.util.UUID;

public interface OutfitMetadata {
    @NonNull List<OutfitOccasion> getOutfitOccasions();
    @NonNull List<OutfitSeason> getOutfitSeasons();
    @NonNull List<String> getOutfitHashtags();
    long getLastModifiedTimestampInMilliSecs();
    @NonNull UUID getOutfitUuid();
    @NonNull Outfit createOutfit(@NonNull final Bitmap outfitPhotoScaledToScreenSizeBitmap);

}
