package com.silensoft.outfitter.model.dao;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.persistableimage.PersistableImage;
import com.silensoft.outfitter.types.Optional;

public interface OutfitPhotoDao {
    boolean savePermanentOutfitPhoto(@NonNull final Outfit outfit);
    boolean saveToBeSyncedToCloudFullSizeOutfitPhoto(@NonNull final PersistableImage persistableImage);
    boolean saveTemporaryOutfitPhoto(@NonNull final Context context, @NonNull final Bitmap outfitPhotoBitmap);
    Optional<Bitmap> getToBeSyncedToCloudFullSizeOutfitPhoto(@NonNull final Outfit outfit);
    @NonNull Optional<Uri> createTemporaryOutfitPhotoFileUri(@NonNull final Activity activity);
    @NonNull Optional<Bitmap> getTemporaryOutfitPhotoScaledToScreenSizeBitmap(@NonNull final Activity activity);
    @NonNull Optional<Uri> getPermanentOutfitPhotoFileUri(@NonNull final Context context, @NonNull final Optional<Outfit> outfit);
    @NonNull Optional<Bitmap> getTemporaryOutfitPhotoBitmap();
}
