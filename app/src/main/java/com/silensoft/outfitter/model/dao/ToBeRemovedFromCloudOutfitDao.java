package com.silensoft.outfitter.model.dao;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;

import java.util.List;

public interface ToBeRemovedFromCloudOutfitDao {
    @NonNull List<Outfit> findAllOutfitsToBeRemovedFromCloud(@NonNull final Context context);
    void insertOutfitToBeRemovedFromCloud(@NonNull final Context context, @NonNull final Outfit outfit);
    void deleteOutfitToBeRemovedFromCloud(@NonNull final Context context, @NonNull final String outfitUuidStr);
}
