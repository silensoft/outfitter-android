package com.silensoft.outfitter.model.dao;


import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;

import java.util.List;

public interface ToBeSyncedToCloudOutfitDao {
    @NonNull List<Outfit> findAllOutfitsToBeSyncedToCloud(@NonNull final Context context);
    void insertOutfitToBeSyncedToCloud(@NonNull final Context context, @NonNull final Outfit outfit);
    void deleteOutfitToBeSyncedToCloud(@NonNull final Context context, @NonNull final String outfitUuidStr);
}

