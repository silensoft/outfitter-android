package com.silensoft.outfitter.model.dao.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.silensoft.outfitter.model.dao.database.contract.OutfitContract;
import com.silensoft.outfitter.model.dao.database.contract.ToBeRemovedFromCloudOutfitContract;
import com.silensoft.outfitter.model.dao.database.contract.ToBeSyncedToCloudOutfitContract;

public class OutfitterDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Outfitter";
    private static final int DATABASE_VERSION = 1;

    public OutfitterDbHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        createOutfitTable(sqLiteDatabase);
        createToBeRemovedFromCloudOutfitTable(sqLiteDatabase);
        createToBeSyncedToCloudOutfitTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int i, final int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + OutfitContract.OutfitEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ToBeRemovedFromCloudOutfitContract.OutfitEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ToBeSyncedToCloudOutfitContract.OutfitEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    private static void createOutfitTable(final SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_OUTFITS_TABLE = "CREATE TABLE IF NOT EXISTS" + OutfitContract.OutfitEntry.TABLE_NAME + " (" +
                OutfitContract.OutfitEntry._ID + " INTEGER NOT NULL, " +
                OutfitContract.OutfitEntry.COLUMN_UUID + " VARCHAR NOT NULL, " +
                OutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP + " TIMESTAMP NOT NULL, " +
                OutfitContract.OutfitEntry.COLUMN_OUTFIT_IMAGE_FILE_PATH + " VARCHAR NOT NULL, " +
                OutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS + " VARCHAR NOT NULL, " +
                OutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS + " VARCHAR NOT NULL," +
                OutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS + " VARCHAR)";

        sqLiteDatabase.execSQL(SQL_CREATE_OUTFITS_TABLE);
    }

    private static void createToBeRemovedFromCloudOutfitTable(final SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_TO_BE_REMOVED_OUTFITS_TABLE = "CREATE TABLE IF NOT EXISTS" + ToBeRemovedFromCloudOutfitContract.OutfitEntry.TABLE_NAME + " (" +
                ToBeRemovedFromCloudOutfitContract.OutfitEntry._ID + " INTEGER NOT NULL, " +
                ToBeRemovedFromCloudOutfitContract.OutfitEntry.COLUMN_UUID + " VARCHAR NOT NULL)";

        sqLiteDatabase.execSQL(SQL_CREATE_TO_BE_REMOVED_OUTFITS_TABLE);
    }

    private static void createToBeSyncedToCloudOutfitTable(final SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_TO_BE_SYNCED_OUTFITS_TABLE = "CREATE TABLE IF NOT EXISTS" + ToBeSyncedToCloudOutfitContract.OutfitEntry.TABLE_NAME + " (" +
                ToBeSyncedToCloudOutfitContract.OutfitEntry._ID + " INTEGER NOT NULL, " +
                ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_UUID + " VARCHAR NOT NULL, " +
                ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP + " TIMESTAMP NOT NULL, " +
                ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_IMAGE_FILE_PATH + " VARCHAR NOT NULL, " +
                ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS + " VARCHAR NOT NULL, " +
                ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS + " VARCHAR NOT NULL," +
                ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS + " VARCHAR)";

        sqLiteDatabase.execSQL(SQL_CREATE_TO_BE_SYNCED_OUTFITS_TABLE);
    }
}
