package com.silensoft.outfitter.model.dao.database.contract;

import android.net.Uri;
import android.provider.BaseColumns;

public final class ToBeRemovedFromCloudOutfitContract {
    public static final String AUTHORITY = "com.silensoft.outfitter.toberemovedfromcloud";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_OUTFITS = "outfits";

    private ToBeRemovedFromCloudOutfitContract() {}

    public static final class OutfitEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_OUTFITS).build();

        public static final String TABLE_NAME = "toBeRemovedFromCloudOutfits";
        public static final String COLUMN_UUID = "uuid";
    }
}
