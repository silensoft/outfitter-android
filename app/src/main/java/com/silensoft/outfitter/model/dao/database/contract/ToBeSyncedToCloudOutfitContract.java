package com.silensoft.outfitter.model.dao.database.contract;

import android.net.Uri;
import android.provider.BaseColumns;

public final class ToBeSyncedToCloudOutfitContract {
    public static final String AUTHORITY = "com.silensoft.outfitter.tobesyncedtocloud";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_OUTFITS = "outfits";

    private ToBeSyncedToCloudOutfitContract() {
    }

    public static final class OutfitEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_OUTFITS).build();

        public static final String TABLE_NAME = "toBeSyncedToCloudOutfits";
        public static final String COLUMN_UUID = "uuid";
        public static final String COLUMN_LAST_MODIFIED_TIMESTAMP = "lastModifiedTimestamp";
        public static final String COLUMN_OUTFIT_IMAGE_FILE_PATH  = "outfitImageFilePath";
        public static final String COLUMN_OUTFIT_SEASONS = "outfitSeasons";
        public static final String COLUMN_OUTFIT_OCCASIONS = "outfitOccasions";
        public static final String COLUMN_OUTFIT_HASHTAGS = "outfitHashtags";
    }
}
