package com.silensoft.outfitter.model.dao.database.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.dao.database.contract.OutfitContract;
import com.silensoft.outfitter.model.dao.database.OutfitterDbHelper;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.FailureReporter;

import static android.content.UriMatcher.NO_MATCH;

public class OutfitContentProviderImpl extends ContentProvider {
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    public static final int MATCHED_URI_OUTFITS = 100;
    public static final int MATCHED_URI_SINGLE_OUTFIT = 101;
    public static final String UNKNOWN_URI = "Unknown uri: ";

    private static final UriMatcher uriMatcher = buildUriMatcher();

    public static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(NO_MATCH);
        uriMatcher.addURI(OutfitContract.AUTHORITY, OutfitContract.PATH_OUTFITS, MATCHED_URI_OUTFITS);
        uriMatcher.addURI(OutfitContract.AUTHORITY, OutfitContract.PATH_OUTFITS + "/*", MATCHED_URI_SINGLE_OUTFIT);
        return uriMatcher;
    }

    @NonNull private Optional<OutfitterDbHelper> outfitterDbHelper = OptionalImpl.empty();
    @NonNull private Optional<SQLiteDatabase> outfitterDatabase = OptionalImpl.empty();

    @SuppressWarnings("FeatureEnvy") // Optional access
    @Override
    public boolean onCreate() {
        final Context context = getContext();
        outfitterDbHelper = OptionalImpl.of(new OutfitterDbHelper(context));
        try {
            outfitterDatabase = OptionalImpl.of(outfitterDbHelper.get().getWritableDatabase());
        } catch (final SQLiteException exception) {
            outfitterDatabase = OptionalImpl.emptyFailedWith(exception);
        }
        return true;
    }

    @Override
    public Uri insert(@NonNull final Uri uri, final ContentValues values) {
        Uri returnUri = Uri.EMPTY;
        final int matchedUri = uriMatcher.match(uri);

        switch (matchedUri) {
            case MATCHED_URI_OUTFITS:
                final long id = outfitterDatabase.mapOrElse(database -> database.insert(OutfitContract.OutfitEntry.TABLE_NAME, null, values), 0L);
                if (id > 0L) {
                    returnUri = ContentUris.withAppendedId(OutfitContract.OutfitEntry.CONTENT_URI, id);
                } else {
                    failureReporter.reportFailure(new SQLiteException("Failed to insert row into " + uri));
                }
                break;
            default:
                throw new UnsupportedOperationException(UNKNOWN_URI + uri);
        }

        if (getContext() != null && getContext().getContentResolver() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return returnUri;
    }

    @Nullable
    public Cursor query(@NonNull final Uri uri, final String[] projection, final String selection, final String[] selectionArgs, final String sortOrder) {
        final int matchedUri = uriMatcher.match(uri);
        final Cursor cursor;

        switch (matchedUri) {
            case MATCHED_URI_OUTFITS:
                cursor = outfitterDatabase.mapOrElse(database ->
                        database.query(OutfitContract.OutfitEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder), null);
                break;
            default:
                throw new UnsupportedOperationException(UNKNOWN_URI + uri);
        }

        if (getContext() != null && getContext().getContentResolver() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public int delete(@NonNull final Uri uri, final String selection, final String[] selectionArgs) {
        final int matchedUri = uriMatcher.match(uri);
        final int outfitsDeletedCount;

        switch (matchedUri) {
            case MATCHED_URI_SINGLE_OUTFIT:
                final String uuidStr = uri.getPathSegments().get(1);
                outfitsDeletedCount = outfitterDatabase.mapOrElse(database ->
                        database.delete(OutfitContract.OutfitEntry.TABLE_NAME, OutfitContract.OutfitEntry.COLUMN_UUID + "=?", new String[] { uuidStr } ), 0);
                break;

            default:
                throw new UnsupportedOperationException(UNKNOWN_URI + uri);
        }

        if (outfitsDeletedCount != 0 && getContext() != null && getContext().getContentResolver() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return outfitsDeletedCount;
    }

    @Override
    public int update(@NonNull final Uri uri, final ContentValues values, final String providedSelection, final String[] providedSelectionArgs) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void shutdown() {
        outfitterDbHelper.ifPresent(SQLiteOpenHelper::close);
        super.shutdown();
    }

    @Override
    public String getType(@NonNull final Uri uri) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
