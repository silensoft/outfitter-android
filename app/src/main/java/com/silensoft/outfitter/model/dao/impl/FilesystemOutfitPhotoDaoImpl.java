package com.silensoft.outfitter.model.dao.impl;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;

import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.dao.OutfitPhotoDao;
import com.silensoft.outfitter.model.persistableimage.PersistableImage;
import com.silensoft.outfitter.model.persistableimage.PersistableImageImpl;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.BitmapUtils;
import com.silensoft.outfitter.utils.FileUtils;

import java.io.File;

public class FilesystemOutfitPhotoDaoImpl implements OutfitPhotoDao {
    private static final String OUTFITTER_DIRECTORY = "/Outfitter";
    private static final String OUTFITTER_TO_BE_SYNCED_DIRECTORY = "/Outfitter/Sync";

    @NonNull private Optional<String> temporaryOutfitPhotoFilePath = OptionalImpl.empty();

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public boolean savePermanentOutfitPhoto(@NonNull final Outfit outfit) {
        final Optional<String> filePath = FileUtils.saveImage(outfit, OUTFITTER_DIRECTORY);
        outfit.setPermanentPhotoFilePath(filePath);
        return filePath.isPresent();
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public boolean saveToBeSyncedToCloudFullSizeOutfitPhoto(@NonNull final PersistableImage persistableImage) {
        final Optional<String> filePath = FileUtils.saveImage(persistableImage, OUTFITTER_TO_BE_SYNCED_DIRECTORY);
        return filePath.isPresent();
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public boolean saveTemporaryOutfitPhoto(@NonNull final Context context, @NonNull final Bitmap outfitPhotoBitmap) {
        return FileUtils.createTempFile(context).mapOrElse(temporaryOutfitPhotoFile -> {
            temporaryOutfitPhotoFilePath = OptionalImpl.ofNullable(temporaryOutfitPhotoFile.getAbsolutePath());
            return FileUtils.saveImageToFile(new PersistableImageImpl(outfitPhotoBitmap), temporaryOutfitPhotoFile);
        }, false);
    }

    @Override
    public Optional<Bitmap> getToBeSyncedToCloudFullSizeOutfitPhoto(@NonNull final Outfit outfit) {
        return FileUtils.loadImage(outfit, OUTFITTER_TO_BE_SYNCED_DIRECTORY);
    }

    @SuppressWarnings({"LawOfDemeter", "FeatureEnvy"})
    @NonNull
    @Override
    public Optional<Uri> createTemporaryOutfitPhotoFileUri(@NonNull final Activity activity) {
        temporaryOutfitPhotoFilePath.ifPresent(FileUtils::deleteFile);

        return FileUtils.createTempFile(activity).flatMap(temporaryOutfitPhotoFile -> {
            temporaryOutfitPhotoFilePath = OptionalImpl.ofNullable(temporaryOutfitPhotoFile.getAbsolutePath());
            return OptionalImpl.ofNullable(FileProvider.getUriForFile(activity, FileUtils.FILE_PROVIDER_AUTHORITY, temporaryOutfitPhotoFile));
        });
    }

    @NonNull
    @Override
    public Optional<Bitmap> getTemporaryOutfitPhotoScaledToScreenSizeBitmap(@NonNull final Activity activity) {
        return temporaryOutfitPhotoFilePath.flatMap(filePath -> BitmapUtils.scalePhotoBitmapToScreenSize(activity, filePath));
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @NonNull
    @Override
    public Optional<Uri> getPermanentOutfitPhotoFileUri(@NonNull final Context context, @NonNull final Optional<Outfit> optOutfit) {
        return optOutfit.flatMap(outfit -> outfit.getPermanentPhotoFilePath().flatMap(outfitPhotoFilePath ->
                OptionalImpl.ofNullable(FileProvider.getUriForFile(context, FileUtils.FILE_PROVIDER_AUTHORITY, new File(outfitPhotoFilePath)))));
    }

    @NonNull
    @Override
    public Optional<Bitmap> getTemporaryOutfitPhotoBitmap() {
        return temporaryOutfitPhotoFilePath.flatMap(filePath -> OptionalImpl.ofNullable(BitmapFactory.decodeFile(filePath)));
    }
}
