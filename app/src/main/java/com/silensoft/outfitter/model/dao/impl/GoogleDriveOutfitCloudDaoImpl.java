package com.silensoft.outfitter.model.dao.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.dao.OutfitCloudDao;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.types.Consumer;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.FailureReporter;
import com.silensoft.outfitter.utils.FileUtils;
import com.silensoft.outfitter.utils.NetworkUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;


public class GoogleDriveOutfitCloudDaoImpl implements OutfitCloudDao {
    private static final String OUTFIT_FILE_MIME_TYPE = "image/jpeg";

    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);
    @SuppressWarnings("LawOfDemeter")
    private final NetworkUtils networkUtils = DependencyManager.getInstance().getImplementationFor(NetworkUtils.class);

    private Optional<GoogleSignInAccount> optSignedInAccount;

    @Override
    public boolean isSignedIn(@NonNull final Context context) {
        final Collection<Scope> requiredScopes = new HashSet<>(2);
        requiredScopes.add(Drive.SCOPE_FILE);
        requiredScopes.add(Drive.SCOPE_APPFOLDER);
        optSignedInAccount = OptionalImpl.ofNullable(GoogleSignIn.getLastSignedInAccount(context));
        return optSignedInAccount.mapOrElse(signedInAccount -> signedInAccount.getGrantedScopes().containsAll(requiredScopes), false);
    }

    @Override
    public void setSignedIn(@NonNull final Context context) {
        optSignedInAccount = OptionalImpl.ofNullable(GoogleSignIn.getLastSignedInAccount(context));
    }

    @SuppressWarnings("ObjectAllocationInLoop")
    @Override
    public void fetchOutfitsUpdatedAfter(@NonNull final Context context,final long updatedSinceTimestampInMilliSecs, @NonNull final Consumer<Outfit> fetchedOutfitConsumer) {
        optSignedInAccount.ifPresent(signedInAccount -> {
            final DriveResourceClient driveResourceClient = Drive.getDriveResourceClient(context, signedInAccount);
            final SortOrder ascendingByLastModifiedDateSortOrder = new SortOrder.Builder().addSortAscending(SortableField.MODIFIED_DATE).build();
            final Query query = new Query.Builder()
                    .addFilter(Filters.greaterThan(SearchableField.MODIFIED_DATE, new Date(updatedSinceTimestampInMilliSecs)))
                    .setSortOrder(ascendingByLastModifiedDateSortOrder)
                    .build();

            driveResourceClient.queryChildren(driveResourceClient.getAppFolder().getResult(), query).continueWithTask(queryOutfitsTask -> {
                for (final Metadata outfitPhotoMetadata : queryOutfitsTask.getResult()) {
                    final DriveFile outfitPhotoDriveFile = outfitPhotoMetadata.getDriveId().asDriveFile();
                    driveResourceClient.openFile(outfitPhotoDriveFile, DriveFile.MODE_READ_ONLY).continueWithTask(readOutfitPhotoFileTask -> {
                        final DriveContents outfitPhotoContents = readOutfitPhotoFileTask.getResult();
                        final Outfit outfit = outfitFactory.createOutfitFromInputStreamAndMetadata(context, outfitPhotoContents.getInputStream(),
                                new GoogleDriveOutfitMetadataImpl(outfitPhotoMetadata));
                        fetchedOutfitConsumer.consume(outfit);
                        return driveResourceClient.discardContents(outfitPhotoContents);
                    }).addOnFailureListener(failureReporter::reportFailure);
                }
                return null;
            });
        });
    }

    @SuppressWarnings("LawOfDemeter")
    @Override
    public void saveOutfitPhotoOrElse(@NonNull final Context context, final Bitmap outfitPhotoFullSizeBitmap, @NonNull final Outfit outfit, @NonNull final Action orElseAction) {

        if (!networkUtils.hasInternetConnection(context)) {
            orElseAction.perform();
            return;
        }

        optSignedInAccount.ifPresent(signedInAccount -> {
            final DriveResourceClient driveResourceClient = Drive.getDriveResourceClient(context, signedInAccount);
            final Task<DriveFolder> appFolderTask = driveResourceClient.getAppFolder();
            final Task<DriveContents> createContentsTask = driveResourceClient.createContents();

            Tasks.whenAll(appFolderTask, createContentsTask).continueWithTask(createOutfitPhotoFileTask -> {
                DriveFolder appFolder = appFolderTask.getResult();
                DriveContents outfitPhotoContents = createContentsTask.getResult();
                OutputStream outfitPhotoOutputStream = outfitPhotoContents.getOutputStream();
                writeOutfitPhotoBitmapToOutputStream(outfitPhotoFullSizeBitmap, outfitPhotoOutputStream);
                MetadataChangeSet outfitPhotoMetadataChangeSet = buildOutfitPhotoMetadataChangeSet(outfit);
                return driveResourceClient.createFile(appFolder, outfitPhotoMetadataChangeSet, outfitPhotoContents);
            }).addOnFailureListener(exception -> {
                failureReporter.reportFailure(exception);
                orElseAction.perform();
            });
        });
    }

    @Override
    public void deleteOutfitPhotoOrElse(@NonNull final Context context, @NonNull final Outfit outfit, @NonNull final Action orElseAction) {

        if (!networkUtils.hasInternetConnection(context)) {
            orElseAction.perform();
            return;
        }

        optSignedInAccount.ifPresent(signedInAccount -> {
            final DriveResourceClient driveResourceClient = Drive.getDriveResourceClient(context, signedInAccount);
            final Query query = new Query.Builder().addFilter(Filters.eq(GoogleDriveOutfitMetadataImpl.UUID_CUSTOM_PROPERTY_KEY, outfit.getUuidStr())).build();

            driveResourceClient.queryChildren(driveResourceClient.getAppFolder().getResult(), query).continueWithTask(queryOutfitTask -> {
                if (queryOutfitTask.getResult().iterator().hasNext()) {
                    final DriveFile outfitPhotoDriveFile = queryOutfitTask.getResult().iterator().next().getDriveId().asDriveFile();
                    return driveResourceClient.delete(outfitPhotoDriveFile);
                } else {
                    return null;
                }
            }).addOnFailureListener(exception -> {
                failureReporter.reportFailure(exception);
                orElseAction.perform();
            });
        });
    }

    private void writeOutfitPhotoBitmapToOutputStream(@NonNull final Bitmap outfitPhotoBitmap, @NonNull final OutputStream outputStream) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        outfitPhotoBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        try {
            outputStream.write(byteArrayOutputStream.toByteArray());
        } catch (final IOException exception) {
            failureReporter.reportFailure(exception);
        }
    }

    private static MetadataChangeSet buildOutfitPhotoMetadataChangeSet(@NonNull final Outfit outfit) {
        final MetadataChangeSet.Builder metadataChangeSetBuilder = new MetadataChangeSet.Builder()
                .setTitle(FileUtils.FILE_NAME_PREFIX + outfit.getUuidStr() + FileUtils.FILE_EXTENSION)
                .setMimeType(OUTFIT_FILE_MIME_TYPE)
                .setStarred(false);
        GoogleDriveOutfitMetadataImpl.setCustomProperties(metadataChangeSetBuilder, outfit.getMetadataMap());
        return metadataChangeSetBuilder.build();
    }
}
