package com.silensoft.outfitter.model.dao.impl;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.silensoft.outfitter.model.dao.OutfitMetadata;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.reactivex.Observable;

public class GoogleDriveOutfitMetadataImpl implements OutfitMetadata {
    public static final CustomPropertyKey OCCASIONS_CUSTOM_PROPERTY_KEY = new CustomPropertyKey(Outfit.METADATA_KEY_OCCASIONS, CustomPropertyKey.PRIVATE);
    public static final CustomPropertyKey SEASONS_CUSTOM_PROPERTY_KEY = new CustomPropertyKey(Outfit.METADATA_KEY_SEASONS, CustomPropertyKey.PRIVATE);
    public static final CustomPropertyKey HASHTAGS_CUSTOM_PROPERTY_KEY = new CustomPropertyKey(Outfit.METADATA_KEY_HASHTAGS, CustomPropertyKey.PRIVATE);
    public static final CustomPropertyKey UUID_CUSTOM_PROPERTY_KEY = new CustomPropertyKey(Outfit.METADATA_KEY_UUID, CustomPropertyKey.PRIVATE);

    private static final Map<String, CustomPropertyKey> METADATA_KEY_TO_CUSTOM_PROPERTY_KEY_MAP = ImmutableMap.of(
            Outfit.METADATA_KEY_OCCASIONS, OCCASIONS_CUSTOM_PROPERTY_KEY,
            Outfit.METADATA_KEY_SEASONS, SEASONS_CUSTOM_PROPERTY_KEY,
            Outfit.METADATA_KEY_HASHTAGS, HASHTAGS_CUSTOM_PROPERTY_KEY,
            Outfit.METADATA_KEY_UUID, UUID_CUSTOM_PROPERTY_KEY
    );

    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);

    private final Map<CustomPropertyKey, String> customProperties;
    private final long lastModifiedTimestampInMilliSecs;

    public GoogleDriveOutfitMetadataImpl(@NonNull final Metadata metadata) {
        customProperties = metadata.getCustomProperties();
        lastModifiedTimestampInMilliSecs = metadata.getModifiedDate().getTime();
    }

    @NonNull
    @Override
    public List<OutfitOccasion> getOutfitOccasions() {
        final String outfitOccasionIndexesStr = customProperties.get(OCCASIONS_CUSTOM_PROPERTY_KEY);
        return ImmutableList.copyOf(OutfitOccasion.fromIndexesString(outfitOccasionIndexesStr));
    }

    @NonNull
    @Override
    public List<OutfitSeason> getOutfitSeasons() {
        final String outfitSeasonsIndexesStr = customProperties.get(SEASONS_CUSTOM_PROPERTY_KEY);
        return OutfitSeason.fromIndexesString(outfitSeasonsIndexesStr);
    }

    @NonNull
    @Override
    public List<String> getOutfitHashtags() {
        final String outfitHashtagsStr = customProperties.get(HASHTAGS_CUSTOM_PROPERTY_KEY);
        final String[] outfitHashtags = outfitHashtagsStr.split(",");
        return Arrays.asList(outfitHashtags);
    }

    @Override
    public long getLastModifiedTimestampInMilliSecs() {
        return lastModifiedTimestampInMilliSecs;
    }

    @NonNull
    @Override
    public UUID getOutfitUuid() {
        return UUID.fromString(customProperties.get(UUID_CUSTOM_PROPERTY_KEY));
    }

    @NonNull
    @Override
    public Outfit createOutfit(@NonNull final Bitmap outfitPhotoScaledToScreenSizeBitmap) {
        return outfitFactory.createOutfit(getOutfitUuid(), outfitPhotoScaledToScreenSizeBitmap, getOutfitOccasions(), getOutfitSeasons(), getOutfitHashtags(), getLastModifiedTimestampInMilliSecs());
    }

    public static void setCustomProperties(@NonNull final MetadataChangeSet.Builder metadataChangeSetBuilder, @NonNull final Map<String, String> outfitMetadata) {
        Observable.fromIterable(outfitMetadata.entrySet()).blockingForEach(outfitProperty -> {
            final CustomPropertyKey customPropertyKey = METADATA_KEY_TO_CUSTOM_PROPERTY_KEY_MAP.get(outfitProperty.getKey());
            if (customPropertyKey != null) {
                metadataChangeSetBuilder.setCustomProperty(customPropertyKey, outfitProperty.getValue());
            }
        });
    }
}
