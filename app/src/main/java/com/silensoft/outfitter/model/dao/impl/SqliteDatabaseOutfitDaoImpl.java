package com.silensoft.outfitter.model.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.dao.OutfitDao;
import com.silensoft.outfitter.model.dao.database.contract.OutfitContract;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.utils.FileUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

public class SqliteDatabaseOutfitDaoImpl implements OutfitDao {
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);

    @NonNull
    @Override
    public Optional<Cursor> findAllOutfitsByOccasionAndSeason(@NonNull final Context context, @NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason) {
        final Cursor cursor = context.getContentResolver().query(OutfitContract.OutfitEntry.CONTENT_URI, null,
                OutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS + " LIKE ?, " + OutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS + " LIKE ?",
                new String[] {'%' + String.valueOf(outfitOccasion.getIndex()) + '%', '%' + String.valueOf(outfitSeason.getIndex()) + '%'},
                null);
        return OptionalImpl.ofNullable(cursor);
    }

    @Override
    public Loader<Cursor> findOutfitsBasedOnPrototypeOutfitSortedByLastModifiedTimeDescending(@NonNull final Context context, @NonNull final Outfit prototypeOutfit) {
        final Uri OUTFITS_URI = OutfitContract.BASE_CONTENT_URI.buildUpon().appendPath(OutfitContract.PATH_OUTFITS).build();
        final String selection = getSelectionFromPrototypeOutfit(prototypeOutfit);
        final String[] selectionArgs = getSelectionArgsFromPrototypeOutfit(prototypeOutfit);
        return new CursorLoader(context, OUTFITS_URI, null, selection, selectionArgs, OutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP + " DESC");
    }

    @NonNull
    @Override
    public Optional<Outfit> getOutfitFromCursorIndex(@NonNull final Cursor cursor, final int index) {
        if (index >= 0 && index < cursor.getColumnCount() && cursor.move(index)) {
            return getOutfitFromCursor(cursor);
        }
        return OptionalImpl.emptyFailedWith(new SQLiteException("Invalid cursor index: " + Integer.toString(index)));
    }

    @Override
    public int getNumberOfOutfits(@NonNull final Cursor cursor) {
        return cursor.getCount();
    }

    @SuppressWarnings({"FeatureEnvy", "LawOfDemeter"}) // Optional and POJO access
    @Override
    public boolean insertOutfit(@NonNull final Context context, @NonNull final Outfit outfit) {
        if (databaseAlreadyHasOutfit(context, outfit)) {
            return true;
        }

        final ContentValues contentValues = new ContentValues();
        contentValues.put(OutfitContract.OutfitEntry.COLUMN_UUID, outfit.getUuidStr());
        contentValues.put(OutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP, outfit.getLastModifiedTimeInMillis());
        outfit.getPermanentPhotoFilePath().ifPresent(outfitPhotoFilePath -> contentValues.put(OutfitContract.OutfitEntry.COLUMN_OUTFIT_IMAGE_FILE_PATH, outfitPhotoFilePath));
        contentValues.put(OutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS, outfit.getOccasionIndexesString());
        contentValues.put(OutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS, outfit.getSeasonIndexesString());
        contentValues.put(OutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS, outfit.getHashTagsString());
        final Uri uri = context.getContentResolver().insert(OutfitContract.OutfitEntry.CONTENT_URI, contentValues);
        return uri != Uri.EMPTY;
    }

    @Override
    public void deleteOutfit(@NonNull final Context context, @NonNull final Outfit outfit) {
        context.getContentResolver().delete(OutfitContract.OutfitEntry.CONTENT_URI.buildUpon().appendPath(outfit.getUuidStr()).build(), null, null);
    }

    @Override
    public boolean databaseAlreadyHasOutfit(@NonNull final Context context, @NonNull final Outfit outfit) {
        final Cursor cursor = context.getContentResolver().query(OutfitContract.OutfitEntry.CONTENT_URI,
                null,
                OutfitContract.OutfitEntry.COLUMN_UUID + "=?",
                new String[] { outfit.getUuidStr() },
                null);
        final boolean hasOutfit = cursor != null && cursor.getCount() > 0;
        if (cursor != null) {
            cursor.close();
        }
        return hasOutfit;
    }

    @NonNull
    @Override
    public Optional<Outfit> getOutfitFromCursor(@NonNull final Cursor cursor) {
        if (cursor == null || cursor.isBeforeFirst() || cursor.isAfterLast()) {
            return OptionalImpl.empty();
        }

        final int uuidColumnIndex = cursor.getColumnIndex(OutfitContract.OutfitEntry.COLUMN_UUID);
        final int lastModifiedTimestampColumnIndex = cursor.getColumnIndex(OutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP);
        final int outfitImageFilePathColumnIndex = cursor.getColumnIndex(OutfitContract.OutfitEntry.COLUMN_OUTFIT_IMAGE_FILE_PATH);
        final int outfitSeasonsColumnIndex = cursor.getColumnIndex(OutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS);
        final int outfitOccasionsColumnIndex = cursor.getColumnIndex(OutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS);
        final int outfitHashTagsColumnIndex = cursor.getColumnIndex(OutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS);

        final String uuidStr = cursor.getString(uuidColumnIndex);
        final Timestamp lastModifiedTimestamp = Timestamp.valueOf(cursor.getString(lastModifiedTimestampColumnIndex));
        final String outfitImageFilePath = cursor.getString(outfitImageFilePathColumnIndex);
        final String outfitSeasonIndexesStr = cursor.getString(outfitSeasonsColumnIndex);
        final String outfitOccasionIndexesStr = cursor.getString(outfitOccasionsColumnIndex);
        final String outfitHashTagsStr = cursor.getString(outfitHashTagsColumnIndex);
        final List<String> hashTags = new ArrayList<>(Arrays.asList(outfitHashTagsStr.split(",")));

        return OptionalImpl.of(outfitFactory.createOutfit(UUID.fromString(uuidStr), lastModifiedTimestamp.getTime(), outfitImageFilePath, FileUtils.loadImage(outfitImageFilePath),
                OutfitOccasion.fromIndexesString(outfitOccasionIndexesStr), OutfitSeason.fromIndexesString(outfitSeasonIndexesStr), hashTags));
    }

    @NonNull
    private static String getSelectionFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        return getOutfitOccasionsSelectionFromPrototypeOutfit(prototypeOutfit) +
                getOutfitSeasonsSelectionFromPrototypeOutfit(prototypeOutfit) +
                getOutfitHashTagsSelectionFromPrototypeOutfit(prototypeOutfit);
    }

    @SuppressWarnings("LawOfDemeter") // StringBuilder
    @NonNull
    private static String getOutfitOccasionsSelectionFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        final StringBuilder outfitOccasionsSelectionStrBuilder = new StringBuilder(1024);
        final List<OutfitOccasion> outfitOccasions = prototypeOutfit.getOccasions();
        final Observable<String> filterExprs = Observable.fromIterable(outfitOccasions).map((outfitOccasion) -> OutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS + " LIKE ?");
        final Observable<String> orExprs = Observable.fromIterable(outfitOccasions).skip(1L).map((outfitOccasion) -> " OR");
        Observable.zip(filterExprs, orExprs, (filterExpr, orExpr) -> outfitOccasionsSelectionStrBuilder.append(filterExpr).append(orExpr)).subscribe();
        return outfitOccasionsSelectionStrBuilder.toString();
    }

    @NonNull
    private static String getOutfitSeasonsSelectionFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        final StringBuilder outfitSeasonsSelectionStrBuilder = new StringBuilder(1024);
        final List<OutfitSeason> outfitSeasons = prototypeOutfit.getSeasons();
        final Observable<String> filterExprs = Observable.fromIterable(outfitSeasons).map((outfitSeason) -> OutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS + " LIKE ?");
        final Observable<String> orExprs = Observable.fromIterable(outfitSeasons).skip(1L).map((outfitSeason) -> " OR");
        Observable.zip(filterExprs, orExprs, (filterExpr, orExpr) -> outfitSeasonsSelectionStrBuilder.append(filterExpr).append(orExpr)).subscribe();
        return outfitSeasonsSelectionStrBuilder.toString();
    }

    @NonNull
    private static String getOutfitHashTagsSelectionFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        final StringBuilder outfitHashTagsSelectionStrBuilder = new StringBuilder(1024);
        final List<String> outfitHashTags = prototypeOutfit.getHashTags();
        final Observable<String> filterExprs = Observable.fromIterable(outfitHashTags).map((outfitHashTag) -> OutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS + " LIKE ?");
        final Observable<String> orExprs = Observable.fromIterable(outfitHashTags).skip(1L).map((outfitHashTag) -> " OR");
        Observable.zip(filterExprs, orExprs, (filterExpr, orExpr) -> outfitHashTagsSelectionStrBuilder.append(filterExpr).append(orExpr)).subscribe();
        return outfitHashTagsSelectionStrBuilder.toString();
    }

    @NonNull
    private static String[] getSelectionArgsFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        final List<String> selectionArgs = new ArrayList<>(25);
        selectionArgs.addAll(getOutfitOccasionsSelectionArgsFromPrototypeOutfit(prototypeOutfit));
        selectionArgs.addAll(getOutfitSeasonsSelectionArgsFromPrototypeOutfit(prototypeOutfit));
        selectionArgs.addAll(getOutfitHashTagsSelectionArgsFromPrototypeOutfit(prototypeOutfit));
        return selectionArgs.toArray(new String[selectionArgs.size()]);
    }

    @NonNull
    private static List<String> getOutfitOccasionsSelectionArgsFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        return Observable.fromIterable(prototypeOutfit.getOccasions()).map(outfitOccasion -> '%' + String.valueOf(outfitOccasion.getIndex()) + '%').toList().blockingGet();
    }

    @NonNull
    private static List<String> getOutfitSeasonsSelectionArgsFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        return Observable.fromIterable(prototypeOutfit.getSeasons()).map(outfitSeason -> '%' + String.valueOf(outfitSeason.getIndex()) + '%').toList().blockingGet();
    }

    @NonNull
    private static List<String> getOutfitHashTagsSelectionArgsFromPrototypeOutfit(@NonNull final Outfit prototypeOutfit) {
        return Observable.fromIterable(prototypeOutfit.getHashTags()).map(outfitHashTag -> '%' + outfitHashTag + '%').toList().blockingGet();
    }
}
