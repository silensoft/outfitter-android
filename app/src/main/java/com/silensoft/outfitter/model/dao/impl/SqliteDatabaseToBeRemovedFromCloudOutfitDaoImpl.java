package com.silensoft.outfitter.model.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.dao.database.contract.ToBeRemovedFromCloudOutfitContract;
import com.silensoft.outfitter.model.dao.ToBeRemovedFromCloudOutfitDao;
import com.silensoft.outfitter.model.factory.OutfitFactory;

import java.util.ArrayList;
import java.util.List;

public class SqliteDatabaseToBeRemovedFromCloudOutfitDaoImpl implements ToBeRemovedFromCloudOutfitDao {
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);

    @SuppressWarnings("LawOfDemeter")
    @NonNull
    @Override
    public List<Outfit> findAllOutfitsToBeRemovedFromCloud(@NonNull final Context context) {
        final Cursor cursor = context.getContentResolver().query(ToBeRemovedFromCloudOutfitContract.OutfitEntry.CONTENT_URI, null, null, null, null);
        final List<Outfit> outfits = new ArrayList<>(10);
        while (cursor != null && cursor.moveToNext()) {
            final Outfit outfit = getOutfitFromCursor(cursor);
            outfits.add(outfit);
        }
        return outfits;
    }

    @Override
    public void insertOutfitToBeRemovedFromCloud(@NonNull final Context context, @NonNull final Outfit outfit) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put(ToBeRemovedFromCloudOutfitContract.OutfitEntry.COLUMN_UUID, outfit.getUuidStr());
        context.getContentResolver().insert(ToBeRemovedFromCloudOutfitContract.OutfitEntry.CONTENT_URI, contentValues);
    }

    @Override
    public void deleteOutfitToBeRemovedFromCloud(@NonNull final Context context, @NonNull final String outfitUuidStr) {
        context.getContentResolver().delete(ToBeRemovedFromCloudOutfitContract.OutfitEntry.CONTENT_URI.buildUpon().appendPath(outfitUuidStr).build(), null, null);
    }

    private Outfit getOutfitFromCursor(@NonNull final Cursor cursor) {
        final int uuidColumnIndex = cursor.getColumnIndex(ToBeRemovedFromCloudOutfitContract.OutfitEntry.COLUMN_UUID);
        final String uuidStr = cursor.getString(uuidColumnIndex);
        return outfitFactory.createOutfit(uuidStr);
    }
}
