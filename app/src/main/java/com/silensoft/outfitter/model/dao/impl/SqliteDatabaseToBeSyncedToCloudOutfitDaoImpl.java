package com.silensoft.outfitter.model.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.model.dao.database.contract.OutfitContract;
import com.silensoft.outfitter.model.dao.database.contract.ToBeSyncedToCloudOutfitContract;
import com.silensoft.outfitter.model.dao.ToBeSyncedToCloudOutfitDao;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.utils.FileUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class SqliteDatabaseToBeSyncedToCloudOutfitDaoImpl implements ToBeSyncedToCloudOutfitDao {
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);

    @NonNull
    @Override
    public List<Outfit> findAllOutfitsToBeSyncedToCloud(@NonNull final Context context) {
        final Cursor cursor = context.getContentResolver().query(ToBeSyncedToCloudOutfitContract.OutfitEntry.CONTENT_URI, null, null, null, null);
        final List<Outfit> outfits = new ArrayList<>(10);
        while (cursor != null && cursor.moveToNext()) {
            final Outfit outfit = getOutfitFromCursor(cursor);
            outfits.add(outfit);
        }
        return outfits;
    }

    @SuppressWarnings({"LawOfDemeter", "FeatureEnvy"}) // Optional and POJO access
    @Override
    public void insertOutfitToBeSyncedToCloud(@NonNull final Context context, @NonNull final Outfit outfit) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_UUID, outfit.getUuidStr());
        contentValues.put(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP, outfit.getLastModifiedTimeInMillis());
        outfit.getPermanentPhotoFilePath().ifPresent(outfitPhotoFilePath -> contentValues.put(OutfitContract.OutfitEntry.COLUMN_OUTFIT_IMAGE_FILE_PATH, outfitPhotoFilePath));
        contentValues.put(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS, outfit.getOccasionIndexesString());
        contentValues.put(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS, outfit.getSeasonIndexesString());
        contentValues.put(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS, outfit.getHashTagsString());
        context.getContentResolver().insert(ToBeSyncedToCloudOutfitContract.OutfitEntry.CONTENT_URI, contentValues);
    }

    @Override
    public void deleteOutfitToBeSyncedToCloud(@NonNull final Context context, @NonNull final String outfitUuidStr) {
        context.getContentResolver().delete(ToBeSyncedToCloudOutfitContract.OutfitEntry.CONTENT_URI.buildUpon().appendPath(outfitUuidStr).build(), null, null);
    }

    private Outfit getOutfitFromCursor(@NonNull final Cursor cursor) {
        final int uuidColumnIndex = cursor.getColumnIndex(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_UUID);
        final int lastModifiedTimestampColumnIndex = cursor.getColumnIndex(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_LAST_MODIFIED_TIMESTAMP);
        final int outfitImageFilePathColumnIndex = cursor.getColumnIndex(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_IMAGE_FILE_PATH);
        final int outfitSeasonsColumnIndex = cursor.getColumnIndex(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_SEASONS);
        final int outfitOccasionsColumnIndex = cursor.getColumnIndex(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_OCCASIONS);
        final int outfitHashTagsColumnIndex = cursor.getColumnIndex(ToBeSyncedToCloudOutfitContract.OutfitEntry.COLUMN_OUTFIT_HASHTAGS);

        final String uuidStr = cursor.getString(uuidColumnIndex);
        final Timestamp lastModifiedTimestamp = Timestamp.valueOf(cursor.getString(lastModifiedTimestampColumnIndex));
        final String outfitImageFilePath = cursor.getString(outfitImageFilePathColumnIndex);
        final String outfitSeasonIndexesStr = cursor.getString(outfitSeasonsColumnIndex);
        final String outfitOccasionIndexesStr = cursor.getString(outfitOccasionsColumnIndex);
        final String outfitHashTagsStr = cursor.getString(outfitHashTagsColumnIndex);
        final List<String> hashTags = new ArrayList<>(Arrays.asList(outfitHashTagsStr.split(",")));

        return outfitFactory.createOutfit(UUID.fromString(uuidStr), lastModifiedTimestamp.getTime(), outfitImageFilePath, FileUtils.loadImage(outfitImageFilePath),
                OutfitOccasion.fromIndexesString(outfitOccasionIndexesStr), OutfitSeason.fromIndexesString(outfitSeasonIndexesStr), hashTags);
    }
}
