package com.silensoft.outfitter.model.factory;

import android.content.Context;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.dao.OutfitMetadata;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.types.Optional;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;

public interface OutfitFactory {
    @NonNull Outfit createOutfit(@NonNull final Outfit sourceOutfit);
    @NonNull Outfit createOutfit(@NonNull final String uuidStr);
    @NonNull Outfit createOutfit(@NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason);

    @NonNull Outfit createOutfit(@NonNull final UUID uuid, @NonNull final Bitmap outfitPhotoScaledToScreenSizeBitmap, @NonNull final List<OutfitOccasion> outfitOccasions,
                                 @NonNull final List<OutfitSeason> outfitSeasons, @NonNull final List<String> outfitHashTags, final long lastModifiedTimestampInMillis);

    @SuppressWarnings("MethodWithTooManyParameters")
    @NonNull Outfit createOutfit(@NonNull final UUID uuid, final long lastModifiedTimestampInMillis,
                                 @NonNull final String outfitPhotoFilePath, @NonNull final Optional<Bitmap> outfitPhotoBitmap,
                                 @NonNull final List<OutfitOccasion> outfitOccasions, @NonNull final List<OutfitSeason> outfitSeasons, @NonNull final List<String> outfitHashTags);

    @NonNull Outfit createOutfitFromIntent(@NonNull final Intent intent);
    @NonNull Outfit createOutfitFromBundle(@NonNull final Bundle bundle);
    @NonNull Outfit createOutfitFromInputStreamAndMetadata(@NonNull final Context context, @NonNull final InputStream outfitPhotoInputStream, @NonNull final OutfitMetadata outfitMetadata);
}
