package com.silensoft.outfitter.model.factory;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.webapis.openweathermap.model.List;

public interface ThreeHourWeatherForecastFactory {
    @SuppressWarnings("MethodParameterOfConcreteClass")
    @NonNull
    ThreeHourWeatherForecast createWeatherForecast(@NonNull final Context context, @NonNull final List threeHourWeatherForecast);
}
