package com.silensoft.outfitter.model.factory.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.factory.ThreeHourWeatherForecastFactory;
import com.silensoft.outfitter.model.weatherforecast.DummyThreeHourWeatherForecastImpl;
import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.model.weatherforecast.weathercondition.CloudyWeatherConditionImpl;
import com.silensoft.outfitter.webapis.openweathermap.model.List;

public class DummyThreeHourWeatherForecastFactoryImpl implements ThreeHourWeatherForecastFactory {
    private static final String HOUR_STR = "13:00";
    private static final String TEMPERATURE_STR = "20 ℃";
    public static final int WEATHER_CONDITION_ID = 802;

    @NonNull
    @Override
    public ThreeHourWeatherForecast createWeatherForecast(@NonNull final Context context, @NonNull final List threeHourWeatherForecast) {
        return new DummyThreeHourWeatherForecastImpl(HOUR_STR, TEMPERATURE_STR,
                new CloudyWeatherConditionImpl().getWeatherConditionImageBitmap(context, WEATHER_CONDITION_ID).get());
    }
}
