package com.silensoft.outfitter.model.factory.impl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.model.dao.OutfitMetadata;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitImpl;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.BitmapUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class OutfitFactoryImpl implements OutfitFactory {
    @NonNull
    @Override
    public Outfit createOutfit(@NonNull final Outfit sourceOutfit) {
        return new OutfitImpl(sourceOutfit);
    }

    @SuppressWarnings("MethodWithTooManyParameters")
    @NonNull
    @Override
    public Outfit createOutfit(@NonNull final UUID uuid, @NonNull final Bitmap outfitPhotoScaledToScreenSizeBitmap, @NonNull final List<OutfitOccasion> outfitOccasions,
                               @NonNull final List<OutfitSeason> outfitSeasons, @NonNull final List<String> outfitHashTags, final long lastModifiedTimestampInMilliSecs) {

        return new OutfitImpl(uuid, outfitPhotoScaledToScreenSizeBitmap, outfitOccasions, outfitSeasons, outfitHashTags, lastModifiedTimestampInMilliSecs);
    }

    @NonNull
    @Override
    public Outfit createOutfit(@NonNull final String uuidStr) {
        return new OutfitImpl(uuidStr);
    }

    @NonNull
    @Override
    public Outfit createOutfit(@NonNull final OutfitOccasion outfitOccasion, @NonNull final OutfitSeason outfitSeason) {
        final List<OutfitOccasion> outfitOccasions = new ArrayList<>(Collections.singletonList(outfitOccasion));
        final List<OutfitSeason> outfitSeasons = new ArrayList<>(Collections.singletonList(outfitSeason));
        return new OutfitImpl(outfitOccasions, outfitSeasons, new ArrayList<>(0));
    }

    @SuppressWarnings("MethodWithTooManyParameters")
    @NonNull
    @Override
    public Outfit createOutfit(@NonNull final UUID uuid, final long lastModifiedTimestampInMillis,
                               @NonNull final String outfitPhotoFilePath, @NonNull final Optional<Bitmap> outfitPhotoBitmap,
                               @NonNull final List<OutfitOccasion> outfitOccasions, @NonNull final List<OutfitSeason> outfitSeasons, @NonNull final List<String> outfitHashTags) {
        return new OutfitImpl(uuid, lastModifiedTimestampInMillis, outfitPhotoFilePath, outfitPhotoBitmap, outfitOccasions, outfitSeasons, outfitHashTags);
    }

    @NonNull
    @Override
    public Outfit createOutfitFromIntent(@NonNull final Intent intent) {
        final Bitmap outfitPhotoScaledToScreenSizeBitmap = intent.getParcelableExtra(Outfit.DATA_KEY_OUTFIT_PHOTO_BITMAP);
        final List<Integer> outfitOccasionIndexes = intent.getIntegerArrayListExtra(Outfit.METADATA_KEY_OCCASIONS);
        final List<Integer> outfitSeasonIndexes = intent.getIntegerArrayListExtra(Outfit.METADATA_KEY_SEASONS);
        final List<String> outfitHashTags = intent.getStringArrayListExtra(Outfit.METADATA_KEY_HASHTAGS);
        return new OutfitImpl(outfitPhotoScaledToScreenSizeBitmap, OutfitOccasion.fromIndexes(outfitOccasionIndexes), OutfitSeason.fromIndexes(outfitSeasonIndexes), outfitHashTags);
    }

    @NonNull
    @Override
    public Outfit createOutfitFromBundle(@NonNull final Bundle bundle) {
        final List<Integer> outfitOccasionIndexes = bundle.getIntegerArrayList(Outfit.METADATA_KEY_OCCASIONS);
        final List<Integer> outfitSeasonIndexes = bundle.getIntegerArrayList(Outfit.METADATA_KEY_SEASONS);
        final List<String> outfitHashTags = bundle.getStringArrayList(Outfit.METADATA_KEY_HASHTAGS);
        return new OutfitImpl(OutfitOccasion.fromIndexes(outfitOccasionIndexes), OutfitSeason.fromIndexes(outfitSeasonIndexes), outfitHashTags);
    }

    @NonNull
    @Override
    public Outfit createOutfitFromInputStreamAndMetadata(@NonNull final Context context, @NonNull final InputStream outfitPhotoInputStream, @NonNull final OutfitMetadata outfitMetadata) {
        final Bitmap outfitPhotoScaledToScreenSizeBitmap = BitmapUtils.scalePhotoBitmapToScreenSize(context, outfitPhotoInputStream);
        return outfitMetadata.createOutfit(outfitPhotoScaledToScreenSizeBitmap);

    }
}