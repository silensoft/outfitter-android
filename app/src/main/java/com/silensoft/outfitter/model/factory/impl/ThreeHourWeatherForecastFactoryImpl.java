package com.silensoft.outfitter.model.factory.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.model.factory.ThreeHourWeatherForecastFactory;
import com.silensoft.outfitter.model.weatherforecast.OpenWeatherMapThreeHourWeatherForecastImpl;
import com.silensoft.outfitter.webapis.openweathermap.model.List;

public class ThreeHourWeatherForecastFactoryImpl implements ThreeHourWeatherForecastFactory {

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @NonNull
    public ThreeHourWeatherForecast createWeatherForecast(@NonNull final Context context, @NonNull final List threeHourWeatherForecast) {
        return new OpenWeatherMapThreeHourWeatherForecastImpl(context, threeHourWeatherForecast);
    }
}
