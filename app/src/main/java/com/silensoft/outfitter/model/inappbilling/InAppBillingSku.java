package com.silensoft.outfitter.model.inappbilling;

import android.content.Context;
import android.support.annotation.NonNull;

public interface InAppBillingSku {
    @NonNull
    InAppBillingSkuType getType();
    @NonNull String getPrice();
    @NonNull String getPromotionTitle(@NonNull final Context context);
    @NonNull String getPromotionMessage(@NonNull final Context context);
    @NonNull String getSkuString();
    @NonNull String getItemType();
    @NonNull String getSalesItemLine();
}
