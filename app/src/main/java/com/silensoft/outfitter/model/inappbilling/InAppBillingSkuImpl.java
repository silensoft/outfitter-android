package com.silensoft.outfitter.model.inappbilling;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSku;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.libraries.inappbilling.SkuDetails;

public class InAppBillingSkuImpl implements InAppBillingSku {
    private final InAppBillingSkuType type;
    private final String price;
    private final String title;

    public InAppBillingSkuImpl() {
        type = InAppBillingSkuType.NONE;
        price = "";
        title = "";
    }

    @SuppressWarnings("MethodParameterOfConcreteClass")
    public InAppBillingSkuImpl(@NonNull final InAppBillingSkuType type_, @NonNull final SkuDetails skuDetails) {
        type = type_;
        price = skuDetails.getPrice();
        title = skuDetails.getTitle();
    }

    @NonNull
    public InAppBillingSkuType getType() {
        return type;
    }

    @NonNull
    public String getPrice() {
        return price;
    }

    @NonNull
    public String getPromotionTitle(@NonNull final Context context) {
        if (type == InAppBillingSkuType.PLUS_PACKAGE) {
            return context.getString(R.string.promote_plus_package_title);
        } else if (type == InAppBillingSkuType.PRO_SUBSCRIPTION) {
            return context.getString(R.string.promote_pro_subscription_title);
        }
        return "";
    }

    @NonNull
    public String getPromotionMessage(@NonNull final Context context) {
        String baseMessage = "";
        if (type == InAppBillingSkuType.PLUS_PACKAGE) {
            baseMessage = context.getString(R.string.promote_plus_package_message);
        } else if (type == InAppBillingSkuType.PRO_SUBSCRIPTION) {
            baseMessage = context.getString(R.string.promote_pro_subscription_message);
        }
        return baseMessage + getPrice();
    }

    @NonNull
    public String getSkuString() {
        return type.toSkuString();
    }

    @NonNull
    public String getItemType() {
        return type.toItemTypeString();
    }

    @NonNull
    @Override
    public String getSalesItemLine() {
        return title + ' ' + price;
    }
}
