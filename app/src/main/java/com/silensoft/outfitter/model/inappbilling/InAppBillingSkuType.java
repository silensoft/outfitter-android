package com.silensoft.outfitter.model.inappbilling;


import android.support.annotation.NonNull;

import com.silensoft.outfitter.libraries.inappbilling.IabHelper;

public enum InAppBillingSkuType {
    NONE("", ""),
    PLUS_PACKAGE("", IabHelper.ITEM_TYPE_INAPP), // TODO: RELEASE: Add SKU
    PRO_SUBSCRIPTION("", IabHelper.ITEM_TYPE_SUBS); // TODO: RELEASE: Add SKU

    private final String skuStr;
    private final String itemTypeStr;

    InAppBillingSkuType(@NonNull final String skuStr_, @NonNull final String itemTypeStr_) {
        skuStr = skuStr_;
        itemTypeStr = itemTypeStr_;
    }

    @NonNull
    public String toSkuString() {
        return skuStr;
    }

    @NonNull
    public String toItemTypeString() {
        return itemTypeStr;
    }
}
