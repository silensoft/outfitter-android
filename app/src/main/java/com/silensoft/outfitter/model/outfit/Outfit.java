package com.silensoft.outfitter.model.outfit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.persistableimage.PersistableImage;
import com.silensoft.outfitter.types.Optional;

import java.util.List;
import java.util.Map;

public interface Outfit extends PersistableImage {
    String DATA_KEY_OUTFIT_PHOTO_BITMAP = "bitmap";
    String METADATA_KEY_OCCASIONS = "occasions";
    String METADATA_KEY_SEASONS = "seasons";
    String METADATA_KEY_HASHTAGS = "hashtags";
    String METADATA_KEY_UUID = "uuid";

    @NonNull Uri getPhotoUri(@NonNull final Context context);
    @NonNull List<OutfitOccasion> getOccasions();
    @NonNull List<Integer> getOccasionIndexes();
    @NonNull String getOccasionIndexesString();
    @NonNull String getOccasionsString(@NonNull final Context context);
    @NonNull List<OutfitSeason> getSeasons();
    @NonNull List<Integer> getSeasonIndexes();
    @NonNull String getSeasonIndexesString();
    @NonNull String getSeasonsString(@NonNull final Context context);
    @NonNull List<String> getHashTags();
    @NonNull String getHashTagsString();
    long getLastModifiedTimeInMillis();
    @NonNull Optional<String> getPermanentPhotoFilePath();
    @NonNull Map<String, String> getMetadataMap();

    void persistToIntent(@NonNull final Intent intent);
    void persistToBundle(@NonNull final Bundle bundle);

    void setOccasions(@NonNull final List<OutfitOccasion> outfitOccasions);
    void setOccasionIndexes(@NonNull final List<Integer> outfitOccasionIndexes);
    void setSeasons(@NonNull final List<OutfitSeason> outfitSeasons);
    void setSeasonIndexes(@NonNull final List<Integer> outfitSeasonIndexes);
    void setHashTags(@NonNull final List<String> hashTags);
    void setPermanentPhotoFilePath(@NonNull final Optional<String> outfitPermanentPhotoFilePath);
}
