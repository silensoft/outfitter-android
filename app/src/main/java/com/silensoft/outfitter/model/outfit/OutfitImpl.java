package com.silensoft.outfitter.model.outfit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.common.collect.ImmutableList;
import com.silensoft.outfitter.model.persistableimage.PersistableImageImpl;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.FileUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class OutfitImpl extends PersistableImageImpl implements Outfit {
    @NonNull private List<OutfitOccasion> outfitOccasions;
    @NonNull private List<OutfitSeason> outfitSeasons;
    @NonNull private final List<String> outfitHashTags;
    @NonNull private Optional<String> outfitPermanentPhotoFilePath;
    private final long outfitLastModifiedTimeInMillis;

    public OutfitImpl(@NonNull final Outfit sourceOutfit) {
        super(sourceOutfit.getImageBitmap(), UUID.fromString(sourceOutfit.getUuidStr()));
        outfitOccasions = new ArrayList<>(sourceOutfit.getOccasions());
        outfitSeasons = new ArrayList<>(sourceOutfit.getSeasons());
        outfitHashTags = new ArrayList<>(sourceOutfit.getHashTags());
        outfitLastModifiedTimeInMillis = sourceOutfit.getLastModifiedTimeInMillis();
        outfitPermanentPhotoFilePath = sourceOutfit.getPermanentPhotoFilePath();
    }

    public OutfitImpl(@NonNull final String uuidStr) {
        super(OptionalImpl.empty(), UUID.fromString(uuidStr));
        outfitOccasions = new ArrayList<>(1);
        outfitSeasons = new ArrayList<>(1);
        outfitHashTags = new ArrayList<>(1);
        outfitLastModifiedTimeInMillis = System.currentTimeMillis();
        outfitPermanentPhotoFilePath = OptionalImpl.empty();
    }

    public OutfitImpl(@NonNull final List<OutfitOccasion> outfitOccasions_, @NonNull final List<OutfitSeason> outfitSeasons_, @NonNull final List<String> outfitHashtags_) {
        outfitOccasions = new ArrayList<>(outfitOccasions_);
        outfitSeasons = new ArrayList<>(outfitSeasons_);
        outfitHashTags = new ArrayList<>(outfitHashtags_);
        outfitLastModifiedTimeInMillis = System.currentTimeMillis();
        outfitPermanentPhotoFilePath = OptionalImpl.empty();
    }

    public OutfitImpl(@NonNull final Bitmap outfitPhotoScaledToScreenSizeBitmap_, @NonNull final List<OutfitOccasion> outfitOccasions_,
                      @NonNull final List<OutfitSeason> outfitSeasons_, @NonNull final List<String> outfitHashtags_) {

        super(outfitPhotoScaledToScreenSizeBitmap_);
        outfitOccasions = new ArrayList<>(outfitOccasions_);
        outfitSeasons = new ArrayList<>(outfitSeasons_);
        outfitHashTags = new ArrayList<>(outfitHashtags_);
        outfitLastModifiedTimeInMillis = System.currentTimeMillis();
        outfitPermanentPhotoFilePath = OptionalImpl.empty();
    }

    @SuppressWarnings("ConstructorWithTooManyParameters")
    public OutfitImpl(@NonNull final UUID uuid_, @NonNull final Bitmap outfitPhotoScaledToScreenSizeBitmap_, @NonNull final List<OutfitOccasion> outfitOccasions_,
                      @NonNull final List<OutfitSeason> outfitSeasons_, @NonNull final List<String> outfitHashtags_, final long outfitLastModifiedTimeInMillis_) {

        super(outfitPhotoScaledToScreenSizeBitmap_, uuid_);
        outfitOccasions = new ArrayList<>(outfitOccasions_);
        outfitSeasons = new ArrayList<>(outfitSeasons_);
        outfitHashTags = new ArrayList<>(outfitHashtags_);
        outfitLastModifiedTimeInMillis = outfitLastModifiedTimeInMillis_;
        outfitPermanentPhotoFilePath = OptionalImpl.empty();
    }

    @SuppressWarnings("ConstructorWithTooManyParameters")
    public OutfitImpl(@NonNull final UUID uuid_, final long outfitLastModifiedTimeInMillis_,
                      @NonNull final String outfitPermanentPhotoFilePath_, @NonNull final Optional<Bitmap> outfitPhotoScaledToScreenSizeBitmap_,
                      @NonNull final List<OutfitOccasion> outfitOccasions_, @NonNull final List<OutfitSeason> outfitSeasons_, @NonNull final List<String> outfitHashTags_) {

        super(outfitPhotoScaledToScreenSizeBitmap_, uuid_);
        outfitOccasions = new ArrayList<>(outfitOccasions_);
        outfitSeasons = new ArrayList<>(outfitSeasons_);
        outfitHashTags = new ArrayList<>(outfitHashTags_);
        outfitLastModifiedTimeInMillis = outfitLastModifiedTimeInMillis_;
        outfitPermanentPhotoFilePath = OptionalImpl.of(outfitPermanentPhotoFilePath_);
    }

    @NonNull
    @Override
    public Uri getPhotoUri(@NonNull final Context context) {
        return outfitPermanentPhotoFilePath.mapOrElse(filePath -> FileUtils.getUri(context, filePath), Uri.EMPTY);
    }

    @NonNull
    @Override
    public List<OutfitOccasion> getOccasions() {
        return ImmutableList.copyOf(outfitOccasions);
    }

    @NonNull
    @Override
    public List<OutfitSeason> getSeasons() {
        return ImmutableList.copyOf(outfitSeasons);
    }

    @NonNull
    @Override
    public String getOccasionIndexesString() {
        return TextUtils.join(", ", OutfitOccasion.toStringIndexes(outfitOccasions));
    }

    @NonNull
    @Override
    public String getSeasonIndexesString() {
        return TextUtils.join(",", OutfitSeason.toStringIndexes(outfitSeasons));
    }

    @NonNull
    @Override
    public String getOccasionsString(@NonNull final Context context) {
        return OutfitOccasion.toString(context, outfitOccasions);
    }

    @NonNull
    @Override
    public String getSeasonsString(@NonNull final Context context) {
        return OutfitSeason.toString(context, outfitSeasons);
    }

    @NonNull
    @Override
    public String getHashTagsString() {
        return TextUtils.join(", ", outfitHashTags);
    }

    @NonNull
    @Override
    public List<Integer> getOccasionIndexes() {
        return OutfitOccasion.toIndexes(outfitOccasions);
    }

    @NonNull
    @Override
    public List<Integer> getSeasonIndexes() {
        return OutfitSeason.toIndexes(outfitSeasons);
    }

    @NonNull
    @Override
    public List<String> getHashTags() {
        return new ArrayList<>(outfitHashTags);
    }

    @Override
    public long getLastModifiedTimeInMillis() {
        return outfitLastModifiedTimeInMillis;
    }

    @NonNull
    @Override
    public Optional<String> getPermanentPhotoFilePath() {
        return outfitPermanentPhotoFilePath;
    }

    @NonNull
    @Override
    public Map<String, String> getMetadataMap() {
        final Map<String, String> outfitMetadataMap = new HashMap<>(3);
        outfitMetadataMap.put(METADATA_KEY_OCCASIONS, getOccasionIndexesString());
        outfitMetadataMap.put(METADATA_KEY_SEASONS, getSeasonIndexesString());
        outfitMetadataMap.put(METADATA_KEY_HASHTAGS, getHashTagsString());
        return outfitMetadataMap;
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public void persistToIntent(@NonNull final Intent intent) {
        getImageBitmap().ifPresent(outfitPhotoBitmap -> intent.putExtra(DATA_KEY_OUTFIT_PHOTO_BITMAP, outfitPhotoBitmap));
        intent.putIntegerArrayListExtra(METADATA_KEY_OCCASIONS, new ArrayList<>(getOccasionIndexes()));
        intent.putIntegerArrayListExtra(METADATA_KEY_SEASONS, new ArrayList<>(getSeasonIndexes()));
        intent.putStringArrayListExtra(METADATA_KEY_HASHTAGS, new ArrayList<>(getHashTags()));
    }

    @Override
    public void persistToBundle(@NonNull final Bundle bundle) {
        bundle.putIntegerArrayList(METADATA_KEY_OCCASIONS, new ArrayList<>(getOccasionIndexes()));
        bundle.putIntegerArrayList(METADATA_KEY_SEASONS, new ArrayList<>(getSeasonIndexes()));
        bundle.putStringArrayList(METADATA_KEY_HASHTAGS, new ArrayList<>(getHashTags()));
    }

    @Override
    public void setOccasions(@NonNull final List<OutfitOccasion> outfitOccasions_) {
        outfitOccasions.clear();
        outfitOccasions.addAll(outfitOccasions_);
    }

    @Override
    public void setOccasionIndexes(@NonNull final List<Integer> outfitOccasionIndexes) {
        outfitOccasions = OutfitOccasion.fromIndexes(outfitOccasionIndexes);
    }

    @Override
    public void setSeasons(@NonNull final List<OutfitSeason> outfitSeasons_) {
        outfitSeasons.clear();
        outfitSeasons.addAll(outfitSeasons_);
    }

    @Override
    public void setSeasonIndexes(@NonNull final List<Integer> outfitSeasonIndexes) {
        outfitSeasons = OutfitSeason.fromIndexes(outfitSeasonIndexes);
    }

    @Override
    public void setHashTags(@NonNull final List<String> outfitHashTags_) {
        outfitHashTags.clear();
        outfitHashTags.addAll(outfitHashTags_);
    }

    @Override
    public void setPermanentPhotoFilePath(@NonNull final Optional<String> outfitPermanentPhotoFilePath_) {
        outfitPermanentPhotoFilePath = outfitPermanentPhotoFilePath_;
    }
}
