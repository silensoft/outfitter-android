package com.silensoft.outfitter.model.outfit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.common.collect.ImmutableList;
import com.silensoft.outfitter.R;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

public enum OutfitOccasion {
    LEISURE(R.string.occasion_leisure, 0),
    BUSINESS(R.string.occasion_business, 1),
    PARTY(R.string.occasion_party, 2),
    FORMAL(R.string.occasion_formal, 3),
    SPORTS_OUTDOOR(R.string.occasion_sports_outdoor, 4),
    SPORTS_INDOOR(R.string.occasion_sports_indoor, 5),
    BEACH(R.string.occasion_beach, 6);

    private static final List<OutfitOccasion> VALUES = ImmutableList.copyOf(Arrays.asList(values()));
    public static List<OutfitOccasion> getValues() {
        return ImmutableList.copyOf(VALUES);
    }

    private final int occasionStrResourceId;
    private final int occasionIndex;

    OutfitOccasion(final int occasionStrResourceId_, final int occasionIndex_) {
        occasionStrResourceId = occasionStrResourceId_;
        occasionIndex = occasionIndex_;
    }

    @NonNull
    public String toString(@NonNull final Context context) {
        return context.getResources().getString(occasionStrResourceId);
    }

    public int getIndex() {
        return occasionIndex;
    }

    public static OutfitOccasion fromIndex(final Integer index) {
        if (index != null && index >= 0 && index < VALUES.size()) {
            return VALUES.get(index);
        }
        return LEISURE;
    }

    @NonNull
    public static String fromIndexToString(@NonNull final Context context, final Integer index) {
        if (index != null && index >= 0 && index < VALUES.size()) {
            return VALUES.get(index).toString(context);
        }
        return LEISURE.toString(context);
    }

    @NonNull
    public static List<OutfitOccasion> fromIndexes(@NonNull final List<Integer> outfitOccasionIndexes) {
        return Observable.fromIterable(outfitOccasionIndexes).map(OutfitOccasion::fromIndex).toList().blockingGet();
    }

    @NonNull
    public static List<String> toStringIndexes(@NonNull final List<OutfitOccasion> outfitOccasions) {
        return Observable.fromIterable(outfitOccasions).map(outfitOccasion -> Integer.toString(outfitOccasion.getIndex())).toList().blockingGet();
    }

    @NonNull
    public static List<Integer> toIndexes(@NonNull final List<OutfitOccasion> outfitOccasions) {
        return Observable.fromIterable(outfitOccasions).map(OutfitOccasion::getIndex).toList().blockingGet();
    }

    @NonNull
    public static List<OutfitOccasion> fromIndexesString(@NonNull final String indexesString) {
        return Observable.fromArray(indexesString.split(",")).map(indexStr -> fromIndex(Integer.valueOf(indexStr))).toList().blockingGet();
    }

    @NonNull
    public static String fromIndexesToString(@NonNull final Context context, @NonNull final List<Integer> outfitOccasionIndexes) {
        final List<String> outfitOccasionStrs = Observable.fromIterable(outfitOccasionIndexes)
                .map(outfitOccasionIndex -> fromIndexToString(context, outfitOccasionIndex)).toList().blockingGet();
        return TextUtils.join(",", outfitOccasionStrs);
    }

    @NonNull
    public static String toString(@NonNull final Context context, @NonNull final List<OutfitOccasion> outfitOccasions) {
        return TextUtils.join(",", Observable.fromIterable(outfitOccasions).map(outfitOccasion -> outfitOccasion.toString(context)).toList().blockingGet());
    }
}
