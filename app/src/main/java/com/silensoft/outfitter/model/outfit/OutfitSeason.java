package com.silensoft.outfitter.model.outfit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.common.collect.ImmutableList;
import com.silensoft.outfitter.R;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

public enum OutfitSeason {
    SUMMER(R.string.season_summer, 0),
    FALL(R.string.season_fall, 1),
    WINTER(R.string.season_winter, 2),
    SPRING(R.string.season_spring, 3);

    private static final List<OutfitSeason> VALUES = ImmutableList.copyOf(Arrays.asList(values()));

    public static List<OutfitSeason> getValues() {
        return ImmutableList.copyOf(VALUES);
    }

    private final int seasonStrResourceId;
    private final int seasonIndex;

    OutfitSeason(final int seasonStrResourceId_, final int seasonIndex_) {
        seasonStrResourceId = seasonStrResourceId_;
        seasonIndex = seasonIndex_;
    }

    public String toString(@NonNull final Context context) {
        return context.getResources().getString(seasonStrResourceId);
    }

    public int getIndex() {
        return seasonIndex;
    }

    public static OutfitSeason fromIndex(final Integer index) {
        if (index != null && index >= 0 && index < VALUES.size()) {
            return VALUES.get(index);
        }
        return SUMMER;
    }

    @NonNull
    public static String fromIndexToString(@NonNull final Context context, final Integer index) {
        if (index != null && index >= 0 && index < VALUES.size()) {
            return VALUES.get(index).toString(context);
        }
        return SUMMER.toString(context);
    }

    @NonNull
    public static List<OutfitSeason> fromIndexes(@NonNull final List<Integer> outfitSeasonIndexes) {
        return Observable.fromIterable(outfitSeasonIndexes).map(OutfitSeason::fromIndex).toList().blockingGet();
    }

    @NonNull
    public static List<String> toStringIndexes(@NonNull final List<OutfitSeason> outfitSeasons) {
        return Observable.fromIterable(outfitSeasons).map(outfitSeason -> Integer.toString(outfitSeason.getIndex())).toList().blockingGet();
    }

    @NonNull
    public static List<Integer> toIndexes(@NonNull final List<OutfitSeason> outfitSeasons) {
        return Observable.fromIterable(outfitSeasons).map(OutfitSeason::getIndex).toList().blockingGet();
    }

    @NonNull
    public static List<OutfitSeason> fromIndexesString(@NonNull final String indexesString) {
        return Observable.fromArray(indexesString.split(",")).map(indexStr ->fromIndex(Integer.valueOf(indexStr))).toList().blockingGet();
    }

    @NonNull
    public static String fromIndexesToString(@NonNull final Context context, @NonNull final List<Integer> outfitSeasonIndexes) {
        final List<String> outfitSeasonStrs = Observable.fromIterable(outfitSeasonIndexes)
                .map(outfitSeasonIndex -> fromIndexToString(context, outfitSeasonIndex)).toList().blockingGet();
        return TextUtils.join(",", outfitSeasonStrs);
    }

    @NonNull
    public static String toString(@NonNull final Context context, @NonNull final List<OutfitSeason> outfitSeasons) {
        return TextUtils.join(",", Observable.fromIterable(outfitSeasons).map(outfitSeason -> outfitSeason.toString(context)).toList().blockingGet());
    }
}

