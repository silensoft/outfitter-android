package com.silensoft.outfitter.model.outfit;

public enum OutfitsState {
    HAS_NO_OUTFITS,
    HAS_OUTFITS
}
