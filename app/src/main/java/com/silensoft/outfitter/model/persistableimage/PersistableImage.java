package com.silensoft.outfitter.model.persistableimage;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Optional;

import java.io.OutputStream;

public interface PersistableImage {
    @NonNull Optional<Bitmap> getImageBitmap();
    @NonNull void persistImage(@NonNull final OutputStream outputStream);
    @NonNull String getUuidStr();
}
