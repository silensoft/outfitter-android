package com.silensoft.outfitter.model.persistableimage;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

import java.io.OutputStream;
import java.util.UUID;

public class PersistableImageImpl implements PersistableImage {
    private final Optional<Bitmap> optImageBitmap;
    @NonNull private final UUID uuid;

    public PersistableImageImpl() {
        optImageBitmap = OptionalImpl.empty();
        uuid = UUID.randomUUID();
    }

    public PersistableImageImpl(@NonNull final Bitmap imageBitmap) {
        optImageBitmap = OptionalImpl.of(imageBitmap);
        uuid = UUID.randomUUID();
    }

    public PersistableImageImpl(@NonNull final Optional<Bitmap> optImageBitmap_, @NonNull final UUID uuid_) {
        optImageBitmap = optImageBitmap_;
        uuid = uuid_;
    }

    public PersistableImageImpl(@NonNull final Bitmap imageBitmap, @NonNull final UUID uuid_) {
        optImageBitmap = OptionalImpl.of(imageBitmap);
        uuid = uuid_;
    }

    @NonNull
    @Override
    public Optional<Bitmap> getImageBitmap() {
        return optImageBitmap;
    }

    @NonNull
    @Override
    public void persistImage(@NonNull final OutputStream outputStream) {
        optImageBitmap.ifPresent(imageBitmap -> imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream));
    }

    @NonNull
    @Override
    public String getUuidStr() {
        return uuid.toString();
    }
}
