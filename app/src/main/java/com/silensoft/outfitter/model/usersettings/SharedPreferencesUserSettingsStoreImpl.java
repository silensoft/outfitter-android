package com.silensoft.outfitter.model.usersettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class SharedPreferencesUserSettingsStoreImpl implements UserSettingsStore {

    @Override
    public TimeFormat getTimeFormat(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String timeFormatStr = sharedPreferences.getString(PREF_TIMEFORMAT_KEY, "");
        return TimeFormat.fromString(timeFormatStr);
    }

    @Override
    public TemperatureUnit getTemperatureUnit(@NonNull final Context context) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String temperatureUnitStr = sharedPreferences.getString(PREF_TEMPERATUREUNIT_KEY, "");
        return TemperatureUnit.fromString(temperatureUnitStr);
    }
}
