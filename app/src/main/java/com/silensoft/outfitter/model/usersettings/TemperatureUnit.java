package com.silensoft.outfitter.model.usersettings;

import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;

public enum TemperatureUnit {
    NONE(""),
    CELSIUS("Celsius"),
    FAHRENHEIT("Fahrenheit");

    private static final List<TemperatureUnit> VALUES = ImmutableList.copyOf(Arrays.asList(values()));

    private final String temperatureUnitStr;

    TemperatureUnit(@NonNull final String temperatureUnitStr_) {
        temperatureUnitStr = temperatureUnitStr_;
    }

    public static TemperatureUnit fromString(@NonNull final String temperatureUnitStr_) {
        for(final TemperatureUnit value : VALUES) {
            if (value.equals(temperatureUnitStr_)) {
                return value;
            }
        }
        return NONE;
    }

    private boolean equals(@NonNull final String temperatureUnitStr_) {
        return temperatureUnitStr.equals(temperatureUnitStr_);
    }
}
