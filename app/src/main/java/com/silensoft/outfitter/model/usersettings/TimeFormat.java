package com.silensoft.outfitter.model.usersettings;

import android.support.annotation.NonNull;

import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;

public enum TimeFormat {
    NONE(""),
    HOURS_12("12"),
    HOURS_24("24");

    private static final List<TimeFormat> VALUES = ImmutableList.copyOf(Arrays.asList(values()));

    private final String timeFormatStr;

    TimeFormat(@NonNull final String timeFormatStr_) {
        timeFormatStr = timeFormatStr_;
    }

    public static TimeFormat fromString(@NonNull final String timeFormatStr_) {
        for(final TimeFormat value : VALUES) {
            if (value.equals(timeFormatStr_)) {
                return value;
            }
        }
        return NONE;
    }

    private boolean equals(@NonNull final String timeFormatStr_) {
        return timeFormatStr.equals(timeFormatStr_);
    }

}
