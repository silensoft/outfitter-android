
package com.silensoft.outfitter.model.usersettings;

import android.content.Context;
import android.support.annotation.NonNull;

public interface UserSettingsStore {
    String PREF_TIMEFORMAT_KEY = "pref_timeFormat";
    String PREF_TEMPERATUREUNIT_KEY = "pref_temperatureUnit";

    TimeFormat getTimeFormat(@NonNull final Context context);
    TemperatureUnit getTemperatureUnit(@NonNull final Context context);
}
