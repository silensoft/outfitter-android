package com.silensoft.outfitter.model.weatherforecast;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

public class DummyThreeHourWeatherForecastImpl implements ThreeHourWeatherForecast {
    private final String hourStr;
    private final String temperatureStr;
    private final Bitmap weatherConditionImageBitmap;

    public DummyThreeHourWeatherForecastImpl(@NonNull final String hourStr_, @NonNull final String temperatureStr_, @NonNull final Bitmap weatherConditionImageBitmap_) {
        hourStr = hourStr_;
        temperatureStr = temperatureStr_;
        weatherConditionImageBitmap = weatherConditionImageBitmap_;
    }

    @Override
    public String getHourStr() {
        return hourStr;
    }

    @Override
    public String getTemperatureStr() {
        return temperatureStr;
    }

    @Override
    public Bitmap getWeatherConditionImageBitmap() {
        return weatherConditionImageBitmap;
    }
}
