package com.silensoft.outfitter.model.weatherforecast;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.weatherforecast.weathercondition.WeatherConditions;
import com.silensoft.outfitter.model.usersettings.UserSettingsStore;
import com.silensoft.outfitter.model.usersettings.TemperatureUnit;
import com.silensoft.outfitter.model.usersettings.TimeFormat;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.webapis.openweathermap.model.List;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import io.reactivex.Observable;

public class OpenWeatherMapThreeHourWeatherForecastImpl implements ThreeHourWeatherForecast {
    private static final Character DEGREE_FAHRENHEIT = '℉';
    private static final Character DEGREE_CELSIUS = '℃';

    @SuppressWarnings("LawOfDemeter")
    private final UserSettingsStore userSettingsStore = DependencyManager.getInstance().getImplementationFor(UserSettingsStore.class);

    private final Context context;
    private final List threeHourWeatherForecast;

    @SuppressWarnings("MethodParameterOfConcreteClass")
    public OpenWeatherMapThreeHourWeatherForecastImpl(@NonNull final Context context_, @NonNull final List threeHourWeatherForecast_) {
        context = context_;
        threeHourWeatherForecast = threeHourWeatherForecast_;
    }

    @Override
    public String getHourStr() {
        return parseHourStrFromUtcTimeInMillis(threeHourWeatherForecast.getDt());
    }

    @SuppressWarnings("LawOfDemeter") // POJO access
    @Override
    public String getTemperatureStr() {
        return convertToFahrenheitOrCelsiusStr(threeHourWeatherForecast.getMain().getTemp());
    }

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    public Bitmap getWeatherConditionImageBitmap() {
        final int weatherConditionId = threeHourWeatherForecast.getWeather().get(0).getId();
        return Observable.fromIterable(WeatherConditions.WEATHER_CONDITIONS)
                .map(weatherCondition -> weatherCondition.getWeatherConditionImageBitmap(context, weatherConditionId))
                .filter(Optional::isPresent)
                .blockingFirst()
                .get();
    }

    private String parseHourStrFromUtcTimeInMillis(final long utcTimeInMillis) {
        final TimeFormat timeFormat = userSettingsStore.getTimeFormat(context);
        final SimpleDateFormat dateFormat = new SimpleDateFormat(timeFormat == TimeFormat.HOURS_12 ? "hh:mm a" : "HH:mm", context.getResources().getConfiguration().locale);
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format(new Date(utcTimeInMillis));
    }

    @SuppressWarnings("MagicNumber")
    private String convertToFahrenheitOrCelsiusStr(final double temperatureInKelvin) {
        final TemperatureUnit temperatureUnit = userSettingsStore.getTemperatureUnit(context);
        return temperatureUnit == TemperatureUnit.FAHRENHEIT ?
                String.valueOf(temperatureInKelvin * 9.0 / 5.0 - 459.67) + ' ' + DEGREE_FAHRENHEIT :
                String.valueOf(temperatureInKelvin - 273.15) + ' ' + DEGREE_CELSIUS;
    }
}
