package com.silensoft.outfitter.model.weatherforecast;

import android.graphics.Bitmap;

public interface ThreeHourWeatherForecast {
    String getHourStr();
    String getTemperatureStr();
    Bitmap getWeatherConditionImageBitmap();
}
