package com.silensoft.outfitter.model.weatherforecast.weathercondition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class ClearSkyWeatherConditionImpl implements WeatherCondition {
    private static final int CLEAR_SKY = 800;

    @NonNull
    @Override
    public Optional<Bitmap> getWeatherConditionImageBitmap(@NonNull final Context context, final int weatherConditionId) {
        if (weatherConditionId == CLEAR_SKY) {
            return OptionalImpl.of(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_sunny_white_24dp));
        }
        return OptionalImpl.empty();
    }
}
