package com.silensoft.outfitter.model.weatherforecast.weathercondition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class DrizzleWeatherConditionImpl implements WeatherCondition {
    private static final int DRIZZLE_LOWER_LIMIT = 300;
    private static final int DRIZZLE_UPPER_LIMIT = 399;

    @NonNull
    @Override
    public Optional<Bitmap> getWeatherConditionImageBitmap(@NonNull final Context context, final int weatherConditionId) {
        if (weatherConditionId >= DRIZZLE_LOWER_LIMIT && weatherConditionId < DRIZZLE_UPPER_LIMIT) {
            return OptionalImpl.of(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_cloudy_white_24dp));
        }
        return OptionalImpl.empty();
    }
}
