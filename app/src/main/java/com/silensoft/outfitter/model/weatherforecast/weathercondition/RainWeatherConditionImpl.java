package com.silensoft.outfitter.model.weatherforecast.weathercondition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class RainWeatherConditionImpl implements WeatherCondition {
    private static final int RAIN_LOWER_LIMIT = 500;
    private static final int RAIN_UPPER_LIMIT = 599;

    @NonNull
    @Override
    public Optional<Bitmap> getWeatherConditionImageBitmap(@NonNull final Context context, final int weatherConditionId) {
        if (weatherConditionId >= RAIN_LOWER_LIMIT && weatherConditionId <= RAIN_UPPER_LIMIT) {
            return OptionalImpl.of(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_rainy_white_24dp));
        }
        return OptionalImpl.empty();
    }
}
