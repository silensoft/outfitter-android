package com.silensoft.outfitter.model.weatherforecast.weathercondition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class ThunderstormWeatherConditionImpl implements WeatherCondition {
    private static final int THUNDERSTORM_LOWER_LIMIT = 200;
    private static final int THUNDERSTORM_UPPER_LIMIT = 299;

    @NonNull
    @Override
    public Optional<Bitmap> getWeatherConditionImageBitmap(@NonNull final Context context, final int weatherConditionId) {
        if (weatherConditionId >= THUNDERSTORM_LOWER_LIMIT && weatherConditionId < THUNDERSTORM_UPPER_LIMIT) {
            return OptionalImpl.of(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_lightning_rainy_white_24dp));
        }
        return OptionalImpl.empty();
    }
}
