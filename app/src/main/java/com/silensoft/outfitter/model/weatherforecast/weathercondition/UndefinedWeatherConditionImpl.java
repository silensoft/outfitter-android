package com.silensoft.outfitter.model.weatherforecast.weathercondition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;

public class UndefinedWeatherConditionImpl implements WeatherCondition {
    @NonNull
    @Override
    public Optional<Bitmap> getWeatherConditionImageBitmap(@NonNull final Context context, final int weatherConditionId) {
        return OptionalImpl.of(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_weather_cloudy_white_24dp));
    }
}
