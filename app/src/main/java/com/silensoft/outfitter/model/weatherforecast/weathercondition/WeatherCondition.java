package com.silensoft.outfitter.model.weatherforecast.weathercondition;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Optional;

public interface WeatherCondition {
    @NonNull Optional<Bitmap> getWeatherConditionImageBitmap(@NonNull final Context context, final int weatherConditionId);
}
