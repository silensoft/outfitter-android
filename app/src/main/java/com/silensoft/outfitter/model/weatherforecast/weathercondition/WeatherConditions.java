package com.silensoft.outfitter.model.weatherforecast.weathercondition;


import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class WeatherConditions {
    @SuppressWarnings("PublicStaticCollectionField") // Is immutable
    public static final List<WeatherCondition> WEATHER_CONDITIONS = ImmutableList.copyOf(new ArrayList<>(Arrays.asList(
            new ClearSkyWeatherConditionImpl(),
            new CloudyWeatherConditionImpl(),
            new DrizzleWeatherConditionImpl(),
            new PartlyCloudyWeatherConditionImpl(),
            new RainWeatherConditionImpl(),
            new SnowWeatherConditionImpl(),
            new ThunderstormWeatherConditionImpl(),
            new UndefinedWeatherConditionImpl())));

    private WeatherConditions() {}
}
