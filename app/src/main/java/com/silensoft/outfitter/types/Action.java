package com.silensoft.outfitter.types;

public interface Action {
    void perform();
}