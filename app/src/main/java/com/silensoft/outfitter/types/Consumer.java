package com.silensoft.outfitter.types;

import android.support.annotation.NonNull;

public interface Consumer<T> {
    void consume(@NonNull final T value);
}


