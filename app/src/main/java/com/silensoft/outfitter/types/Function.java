package com.silensoft.outfitter.types;


import android.support.annotation.NonNull;

public interface Function<P, R> {
    @NonNull R apply(@NonNull final P parameter);
}
