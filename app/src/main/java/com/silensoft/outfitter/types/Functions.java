package com.silensoft.outfitter.types;


public final class Functions {
    private Functions() {}
    public static boolean isGreaterThanZero(final int value) {
        return value > 0;
    }
    public static int decrement(final int value) {
        return value - 1;
    }
}
