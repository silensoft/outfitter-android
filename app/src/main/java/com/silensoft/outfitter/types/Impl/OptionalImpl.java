package com.silensoft.outfitter.types.Impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.types.Consumer;
import com.silensoft.outfitter.types.Function;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.types.Predicate;
import com.silensoft.outfitter.types.Supplier;
import com.silensoft.outfitter.utils.impl.FailureReporterImpl;

import java.util.NoSuchElementException;

public final class OptionalImpl<T> implements Optional<T> {
    @Nullable private final T value;

    public static <T> Optional<T> empty() {
        return new OptionalImpl<>();
    }
    public static <T> Optional<T> of(@NonNull final T value) {
        return new OptionalImpl<>(value);
    }
    public static <T> Optional<T> ofNullable(final T value) {
        return new OptionalImpl<>(value);
    }
    public static <T> Optional<T> emptyFailedWith(@NonNull final Exception exception) {
        new FailureReporterImpl().reportFailure(exception);
        return empty();
    }

    private OptionalImpl() {
        value = null;
    }
    private OptionalImpl(@NonNull final T value_) {
        this.value = value_;
    }

    @NonNull
    public T orElse(@NonNull final T elseValue) {
        return value != null ? value : elseValue;
    }

    @NonNull
    public Optional<T> orElseOptional(@NonNull final Supplier<T> elseOptionalSupplier) {
        return value != null ? new OptionalImpl<>(value) : elseOptionalSupplier.supply();
    }

    public void ifPresent(@NonNull final Consumer<T> consumer) {
        if (value != null) {
            consumer.consume(value);
        }
    }

    @Override
    public void ifPresentOrElse(@NonNull final Consumer<T> isPresentConsumer, @NonNull final Action elseAction) {
        if (value != null) {
            isPresentConsumer.consume(value);
        } else {
            elseAction.perform();
        }
    }

    @NonNull
    public <V> Optional<V> map(@NonNull final Function<T, V> mapper) {
        return value != null ? new OptionalImpl<>(mapper.apply(value)) : empty();
    }

    @NonNull
    public <V> V mapOrElse(@NonNull final Function<T, V> mapper, @NonNull final V elseValue) {
        return value != null ? mapper.apply(value) : elseValue;
    }

    @NonNull
    public Optional<T> filter(@NonNull final Predicate<T> predicate) {
        return predicate.isTrue(value) ? new OptionalImpl<>(value) : empty();
    }

    @NonNull
    public <V> Optional<V> flatMap(@NonNull final Function<T, Optional<V>> mapper) {
        return value != null ? mapper.apply(value) : OptionalImpl.empty();
    }

    public boolean isPresent() {
        return value != null;
    }

    @NonNull
    public T get() {
        if (value != null) {
            return value;
        } else {
            throw new NoSuchElementException();
        }
    }

    /* public void ifPresent(@NonNull final Action action) {
        if (value != null) {
            action.perform();
        }
    } */
}

