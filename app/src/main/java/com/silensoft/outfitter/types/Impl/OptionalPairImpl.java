package com.silensoft.outfitter.types.Impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.types.OptionalPair;
import com.silensoft.outfitter.types.PairConsumer;

import org.javatuples.Pair;

public final class OptionalPairImpl<X extends Optional, Y extends Optional> implements OptionalPair<X, Y> {
    @Nullable private final Pair<X, Y> optionalPair;

    public static <X extends Optional, Y extends Optional> OptionalPair<X, Y> empty() {
        return new OptionalPairImpl<>();
    }
    public static <X extends Optional, Y extends Optional> OptionalPair<X, Y> of(@NonNull final X optional1, @NonNull final Y optional2) {
        return new OptionalPairImpl<>(optional1, optional2);
    }

    private OptionalPairImpl() {
        optionalPair = null;
    }

    private OptionalPairImpl(@NonNull final X optional1, @NonNull final Y optional2) {
        optionalPair = Pair.with(optional1, optional2);
    }

    @Override
    public void ifBothPresentOrElse(@NonNull final PairConsumer<X, Y> consumer, @NonNull final PairConsumer<X, Y> elseConsumer) {
        if (optionalPair != null && optionalPair.getValue0().isPresent() && optionalPair.getValue1().isPresent()) {
            consumer.consume(optionalPair);
        } else {
            elseConsumer.consume(optionalPair);
        }
    }
}
