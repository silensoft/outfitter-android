package com.silensoft.outfitter.types.Impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.types.OptionalTriplet;
import com.silensoft.outfitter.types.TripletConsumer;

import org.javatuples.Triplet;

public final class OptionalTripletImpl<X extends Optional, Y extends Optional, Z extends Optional> implements OptionalTriplet<X, Y, Z> {
    @Nullable private final Triplet<X, Y, Z> optionalTriplet;

    public static <X extends Optional, Y extends Optional, Z extends Optional> OptionalTriplet<X, Y, Z> of(@NonNull final X optional1, @NonNull final Y optional2, @NonNull final Z optional3) {
        return new OptionalTripletImpl<>(optional1, optional2, optional3);
    }

    private OptionalTripletImpl(@NonNull final X optional1, @NonNull final Y optional2, @NonNull final Z optional3) {
        optionalTriplet = Triplet.with(optional1, optional2, optional3);
    }

    @SuppressWarnings("FeatureEnvy")
    @Override
    public void ifAllPresent(@NonNull final TripletConsumer<X, Y, Z> consumer) {
        if (optionalTriplet != null && optionalTriplet.getValue0().isPresent() && optionalTriplet.getValue1().isPresent() && optionalTriplet.getValue2().isPresent()) {
            consumer.consume(optionalTriplet);
        }
    }
}
