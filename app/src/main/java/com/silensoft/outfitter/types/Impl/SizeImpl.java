package com.silensoft.outfitter.types.Impl;

import com.silensoft.outfitter.types.Size;

public class SizeImpl implements Size {
    private final int height;
    private final int width;

    public SizeImpl(final int width_, final int height_) {
        width = width_;
        height = height_;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
