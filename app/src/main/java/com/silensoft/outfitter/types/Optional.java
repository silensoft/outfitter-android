package com.silensoft.outfitter.types;

import android.support.annotation.NonNull;

public interface Optional<T> {
    @NonNull T orElse(@NonNull final T elseValue);
    @NonNull Optional<T> orElseOptional(@NonNull final Supplier<T> elseOptionalSupplier);
    void ifPresent(@NonNull final Consumer<T> consumer);
    void ifPresentOrElse(@NonNull final Consumer<T> isPresentConsumer, @NonNull final Action elseConsumer);
    @NonNull <V> Optional<V> map(@NonNull final Function<T, V> mapper);
    @NonNull <V> V mapOrElse(@NonNull final Function<T, V> mapper, @NonNull final V elseValue);
    @NonNull Optional<T> filter(@NonNull final Predicate<T> predicate);
    @NonNull <V> Optional<V> flatMap(@NonNull final Function<T, Optional<V>> mapper);
    boolean isPresent();
    @NonNull T get();

    // void ifBothPresent(@NonNull final Action action);
}
