package com.silensoft.outfitter.types;


import android.support.annotation.NonNull;

import org.javatuples.Pair;

public interface OptionalPair<X extends Optional, Y extends Optional> {
    void ifBothPresentOrElse(@NonNull final PairConsumer<X, Y> consumer, @NonNull final PairConsumer<X, Y> elseConsumer);
}
