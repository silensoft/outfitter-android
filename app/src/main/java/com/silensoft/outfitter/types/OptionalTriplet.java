package com.silensoft.outfitter.types;

import android.support.annotation.NonNull;

public interface OptionalTriplet<X, Y, Z> {
    void ifAllPresent(@NonNull final TripletConsumer<X, Y, Z> consumer);
}
