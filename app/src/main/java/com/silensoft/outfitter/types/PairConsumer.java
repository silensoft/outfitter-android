package com.silensoft.outfitter.types;

import android.support.annotation.NonNull;

import org.javatuples.Pair;


public interface PairConsumer<X, Y> {
    void consume(@NonNull Pair<X, Y> valuePair);
}
