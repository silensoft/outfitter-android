package com.silensoft.outfitter.types;


public interface Predicate<T> {
    boolean isTrue(final T value);
}
