package com.silensoft.outfitter.types;

public interface ProgressConsumer {
    void consumeProgress(final int currentProgress, final int maxProgress);
}
