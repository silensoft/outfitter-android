package com.silensoft.outfitter.types;

public interface Size {
    int getHeight();
    int getWidth();
}
