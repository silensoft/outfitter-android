package com.silensoft.outfitter.types;

import android.support.annotation.NonNull;

public interface Supplier<T> {
     @NonNull Optional<T> supply();
}
