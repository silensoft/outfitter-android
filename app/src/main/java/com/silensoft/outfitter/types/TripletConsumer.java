package com.silensoft.outfitter.types;

import android.support.annotation.NonNull;

import org.javatuples.Triplet;

public interface TripletConsumer<X, Y, Z> {
    void consume(@NonNull Triplet<X, Y, Z> valueTriplet);
}
