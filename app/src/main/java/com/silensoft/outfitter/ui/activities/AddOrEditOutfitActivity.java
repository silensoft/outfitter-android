package com.silensoft.outfitter.ui.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.ui.adapters.OutfitOptionsArrayAdapterImpl;
import com.silensoft.outfitter.ui.dialogs.DiscardOutfitChangesDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionsSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonsSelectorDialog;
import com.silensoft.outfitter.utils.FailureReporter;

import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class AddOrEditOutfitActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String WHITESPACE = "[\\s\\xA0]+";
    private static final Pattern COMPILED_WHITESPACE_PATTERN = Pattern.compile(WHITESPACE);

    @SuppressWarnings("LawOfDemeter")
    private final OutfitOccasionsSelectorDialog outfitOccasionsSelectorDialog = DependencyManager.getInstance().getImplementationFor(OutfitOccasionsSelectorDialog.class);

    @SuppressWarnings("LawOfDemeter")
    private final OutfitSeasonsSelectorDialog outfitSeasonsSelectorDialog = DependencyManager.getInstance().getImplementationFor(OutfitSeasonsSelectorDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final DiscardOutfitChangesDialog discardOutfitChangesDialog = DependencyManager.getInstance().getImplementationFor(DiscardOutfitChangesDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    @BindView(R.id.optionsListView) ListView outfitOptionsListView;
    @BindView(R.id.imageView) ImageView outfitPhotoView;
    @BindView(R.id.toolbar) Toolbar toolbar;

    private FirebaseAnalytics firebaseAnalytics;
    OutfitOptionsArrayAdapterImpl outfitOptionsAdapter;
    private Outfit outfit;
    private boolean editOutfit;

    @SuppressWarnings("LawOfDemeter") // Optional access
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_outfit);
        ButterKnife.bind(this);
        setupActionBar();
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        outfit = outfitFactory.createOutfitFromIntent(getIntent());
        editOutfit = getIntent().getBooleanExtra(OutfitActions.EDIT_OUTFIT_KEY, false);
        outfitOptionsAdapter = new OutfitOptionsArrayAdapterImpl(this, outfit);
        outfitOptionsListView.setAdapter(outfitOptionsAdapter);
        outfitOptionsListView.setOnItemClickListener(this);
        outfit.getImageBitmap().ifPresent(outfitPhotoBitmap -> outfitPhotoView.setImageBitmap(outfitPhotoBitmap));
    }

    @Override
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int position, final long l) {
        if (position == 0) {
            outfitOccasionsSelectorDialog.show(this, outfit);
        } else if (position == 1) {
            outfitSeasonsSelectorDialog.show(this, outfit);
        } else if (position == 2) {
            final Intent editHashTagsActivityIntent = new Intent(this, EditHashTagsActivity.class);
            outfit.persistToIntent(editHashTagsActivityIntent);
            try {
                startActivity(editHashTagsActivityIntent);
            } catch (final ActivityNotFoundException exception) {
                failureReporter.reportFailure(exception);
            }
        }
        outfitOptionsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if (editOutfit) {
            getMenuInflater().inflate(R.menu.edit_outfit_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.add_outfit_menu, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.add:
                analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.ADD_OUTFIT);
                // noinspection fallthrough
            case R.id.ready:
                analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.EDIT_OUTFIT);
                finishActivity();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        discardOutfitChangesDialog.showAndOnPositiveSelected(this, super::onBackPressed);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        if (resultCode == RESULT_OK && intent != null) {
            final String hashTagsStr = intent.getStringExtra(Outfit.METADATA_KEY_HASHTAGS);
            if (hashTagsStr != null) {
                final List<String> hashTags = Observable.fromArray(COMPILED_WHITESPACE_PATTERN.split(hashTagsStr)).map(String::trim).toList().blockingGet();
                outfit.setHashTags(hashTags);
            }
        }
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        final ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }
    }

    private void finishActivity() {
        final Intent addOrEditOutfitResultIntent = new Intent();
        outfit.persistToIntent(addOrEditOutfitResultIntent);
        addOrEditOutfitResultIntent.putExtra(OutfitActions.EDIT_OUTFIT_KEY, editOutfit);
        setResult(Activity.RESULT_OK, addOrEditOutfitResultIntent);
        finish();
    }
}
