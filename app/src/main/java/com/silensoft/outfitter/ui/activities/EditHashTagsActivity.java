package com.silensoft.outfitter.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.ui.dialogs.DiscardHashTagChangesDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditHashTagsActivity extends AppCompatActivity {
    @SuppressWarnings("LawOfDemeter")
    private final DiscardHashTagChangesDialog discardHashTagChangesDialog = DependencyManager.getInstance().getImplementationFor(DiscardHashTagChangesDialog.class);

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.editHashTags) EditText hashTagsEditText;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_hash_tags);
        ButterKnife.bind(this);
        setupActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.edit_outfit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.ready:
                finishActivity();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        discardHashTagChangesDialog.showAndOnPositiveSelected(this, super::onBackPressed);
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        final ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }
    }

    private void finishActivity() {
        final Intent editHashTagsResultIntent = new Intent();
        editHashTagsResultIntent.putExtra(Outfit.METADATA_KEY_HASHTAGS, hashTagsEditText.getText().toString());
        setResult(Activity.RESULT_OK, editHashTagsResultIntent);
        finish();
    }
}
