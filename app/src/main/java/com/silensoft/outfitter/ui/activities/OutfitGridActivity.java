package com.silensoft.outfitter.ui.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.ad.BannerAdActions;
import com.silensoft.outfitter.actions.appusage.AppUsageActions;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridActions;
import com.silensoft.outfitter.actions.promotion.PromoteAppRatingActions;
import com.silensoft.outfitter.actions.promotion.PromoteInAppPurchaseActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.ui.adapters.OutfitGridAdapterImpl;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridLoaderManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OutfitGridActivity extends AppCompatActivity {
    private static final int GRID_LAYOUT_SPAN_COUNT = 2;

    @SuppressWarnings("LawOfDemeter")
    private final BannerAdActions bannerAdActions = DependencyManager.getInstance().getImplementationFor(BannerAdActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final InAppBillingActions inAppBillingActions = DependencyManager.getInstance().getImplementationFor(InAppBillingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitGridLoaderManager outfitGridLoaderManager = DependencyManager.getInstance().getImplementationFor(OutfitGridLoaderManager.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitGridActions outfitGridActions = DependencyManager.getInstance().getImplementationFor(OutfitGridActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitOccasionActions randomOutfitOccasionActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitOccasionActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitSeasonActions randomOutfitSeasonActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitSeasonActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final AppUsageActions appUsageActions = DependencyManager.getInstance().getImplementationFor(AppUsageActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final PromoteAppRatingActions promoteAppRatingActions = DependencyManager.getInstance().getImplementationFor(PromoteAppRatingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final PromoteInAppPurchaseActions promoteInAppPurchaseActions = DependencyManager.getInstance().getImplementationFor(PromoteInAppPurchaseActions.class);

    @BindView(R.id.adView) AdView adView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.outfitGrid) RecyclerView outfitGrid;
    SearchView searchView;

    private FirebaseAnalytics firebaseAnalytics;
    private OutfitGridAdapterImpl outfitGridAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_outfit_grid);
        ButterKnife.bind(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setSupportActionBar(toolbar);
        appUsageActions.updateAppUsageInfo(this);
        promoteAppRatingActions.showRateAppDialogWhenAppropriate(this, firebaseAnalytics);
        promoteInAppPurchaseActions.showInAppPurchaseDialogWhenAppropriate(this, firebaseAnalytics);
        final Outfit prototypeOutfit = getPrototypeOutfit(savedInstanceState);
        outfitGridActions.showOutfitsByHashTags(this, getIntent(), firebaseAnalytics);
        outfitGrid.setLayoutManager(new GridLayoutManager(this, GRID_LAYOUT_SPAN_COUNT));
        outfitGridAdapter = new OutfitGridAdapterImpl(this);
        outfitGrid.setAdapter(outfitGridAdapter);
        outfitGridLoaderManager.initialize(this, outfitGridAdapter, searchView, prototypeOutfit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        outfitGridAdapter.notifyDataSetChanged();
        final List<InAppBillingSkuType> purchases = new ArrayList<>(Arrays.asList(InAppBillingSkuType.PLUS_PACKAGE, InAppBillingSkuType.PRO_SUBSCRIPTION));
        inAppBillingActions.doOnPurchasesUpdated(this, () ->
                inAppBillingActions.hasPurchasedAnyOf(purchases, OptionalImpl.empty(), OptionalImpl.of(() -> {
                    adView.setVisibility(View.VISIBLE);
                    bannerAdActions.showAd(adView);
                })));
    }

    @Override
    protected void onPause() {
        super.onPause();
        inAppBillingActions.stopListeningPurchaseUpdates(this);
    }

    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        searchView.setQuery("", false);
        searchView.setIconified(true);
        outfitGridLoaderManager.persistPrototypeOutfitToBundle(bundle);
        super.onSaveInstanceState(bundle);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.outfit_grid_menu, menu);
        setupHashTagSearch(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        return outfitGridActions.handleMenuItemSelection(this, menuItem, firebaseAnalytics);
    }

    private void setupHashTagSearch(final Menu menu) {
        final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(true);
        }
    }

    @NonNull
    private Outfit getPrototypeOutfit(final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            return outfitFactory.createOutfitFromBundle(savedInstanceState);
        } else {
            final OutfitOccasion outfitOccasion = randomOutfitOccasionActions.getCurrentOccasion();
            final OutfitSeason outfitSeason = randomOutfitSeasonActions.getCurrentSeason();
            return outfitFactory.createOutfit(outfitOccasion, outfitSeason);
        }
    }
}
