package com.silensoft.outfitter.ui.activities;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.ad.InterstitialAdActions;
import com.silensoft.outfitter.actions.appusage.AppUsageActions;
import com.silensoft.outfitter.actions.geolocation.GeoLocationActions;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.actions.promotion.InviteFriendsActions;
import com.silensoft.outfitter.actions.promotion.PromoteAppRatingActions;
import com.silensoft.outfitter.actions.promotion.PromoteInAppPurchaseActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitActions;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.actions.cloud.OutfitsCloudStorageActions;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.ui.presenters.CurrentOccasionAndSeasonPresenter;
import com.silensoft.outfitter.ui.presenters.RandomOutfitPresenter;
import com.silensoft.outfitter.ui.components.CustomImageSwitcher;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionSelectorDialog;
import com.silensoft.outfitter.ui.managers.CustomAppTitleStyleManager;
import com.silensoft.outfitter.ui.managers.FullScreenContentManager;
import com.silensoft.outfitter.ui.managers.SwipeLeftOrRightImageSwitcherManager;
import com.silensoft.outfitter.ui.menus.MenuInflater;
import com.silensoft.outfitter.ui.menus.RandomOutfitActivityMenu;
import com.silensoft.outfitter.ui.presenters.WeatherForecastPresenter;
import com.silensoft.outfitter.utils.FailureReporter;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

// TODO: RELEASE: Add App indexing
@SuppressLint("GoogleAppIndexingApiWarning")
public class RandomOutfitActivity extends AppCompatActivity {
    public static final long INITIAL_DELAY_MILLIS = 100L;

    @SuppressWarnings("LawOfDemeter")
    private final InterstitialAdActions interstitialAdActions = DependencyManager.getInstance().getImplementationFor(InterstitialAdActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final AppUsageActions appUsageActions = DependencyManager.getInstance().getImplementationFor(AppUsageActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final CustomAppTitleStyleManager customAppTitleStyleManager = DependencyManager.getInstance().getImplementationFor(CustomAppTitleStyleManager.class);
    @SuppressWarnings("LawOfDemeter")
    private final FullScreenContentManager fullScreenContentManager = DependencyManager.getInstance().getImplementationFor(FullScreenContentManager.class);
    @SuppressWarnings("LawOfDemeter")
    private final InAppBillingActions inAppBillingActions = DependencyManager.getInstance().getImplementationFor(InAppBillingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final InviteFriendsActions inviteFriendsActions = DependencyManager.getInstance().getImplementationFor(InviteFriendsActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final MenuInflater menuInflater = DependencyManager.getInstance().getImplementationFor(MenuInflater.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitsCloudStorageActions outfitsCloudStorageActions = DependencyManager.getInstance().getImplementationFor(OutfitsCloudStorageActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitOccasionSelectorDialog outfitOccasionSelectorDialog = DependencyManager.getInstance().getImplementationFor(OutfitOccasionSelectorDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final PromoteAppRatingActions promoteAppRatingActions = DependencyManager.getInstance().getImplementationFor(PromoteAppRatingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final PromoteInAppPurchaseActions promoteInAppPurchaseActions = DependencyManager.getInstance().getImplementationFor(PromoteInAppPurchaseActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitActions randomOutfitActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitActivityMenu randomOutfitActivityMenu = DependencyManager.getInstance().getImplementationFor(RandomOutfitActivityMenu.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitPresenter randomOutfitPresenter = DependencyManager.getInstance().getImplementationFor(RandomOutfitPresenter.class);
    @SuppressWarnings("LawOfDemeter")
    private final SwipeLeftOrRightImageSwitcherManager outfitImageSwitcherManager = DependencyManager.getInstance().getImplementationFor(SwipeLeftOrRightImageSwitcherManager.class);
    @SuppressWarnings("LawOfDemeter")
    private final GeoLocationActions geoLocationActions = DependencyManager.getInstance().getImplementationFor(GeoLocationActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final WeatherForecastPresenter weatherForecastPresenter = DependencyManager.getInstance().getImplementationFor(WeatherForecastPresenter.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);
    @SuppressWarnings("LawOfDemeter")
    private final CurrentOccasionAndSeasonPresenter currentOccasionAndSeasonPresenter = DependencyManager.getInstance().getImplementationFor(CurrentOccasionAndSeasonPresenter.class);

    @BindView(R.id.bottomNavigationView) BottomNavigationView bottomNavigationView;
    @BindView(R.id.fullScreenContentView) View fullScreenContentView;
    @BindView(R.id.fullscreenContentControlsView) View fullScreenContentControlsView;
    @BindView(R.id.changeOccasion) FloatingActionButton changeOccasionButton;
    @BindView(R.id.outfitImageSwitcher) CustomImageSwitcher outfitImageSwitcher;
    @BindView(R.id.noOutfitsView) View noOutfitsView;
    @BindView(R.id.weatherForecastGridView) GridView weatherForecastGridView;
    @BindView(R.id.seasonAndOccasionIndicatorTextView) TextView occasionAndSeasonIndicatorTextView;

    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_random_outfit);
        ButterKnife.bind(this);
        Timber.plant(new Timber.DebugTree());
        geoLocationActions.startListeningGeoLocationChanges(this);
        weatherForecastPresenter.initialize(this, firebaseAnalytics, weatherForecastGridView);
        inAppBillingActions.setupInAppBilling(this, weatherForecastPresenter::presentNewWeatherForecastIfApplicable);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        randomOutfitActions.setCurrentSeasonAndOccasionFrom(savedInstanceState);
        currentOccasionAndSeasonPresenter.initialize(this, occasionAndSeasonIndicatorTextView);
        appUsageActions.updateAppUsageInfo(this);
        promoteAppRatingActions.showRateAppDialogWhenAppropriate(this, firebaseAnalytics);
        promoteInAppPurchaseActions.showInAppPurchaseDialogWhenAppropriate(this, firebaseAnalytics);
        fullScreenContentManager.initialize(getSupportActionBar(), fullScreenContentControlsView, fullScreenContentView, bottomNavigationView);
        outfitImageSwitcherManager.initialize(this, outfitImageSwitcher, this::showNextRandomOutfitPhoto, this::showPreviousOutfitPhoto);
        interstitialAdActions.initialize(this);
        randomOutfitActions.restartPresenting();
        customAppTitleStyleManager.setCustomAppTitleStyle(this, R.color.white, R.font.lobster);
        outfitsCloudStorageActions.initialize(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> (item.getItemId() == R.id.outfitGrid) && startOutfitGridActivity());
        changeOccasionButton.setOnClickListener(view -> outfitOccasionSelectorDialog.show(this, firebaseAnalytics));
        showNextRandomOutfitPhoto();
    }

    @Override
    protected void onStart() {
        super.onStart();
        outfitsCloudStorageActions.syncOutfitsWithCloudAfterSuccessfulSignIn(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        inAppBillingActions.doOnPurchasesUpdated(this, weatherForecastPresenter::presentUpdatedWeatherForecastIfApplicable);
    }

    @Override
    protected void onPause() {
        super.onPause();
        inAppBillingActions.stopListeningPurchaseUpdates(this);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        randomOutfitActions.saveCurrentSeasonAndOccasionTo(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPostCreate(final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fullScreenContentManager.delayedHide(INITIAL_DELAY_MILLIS);
    }

    @Override
    protected void onStop() {
        super.onStop();
        randomOutfitActions.stopPresenting();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        inAppBillingActions.terminateInAppBilling();
        geoLocationActions.stopListeningGeoLocationChanges();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        menuInflater.inflateMenu(this, R.menu.random_outfit_menu, menu);
        randomOutfitActivityMenu.associateMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        return randomOutfitActivityMenu.handleMenuItemSelection(this, menuItem, firebaseAnalytics);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        outfitActions.handleRequestPermissionResults(this, requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        outfitActions.startAddOutfitActivity(this, requestCode, resultCode, intent);
        outfitsCloudStorageActions.syncOutfitsWithCloudAfterSuccessfulSignIn(this, requestCode, resultCode, intent);
        outfitActions.addOutfitFromIntent(this, requestCode, resultCode, intent);
        inviteFriendsActions.logInvitedFriends(requestCode, resultCode, intent, firebaseAnalytics);
    }

    private void showNextRandomOutfitPhoto() {
        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.SHOW_NEXT_RANDOM_OUTFIT);
        randomOutfitPresenter.presentNextRandomOutfit(this, outfitImageSwitcher, noOutfitsView);
    }

    private void showPreviousOutfitPhoto() {
        analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.SHOW_PREVIOUS_OUTFIT);
        randomOutfitPresenter.presentPreviousViewedOutfit(this, outfitImageSwitcher);
    }

    private boolean startOutfitGridActivity() {
        final Intent outfitGridActivityIntent = new Intent(getApplicationContext(), OutfitGridActivity.class);

        try {
            startActivity(outfitGridActivityIntent);
        } catch (final ActivityNotFoundException exception) {
            failureReporter.reportFailure(exception);
        }

        return true;
    }
}
