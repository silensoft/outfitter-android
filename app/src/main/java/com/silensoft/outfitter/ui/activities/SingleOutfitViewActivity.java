package com.silensoft.outfitter.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.model.factory.OutfitFactory;
import com.silensoft.outfitter.types.Impl.OptionalImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleOutfitViewActivity extends AppCompatActivity {
    @SuppressWarnings("LawOfDemeter")
    private final OutfitFactory outfitFactory = DependencyManager.getInstance().getImplementationFor(OutfitFactory.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.singleOutfitPhotoView) ImageView singleOutfitPhotoView;
    @BindView(R.id.editOutfitButton) FloatingActionButton editOutfitButton;

    private FirebaseAnalytics firebaseAnalytics;
    private Outfit outfit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_single_outfit_view);
        ButterKnife.bind(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setupActionBar();
        outfit = outfitFactory.createOutfitFromIntent(getIntent());
        editOutfitButton.setOnClickListener(view -> outfitActions.startEditOutfitActivity(this, OptionalImpl.of(outfit)));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.single_outfit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        outfitActions.handleMenuItemId(this, menuItem.getItemId(), OptionalImpl.of(outfit), firebaseAnalytics);

        switch(menuItem.getItemId()) {
            case R.id.removeOutfit:
                finish();
                return true;
            default:
                return false;
        }
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        final ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }
    }

}
