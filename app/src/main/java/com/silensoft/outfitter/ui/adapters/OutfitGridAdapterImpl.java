package com.silensoft.outfitter.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.actions.outfitgrid.OutfitGridLoaderManager;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.dao.OutfitDao;
import com.silensoft.outfitter.utils.FileUtils;

public class OutfitGridAdapterImpl extends RecyclerView.Adapter<OutfitGridAdapterImpl.OutfitPhotoViewHolder> {
    @SuppressWarnings("LawOfDemeter")
    private final OutfitDao outfitDao = DependencyManager.getInstance().getImplementationFor(OutfitDao.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitGridLoaderManager outfitGridLoaderManager = DependencyManager.getInstance().getImplementationFor(OutfitGridLoaderManager.class);

    private final Context context;

    public OutfitGridAdapterImpl(@NonNull final Context context_) {
        this.context = context_;
    }

    @SuppressWarnings("MethodReturnOfConcreteClass")
    @Override
    public OutfitPhotoViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.outfit_grid_item, parent, false);
        return new OutfitPhotoViewHolder(view);
    }

    @SuppressWarnings({"MethodParameterOfConcreteClass", "LawOfDemeter", "FeatureEnvy"}) // Optional and POJO access
    @Override
    public void onBindViewHolder(final OutfitPhotoViewHolder outfitPhotoViewHolder, final int position) {
        outfitGridLoaderManager.getOutfitAtGridPosition(position).ifPresent(outfit -> {
            outfit.getPermanentPhotoFilePath()
                    .ifPresent(outfitPhotoFilePath -> FileUtils.loadImage(outfitPhotoFilePath)
                            .ifPresentOrElse(outfitPhotoBitmap -> {
                                outfitPhotoViewHolder.noOutfitPhotoTextView.setVisibility(View.GONE);
                                outfitPhotoViewHolder.outfitImageView.setVisibility(View.VISIBLE);
                                outfitPhotoViewHolder.outfitImageView.setImageBitmap(outfitPhotoBitmap);
                            }, () -> {
                                outfitPhotoViewHolder.noOutfitPhotoTextView.setVisibility(View.VISIBLE);
                                outfitPhotoViewHolder.outfitImageView.setVisibility(View.GONE);
                            }));
            outfitPhotoViewHolder.outfitImageView.setTag(outfit.getUuidStr());
            outfitPhotoViewHolder.outfitImageView.setOnClickListener(view -> outfitActions.startSingleOutfitViewActivity(context, outfit));
            outfitPhotoViewHolder.outfitSelectedCheckBox.setChecked(outfitGridLoaderManager.isOutfitSelected(outfit));
            outfitPhotoViewHolder.outfitSelectedCheckBox.setOnClickListener(view -> outfitGridLoaderManager.toggleOutfitSelected(outfit));
        });
    }

    @Override
    public int getItemCount() {
        return outfitGridLoaderManager.getOutfitCount();
    }

    class OutfitPhotoViewHolder extends RecyclerView.ViewHolder {
        ImageView outfitImageView;
        CheckBox outfitSelectedCheckBox;
        TextView noOutfitPhotoTextView;

        public OutfitPhotoViewHolder(final View view) {
            super(view);
            outfitImageView = view.findViewById(R.id.outfitGridItemImage);
            outfitSelectedCheckBox = view.findViewById(R.id.outfitGridItemCheckBox);
            noOutfitPhotoTextView = view.findViewById(R.id.noOutfitPhotoTextView);
        }
    }
}
