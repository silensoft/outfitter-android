package com.silensoft.outfitter.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.model.outfit.Outfit;

import java.util.ArrayList;
import java.util.Arrays;


public class OutfitOptionsArrayAdapterImpl extends ArrayAdapter<String> {
    private final Context context;
    private final Outfit outfit;

    public OutfitOptionsArrayAdapterImpl(@NonNull final Context context_, @NonNull final Outfit outfit_) {

        super(context_, android.R.layout.simple_list_item_2, new ArrayList<>(Arrays.asList(
                context_.getResources().getString(R.string.occasion_option_name),
                context_.getResources().getString(R.string.season_option_name),
                context_.getResources().getString(R.string.hashtags_option_name))));

        this.context = context_;
        this.outfit = outfit_;
    }

    @SuppressWarnings("FeatureEnvy") // Accesses only POJO
    @NonNull
    @Override
    public View getView(final int position, final View convertView, @NonNull final ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        final TextView text1 = view.findViewById(android.R.id.text1);
        final TextView text2 = view.findViewById(android.R.id.text2);
        text1.setText(getItem(position));

        if (position == 0) {
            text2.setText(outfit.getOccasionsString(context));
        } else if (position == 1) {
            text2.setText(outfit.getSeasonsString(context));
        } else {
            text2.setText(outfit.getHashTagsString());
        }

        return view;
    }
}
