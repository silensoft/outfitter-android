package com.silensoft.outfitter.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;

import java.util.List;

public class WeatherForecastArrayAdapterImpl extends ArrayAdapter<ThreeHourWeatherForecast> {

    public WeatherForecastArrayAdapterImpl(@NonNull final Context context, @NonNull final List<ThreeHourWeatherForecast> threeHourWeatherForecasts) {
        super(context, R.layout.weather_forecast_item, threeHourWeatherForecasts);
    }

    @SuppressWarnings("FeatureEnvy") // POJO access
    @NonNull
    @Override
    public View getView(final int position, final View convertView, @NonNull final ViewGroup parent) {
        final View view = super.getView(position, convertView, parent);
        final TextView  weatherForecastHour = view.findViewById(R.id.weatherForecastHour);
        final ImageView weatherForecastImage = view.findViewById(R.id.weatherForecastImage);
        final TextView weatherForecastTemperature = view.findViewById(R.id.weatherForecastTemperature);
        final ThreeHourWeatherForecast threeHourWeatherForecast = getItem(position);

        weatherForecastHour.setText(threeHourWeatherForecast.getHourStr());
        weatherForecastImage.setImageBitmap(threeHourWeatherForecast.getWeatherConditionImageBitmap());
        weatherForecastTemperature.setText(threeHourWeatherForecast.getTemperatureStr());

        return view;
    }
}
