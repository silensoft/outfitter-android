package com.silensoft.outfitter.ui.components;


import android.content.Context;
import android.widget.ImageSwitcher;

public class CustomImageSwitcher extends ImageSwitcher {
    public CustomImageSwitcher(final Context context) {
        super(context);
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return false;
    }
}
