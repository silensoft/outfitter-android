package com.silensoft.outfitter.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Action;

import java.util.List;

public interface ChooseOutfitPhotoSourceDialog {
    void show(@NonNull final Activity context);
}
