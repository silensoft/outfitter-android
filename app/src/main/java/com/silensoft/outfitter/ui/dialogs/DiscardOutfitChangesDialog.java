package com.silensoft.outfitter.ui.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.types.Action;

public interface DiscardOutfitChangesDialog {
    void showAndOnPositiveSelected(@NonNull final Context context, @NonNull final Action onPositiveSelectedAction);
}
