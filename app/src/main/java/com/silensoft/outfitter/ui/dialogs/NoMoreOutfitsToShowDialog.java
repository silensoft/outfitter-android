package com.silensoft.outfitter.ui.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

public interface NoMoreOutfitsToShowDialog {
    void show(@NonNull final Context context);
}
