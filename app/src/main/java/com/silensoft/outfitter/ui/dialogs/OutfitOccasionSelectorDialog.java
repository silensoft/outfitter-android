package com.silensoft.outfitter.ui.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;

public interface OutfitOccasionSelectorDialog {
    void show(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics);
}
