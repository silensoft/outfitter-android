package com.silensoft.outfitter.ui.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.outfit.Outfit;

public interface OutfitSeasonsSelectorDialog {
    void show(@NonNull final Context context, @NonNull final Outfit outfit);
}
