package com.silensoft.outfitter.ui.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.model.inappbilling.InAppBillingSku;
import com.silensoft.outfitter.types.Action;

public interface PromoteInAppPurchaseDialog {
    void showAndOnPositiveOrNegativeSelected(@NonNull final Context context, @NonNull final InAppBillingSku inAppBillingSku,
                                             @NonNull final Action onPositiveSelectedAction, @NonNull final Action onNegativeSelectedAction);
}
