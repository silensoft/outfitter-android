package com.silensoft.outfitter.ui.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;

public interface PurchaseSelectorDialog {
    void show(@NonNull final Context context);
}
