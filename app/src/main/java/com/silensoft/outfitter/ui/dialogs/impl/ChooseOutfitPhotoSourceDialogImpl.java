package com.silensoft.outfitter.ui.dialogs.impl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.ui.dialogs.ChooseOutfitPhotoSourceDialog;

public class ChooseOutfitPhotoSourceDialogImpl implements ChooseOutfitPhotoSourceDialog {
    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);

    @Override
    public void show(@NonNull final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.add_outfit_method)
                .setItems(R.array.add_outfit_methods, (dialog, position) -> {
                    if (position == 0) {
                        outfitActions.pickOutfitPhotoFromCamera(activity);
                    } else if (position == 1) {
                        outfitActions.pickOutfitPhotoFromGallery(activity);
                    }
                });
        builder.show();
    }
}
