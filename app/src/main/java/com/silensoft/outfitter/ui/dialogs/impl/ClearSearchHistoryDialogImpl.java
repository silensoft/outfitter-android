package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.ui.dialogs.ClearSearchHistoryDialog;

public class ClearSearchHistoryDialogImpl implements ClearSearchHistoryDialog {
    @Override
    public void showAndOnPositiveSelected(@NonNull final Context context, @NonNull final Action onPositiveSelectedAction) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.clear_search_history_title)
                .setMessage(R.string.clear_search_history_message)
                .setPositiveButton(R.string.yes, (dialog, id) -> onPositiveSelectedAction.perform())
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
