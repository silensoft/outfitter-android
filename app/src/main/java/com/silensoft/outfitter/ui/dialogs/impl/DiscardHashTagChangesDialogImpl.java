package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.ui.dialogs.DiscardHashTagChangesDialog;

public class DiscardHashTagChangesDialogImpl implements DiscardHashTagChangesDialog {
    public void showAndOnPositiveSelected(@NonNull final Context context, @NonNull final Action onPositiveSelectedAction) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.discard_hashtags_changes_title)
                .setMessage(R.string.discard_hashtags_changes_message)
                .setPositiveButton(R.string.ok, (dialog, id) -> onPositiveSelectedAction.perform())
                .setNegativeButton(R.string.cancel, null)
                .show();
    }
}
