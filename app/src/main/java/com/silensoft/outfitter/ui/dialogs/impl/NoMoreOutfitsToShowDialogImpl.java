package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.ui.dialogs.NoMoreOutfitsToShowDialog;

public class NoMoreOutfitsToShowDialogImpl implements NoMoreOutfitsToShowDialog {
    @Override
    public void show(@NonNull final Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.no_more_outfits_title)
                .setMessage(R.string.no_more_outfits_message)
                .setNeutralButton(R.string.restart, null)
                .show();
    }
}
