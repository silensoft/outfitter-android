package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.OutfitOccasion;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionSelectorDialog;
import com.silensoft.outfitter.ui.presenters.CurrentOccasionAndSeasonPresenter;

public class OutfitOccasionSelectorDialogImpl implements OutfitOccasionSelectorDialog {
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitOccasionActions randomOutfitOccasionActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitOccasionActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final CurrentOccasionAndSeasonPresenter currentOccasionAndSeasonPresenter = DependencyManager.getInstance().getImplementationFor(CurrentOccasionAndSeasonPresenter.class);

    @Override
    public void show(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.outfit_for_occasion)
                .setItems(R.array.occasions, (dialog, position) -> {
                    analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.CHANGE_OCCASION, OutfitOccasion.fromIndexToString(context, position));
                    randomOutfitOccasionActions.setCurrentOccasion(OutfitOccasion.fromIndex(position));
                    currentOccasionAndSeasonPresenter.updateOccasionAndSeason(context);
                }).show();
    }
}
