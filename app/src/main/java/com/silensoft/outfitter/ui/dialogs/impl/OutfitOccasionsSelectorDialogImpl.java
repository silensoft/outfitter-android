package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.ui.dialogs.OutfitOccasionsSelectorDialog;
import com.silensoft.outfitter.utils.ListUtils;

import java.util.List;

public class OutfitOccasionsSelectorDialogImpl implements OutfitOccasionsSelectorDialog {
    @Override
    public void show(@NonNull final Context context, @NonNull final Outfit outfit) {
        final List<Integer> outfitOccasionIndexes = outfit.getOccasionIndexes();
        final boolean[] checkedItems = ListUtils.getHasItemAtIndexArrayFromIndexes(outfitOccasionIndexes);

        final AlertDialog outfitOccasionsSelectorDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.outfit_for_occasion)
                .setMultiChoiceItems(R.array.occasions, checkedItems, (dialog, outfitOccasionIndex, isChecked) -> {
                    if (isChecked) {
                        outfitOccasionIndexes.add(outfitOccasionIndex);
                    } else {
                        outfitOccasionIndexes.remove((Integer)outfitOccasionIndex);
                    }
                })
                .setPositiveButton(R.string.ok, (dialog, id) -> outfit.setOccasionIndexes(outfitOccasionIndexes))
                .setNegativeButton(R.string.cancel, null)
                .show();

        outfitOccasionsSelectorDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(view -> {
            if (outfitOccasionIndexes.isEmpty()) {
                Toast.makeText(context, R.string.select_at_least_one_occasion, Toast.LENGTH_SHORT).show();
            } else {
                outfitOccasionsSelectorDialog.dismiss();
            }
        });
    }
}
