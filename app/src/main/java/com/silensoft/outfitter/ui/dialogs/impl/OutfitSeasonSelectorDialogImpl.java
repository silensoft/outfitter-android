package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.analytics.AnalyticsEvent;
import com.silensoft.outfitter.actions.analytics.AnalyticsEventLogger;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.OutfitSeason;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonSelectorDialog;
import com.silensoft.outfitter.ui.presenters.CurrentOccasionAndSeasonPresenter;

public class OutfitSeasonSelectorDialogImpl implements OutfitSeasonSelectorDialog {
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitSeasonActions randomOutfitSeasonActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitSeasonActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final AnalyticsEventLogger analyticsEventLogger = DependencyManager.getInstance().getImplementationFor(AnalyticsEventLogger.class);
    @SuppressWarnings("LawOfDemeter")
    private final CurrentOccasionAndSeasonPresenter currentOccasionAndSeasonPresenter = DependencyManager.getInstance().getImplementationFor(CurrentOccasionAndSeasonPresenter.class);

    @Override
    public void show(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.outfit_for_season)
                .setItems(R.array.seasons, (dialog, position) -> {
                    analyticsEventLogger.logAnalyticsEvent(firebaseAnalytics, AnalyticsEvent.CHANGE_SEASON, OutfitSeason.fromIndexToString(context, position));
                    randomOutfitSeasonActions.setCurrentSeason(OutfitSeason.fromIndex(position));
                    currentOccasionAndSeasonPresenter.updateOccasionAndSeason(context);
                })
                .show();
    }
}
