package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonsSelectorDialog;
import com.silensoft.outfitter.utils.ListUtils;

import java.util.List;

public class OutfitSeasonsSelectorDialogImpl implements OutfitSeasonsSelectorDialog {
    @Override
    public void show(@NonNull final Context context, @NonNull final Outfit outfit) {
        final List<Integer> outfitSeasonIndexes = outfit.getSeasonIndexes();
        final boolean[] checkedItems = ListUtils.getHasItemAtIndexArrayFromIndexes(outfitSeasonIndexes);

        new AlertDialog.Builder(context)
                .setTitle(R.string.outfit_for_season)
                .setMultiChoiceItems(R.array.seasons, checkedItems, (dialog, outfitSeasonIndex, isChecked) -> {
                    if (isChecked) {
                        outfitSeasonIndexes.add(outfitSeasonIndex);
                    } else {
                        outfitSeasonIndexes.remove((Integer)outfitSeasonIndex);
                    }
                })
                .setPositiveButton(R.string.ok, (dialog, id) -> outfit.setSeasonIndexes(outfitSeasonIndexes))
                .setNegativeButton(R.string.cancel, null)
                .show();
    }
}
