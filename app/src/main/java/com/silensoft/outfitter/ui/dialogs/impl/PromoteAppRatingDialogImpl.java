package com.silensoft.outfitter.ui.dialogs.impl;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.ui.dialogs.PromoteAppRatingDialog;

public class PromoteAppRatingDialogImpl implements PromoteAppRatingDialog {
    @Override
    public void showAndOnPositiveOrNegativeSelected(@NonNull final Context context, @NonNull final Action onPositiveSelectedAction, @NonNull final Action onNegativeSelectedAction) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.promote_app_rating_title)
                .setMessage(R.string.promote_app_rating_message)
                .setPositiveButton(R.string.rate, (dialog, id) -> onPositiveSelectedAction.perform())
                .setNeutralButton(R.string.no_thanks, null)
                .setNegativeButton(R.string.dont_ask_again, (dialog, id) -> onNegativeSelectedAction.perform())
                .show();
    }
}
