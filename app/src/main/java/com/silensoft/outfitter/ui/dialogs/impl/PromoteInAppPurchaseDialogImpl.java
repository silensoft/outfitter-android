package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSku;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.ui.dialogs.PromoteInAppPurchaseDialog;

public class PromoteInAppPurchaseDialogImpl implements PromoteInAppPurchaseDialog {
    @Override
    public void showAndOnPositiveOrNegativeSelected(@NonNull final Context context, @NonNull final InAppBillingSku inAppBillingSku,
                                                    @NonNull final Action onPositiveSelectedAction, @NonNull final Action onNegativeSelectedAction) {
        new AlertDialog.Builder(context)
                .setTitle(inAppBillingSku.getPromotionTitle(context))
                .setMessage(inAppBillingSku.getPromotionMessage(context))
                .setPositiveButton(R.string.purchase, (dialog, id) -> onPositiveSelectedAction.perform())
                .setNeutralButton(R.string.no_thanks, null)
                .setNegativeButton(R.string.dont_ask_again, (dialog, id) -> onNegativeSelectedAction.perform())
                .show();
    }
}
