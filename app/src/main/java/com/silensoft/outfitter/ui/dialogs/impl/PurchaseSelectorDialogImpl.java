package com.silensoft.outfitter.ui.dialogs.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSku;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.ui.activities.SettingsActivity;
import com.silensoft.outfitter.ui.dialogs.PurchaseSelectorDialog;
import com.silensoft.outfitter.utils.FailureReporter;

public class PurchaseSelectorDialogImpl implements PurchaseSelectorDialog {
    @SuppressWarnings("LawOfDemeter")
    private final InAppBillingActions inAppBillingActions = DependencyManager.getInstance().getImplementationFor(InAppBillingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final FailureReporter failureReporter = DependencyManager.getInstance().getImplementationFor(FailureReporter.class);

    private int selection = -1;

    @Override
    public void show(@NonNull final Context context) {
        inAppBillingActions.fetchSellableSkus(sellableSkus -> {
            final String[] salesItemLines = new String[sellableSkus.size()];
            for (int i = 0; i < sellableSkus.size(); i++) {
                salesItemLines[i] = sellableSkus.get(i).getSalesItemLine();
            }

            new AlertDialog.Builder(context)
                    .setTitle(R.string.purchase)
                    .setSingleChoiceItems(salesItemLines, 0, (dialogInterface, position) -> selection = position)
                    .setPositiveButton(R.string.purchase, (dialogInterface, position) -> {
                        if (selection != -1) {
                            final InAppBillingSku sku = sellableSkus.get(selection);
                            inAppBillingActions.purchase(context, sku);
                            if (sku.getType() == InAppBillingSkuType.PRO_SUBSCRIPTION) {
                                final Intent settingsActivityIntent = new Intent(context, SettingsActivity.class);
                                try {
                                    context.startActivity(settingsActivityIntent);
                                } catch (final ActivityNotFoundException exception) {
                                    failureReporter.reportFailure(exception);
                                }
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        });

    }
}
