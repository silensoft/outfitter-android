package com.silensoft.outfitter.ui.managers;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

public interface CustomAppTitleStyleManager {
    void setCustomAppTitleStyle(@NonNull final AppCompatActivity activity, final int colorResourceId, final int fontResourceId);
}
