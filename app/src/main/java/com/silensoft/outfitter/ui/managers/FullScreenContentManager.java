package com.silensoft.outfitter.ui.managers;

import android.support.v7.app.ActionBar;
import android.view.View;

public interface FullScreenContentManager {
    void initialize(final ActionBar actionBar, final View controlsView, final View contentView, final View controlView);
    void delayedHide(long delayMillis);
}
