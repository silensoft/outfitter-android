package com.silensoft.outfitter.ui.managers;

import android.content.Context;

import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.ui.components.CustomImageSwitcher;

public interface SwipeLeftOrRightImageSwitcherManager {
    @SuppressWarnings("MethodParameterOfConcreteClass")
    void initialize(final Context context, final CustomImageSwitcher imageSwitcher, final Action swipeLeftAction, final Action swipeRightAction);
}
