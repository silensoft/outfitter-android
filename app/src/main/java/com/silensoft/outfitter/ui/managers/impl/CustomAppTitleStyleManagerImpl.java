package com.silensoft.outfitter.ui.managers.impl;

import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.silensoft.outfitter.ui.managers.CustomAppTitleStyleManager;

public class CustomAppTitleStyleManagerImpl implements CustomAppTitleStyleManager{
    @Override
    public void setCustomAppTitleStyle(@NonNull final AppCompatActivity activity, final int colorResourceId, final int fontResourceId) {
        final int appTitleResourceId = activity.getResources().getIdentifier("action_bar_title", "id", "android");
        final TextView appTitle = activity.findViewById(appTitleResourceId);
        appTitle.setTextColor(activity.getResources().getColor(colorResourceId));
        appTitle.setTypeface(ResourcesCompat.getFont(activity, fontResourceId));
    }
}
