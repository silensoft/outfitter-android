package com.silensoft.outfitter.ui.managers.impl;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.silensoft.outfitter.ui.managers.FullScreenContentManager;

public class FullScreenContentManagerImpl implements FullScreenContentManager {
    private static final boolean AUTO_HIDE = true;
    private static final long AUTO_HIDE_DELAY_MILLIS = 3000L;
    private static final long UI_ANIMATION_DELAY = 300L;

    private final Runnable hideRunnable = this::hide;
    private Runnable hidePart2Runnable;
    private Runnable showPart2Runnable;
    private final Handler hideHandler = new Handler();
    private boolean visible = true;

    private ActionBar actionBar;
    private View controlsView;
    private View contentView;

    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener delayHideTouchListener = (view, motionEvent) -> {
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS);
        }
        return false;
    };

    @SuppressLint("InlinedApi")
    @Override
    public void initialize(final ActionBar actionBar_, final View controlsView_, final View contentView_, final View controlView) {
        this.actionBar = actionBar_;
        this.controlsView = controlsView_;
        this.contentView = contentView_;

        this.hidePart2Runnable = () -> contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        this.showPart2Runnable = () -> {
             if (actionBar != null) {
                 actionBar.show();
             }
             controlsView.setVisibility(View.VISIBLE);
        };

        contentView.setOnClickListener(view -> toggle());
        controlView.setOnTouchListener(delayHideTouchListener);
    }

    public void delayedHide(final long delayMillis) {
        hideHandler.removeCallbacks(hideRunnable);
        hideHandler.postDelayed(hideRunnable, delayMillis);
    }

    private void toggle() {
        if (visible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        if (actionBar != null) {
            actionBar.hide();
        }
        controlsView.setVisibility(View.GONE);
        visible = false;
        hideHandler.removeCallbacks(showPart2Runnable);
        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        visible = true;
        hideHandler.removeCallbacks(hidePart2Runnable);
        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY);
    }
}
