package com.silensoft.outfitter.ui.managers.impl;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.ui.components.CustomImageSwitcher;
import com.silensoft.outfitter.ui.managers.SwipeLeftOrRightImageSwitcherManager;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.ui.presenters.WeatherForecastPresenter;


public class SwipeLeftOrRightImageSwitcherManagerImpl implements SwipeLeftOrRightImageSwitcherManager {
    @SuppressWarnings("LawOfDemeter")
    private final WeatherForecastPresenter weatherForecastPresenter = DependencyManager.getInstance().getImplementationFor(WeatherForecastPresenter.class);

    private float x1;

    @SuppressLint("ClickableViewAccessibility")
    @SuppressWarnings("MethodParameterOfConcreteClass")
    public void initialize(final Context context, final CustomImageSwitcher imageSwitcher, final Action swipeLeftAction, final Action swipeRightAction) {
        imageSwitcher.setFactory(() -> {
            final ImageView myView = new ImageView(context);
            myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            myView.setLayoutParams(new FrameLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
            return myView;
        });

        final Animation inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        final Animation outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right);
        imageSwitcher.setInAnimation(inAnimation);
        imageSwitcher.setOutAnimation(outAnimation);

        imageSwitcher.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x1 = motionEvent.getX();
                    break;

                case MotionEvent.ACTION_UP:
                    handleTouchUpEvent(view, motionEvent, swipeLeftAction, swipeRightAction);
                    break;

                default:
            }
            return false;
        });
    }

    private void handleTouchUpEvent(@NonNull final View view, @NonNull final MotionEvent motionEvent, final Action swipeLeftAction, final Action swipeRightAction) {
        view.performClick();
        weatherForecastPresenter.hideWeatherForecast();
        final float x2 = motionEvent.getX();
        final float deltaX = x2 - x1;
        if (deltaX < 0.0F) {
            swipeRightAction.perform();
        } else if(deltaX > 0.0F) {
            swipeLeftAction.perform();
        }
    }
}
