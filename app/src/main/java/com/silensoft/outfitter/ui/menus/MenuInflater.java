package com.silensoft.outfitter.ui.menus;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.Menu;

public interface MenuInflater {
    void inflateMenu(@NonNull final Activity activity, final int menuResourceId, @NonNull final Menu menu);
}
