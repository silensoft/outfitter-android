package com.silensoft.outfitter.ui.menus;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.model.outfit.OutfitsState;

public interface RandomOutfitActivityMenu {
    void associateMenu(@NonNull final Menu menu);
    boolean handleMenuItemSelection(@NonNull final Activity activity, final MenuItem menuItem, @NonNull final FirebaseAnalytics firebaseAnalytics_);
    void setOutfitsState(@NonNull final OutfitsState outfitsState);
}
