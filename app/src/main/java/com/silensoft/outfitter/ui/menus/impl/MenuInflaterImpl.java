package com.silensoft.outfitter.ui.menus.impl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.Menu;

import com.silensoft.outfitter.ui.menus.MenuInflater;

public class MenuInflaterImpl implements MenuInflater {
    @Override
    public void inflateMenu(@NonNull final Activity activity, final int menuResourceId, @NonNull final Menu menu) {
        activity.getMenuInflater().inflate(menuResourceId, menu);
    }
}
