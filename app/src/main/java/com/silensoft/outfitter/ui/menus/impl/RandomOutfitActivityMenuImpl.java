package com.silensoft.outfitter.ui.menus.impl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.R;
import com.silensoft.outfitter.actions.promotion.InviteFriendsActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.actions.outfit.OutfitActions;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.model.outfit.OutfitsState;
import com.silensoft.outfitter.ui.dialogs.ChooseOutfitPhotoSourceDialog;
import com.silensoft.outfitter.ui.dialogs.OutfitSeasonSelectorDialog;
import com.silensoft.outfitter.ui.dialogs.PurchaseSelectorDialog;
import com.silensoft.outfitter.ui.menus.RandomOutfitActivityMenu;

public class RandomOutfitActivityMenuImpl implements RandomOutfitActivityMenu {
    @SuppressWarnings("LawOfDemeter")
    private final ChooseOutfitPhotoSourceDialog chooseOutfitPhotoSourceDialog = DependencyManager.getInstance().getImplementationFor(ChooseOutfitPhotoSourceDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final InviteFriendsActions inviteFriendsActions = DependencyManager.getInstance().getImplementationFor(InviteFriendsActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitActions outfitActions = DependencyManager.getInstance().getImplementationFor(OutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final OutfitSeasonSelectorDialog outfitSeasonSelectorDialog = DependencyManager.getInstance().getImplementationFor(OutfitSeasonSelectorDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final PurchaseSelectorDialog purchaseSelectorDialog = DependencyManager.getInstance().getImplementationFor(PurchaseSelectorDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitActions randomOutfitActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitActions.class);

    @NonNull private Optional<Menu> optMenu = OptionalImpl.empty();

    @Override
    public void associateMenu(@NonNull final Menu menu) {
        optMenu = OptionalImpl.of(menu);
    }

    @Override
    public boolean handleMenuItemSelection(@NonNull final Activity activity, final MenuItem menuItem, @NonNull final FirebaseAnalytics firebaseAnalytics) {
        final int id = menuItem.getItemId();
        final Optional<Outfit> currentOutfit = randomOutfitActions.getCurrentOutfit();

        switch(id) {
            case R.id.changeSeason:
                outfitSeasonSelectorDialog.show(activity, firebaseAnalytics);
                return true;
            case R.id.addOutfit:
                chooseOutfitPhotoSourceDialog.show(activity);
                return true;
            case R.id.shareOutfit:
            case R.id.editOutfit:
                outfitActions.handleMenuItemId(activity, id, currentOutfit, firebaseAnalytics);
                return true;
            case R.id.removeOutfit:
                outfitActions.handleMenuItemId(activity, id, currentOutfit, firebaseAnalytics);
                randomOutfitActions.removeCurrentOutfit();
                return true;
            case R.id.tellFriends:
                inviteFriendsActions.inviteFriends(activity);
                return true;
            case R.id.purchase:
                purchaseSelectorDialog.show(activity);
                return true;
            default:
                throw new IllegalArgumentException("Invalid menu item id: " + Integer.toString(id));
        }
    }

    @Override
    public void setOutfitsState(@NonNull final OutfitsState outfitsState) {
        optMenu.ifPresent(menu -> {
            menu.getItem(2).setEnabled(outfitsState == OutfitsState.HAS_OUTFITS);
            menu.getItem(3).setEnabled(outfitsState == OutfitsState.HAS_OUTFITS);
            menu.getItem(4).setEnabled(outfitsState == OutfitsState.HAS_OUTFITS);
        });
    }
}
