package com.silensoft.outfitter.ui.presenters;

public interface AdPresenter {
    void presentAdWhenApplicable();
}
