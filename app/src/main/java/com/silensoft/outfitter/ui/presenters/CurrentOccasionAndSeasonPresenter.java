package com.silensoft.outfitter.ui.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

public interface CurrentOccasionAndSeasonPresenter {
    void initialize(@NonNull final Context context, @NonNull final TextView currentOccasionAndSeasonTextView);
    void updateOccasionAndSeason(@NonNull final Context context);
}
