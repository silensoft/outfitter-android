package com.silensoft.outfitter.ui.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.silensoft.outfitter.ui.components.CustomImageSwitcher;

@SuppressWarnings("MethodParameterOfConcreteClass") // UI View object
public interface RandomOutfitPresenter {
    void presentNextRandomOutfit(@NonNull final Context context, @NonNull final CustomImageSwitcher outfitImageSwitcher, @NonNull final View noOutfitsView);
    void presentPreviousViewedOutfit(@NonNull final Context context, @NonNull final CustomImageSwitcher outfitImageSwitcher);
}
