package com.silensoft.outfitter.ui.presenters;

import android.content.Context;
import android.support.annotation.NonNull;

public interface ToastPresenter {
    void presentToast(@NonNull final Context context, final int messageResourceId, final int duration);
}
