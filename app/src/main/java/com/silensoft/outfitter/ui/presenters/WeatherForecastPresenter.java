package com.silensoft.outfitter.ui.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.GridView;

import com.google.firebase.analytics.FirebaseAnalytics;

public interface WeatherForecastPresenter {
    void initialize(@NonNull final Context context, @NonNull final FirebaseAnalytics firebaseAnalytics, @NonNull final GridView gridView);
    void hideWeatherForecast();
    void presentNewWeatherForecastIfApplicable();
    void presentUpdatedWeatherForecastIfApplicable();

}
