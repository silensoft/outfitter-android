package com.silensoft.outfitter.ui.presenters.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitOccasionActions;
import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitSeasonActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.ui.presenters.CurrentOccasionAndSeasonPresenter;

public class CurrentOccasionAndSeasonPresenterImpl implements CurrentOccasionAndSeasonPresenter {
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitOccasionActions randomOutfitOccasionActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitOccasionActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitSeasonActions randomOutfitSeasonActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitSeasonActions.class);

    private Optional<TextView> currentOccasionAndSeasonTextView = OptionalImpl.empty();

    @Override
    public void initialize(@NonNull final Context context, @NonNull final TextView currentOccasionAndSeasonTextView_) {
        currentOccasionAndSeasonTextView = OptionalImpl.of(currentOccasionAndSeasonTextView_);
        updateOccasionAndSeason(context);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void updateOccasionAndSeason(@NonNull final Context context) {
        final String outfitOccasionStr = randomOutfitOccasionActions.getCurrentOccasionStr(context);
        final String outfitSeasonStr = randomOutfitSeasonActions.getCurrentSeasonStr(context);
        currentOccasionAndSeasonTextView.ifPresent(textView -> textView.setText(outfitSeasonStr + System.getProperty("line.separator") + outfitOccasionStr));
    }
}
