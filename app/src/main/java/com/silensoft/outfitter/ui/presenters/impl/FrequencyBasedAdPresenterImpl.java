package com.silensoft.outfitter.ui.presenters.impl;

import com.silensoft.outfitter.actions.ad.InterstitialAdActions;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.ui.presenters.AdPresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FrequencyBasedAdPresenterImpl implements AdPresenter {
    @SuppressWarnings("LawOfDemeter")
    private final InterstitialAdActions interstitialAdActions = DependencyManager.getInstance().getImplementationFor(InterstitialAdActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final InAppBillingActions inAppBillingActions = DependencyManager.getInstance().getImplementationFor(InAppBillingActions.class);

    private final int frequencyMultiplier;
    private int currentFrequency;
    private int presentationTriedCount;

    public FrequencyBasedAdPresenterImpl(final int baseFrequency_, final int frequencyMultiplier_) {
        this.currentFrequency = baseFrequency_;
        this.frequencyMultiplier = frequencyMultiplier_;
    }

    @Override
    public void presentAdWhenApplicable() {
        final List<InAppBillingSkuType> purchases = new ArrayList<>(Arrays.asList(InAppBillingSkuType.PLUS_PACKAGE, InAppBillingSkuType.PRO_SUBSCRIPTION));
        inAppBillingActions.hasPurchasedAnyOf(purchases, OptionalImpl.empty(), OptionalImpl.of(() -> {
            if (presentationTriedCount % currentFrequency == 0) {
                interstitialAdActions.showAd();
                currentFrequency *= frequencyMultiplier;
            }
            presentationTriedCount++;
        }));
    }
}
