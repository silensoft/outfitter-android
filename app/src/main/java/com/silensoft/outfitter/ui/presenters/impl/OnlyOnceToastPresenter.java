package com.silensoft.outfitter.ui.presenters.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.silensoft.outfitter.ui.presenters.ToastPresenter;

public class OnlyOnceToastPresenter implements ToastPresenter {
    private boolean isPresented;

    @Override
    public void presentToast(@NonNull final Context context, final int messageResourceId, final int duration) {
        if (isPresented) {
            return;
        }

        Toast.makeText(context, messageResourceId, Toast.LENGTH_SHORT).show();
        isPresented = true;
    }
}
