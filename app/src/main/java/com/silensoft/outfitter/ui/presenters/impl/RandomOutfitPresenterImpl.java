package com.silensoft.outfitter.ui.presenters.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.silensoft.outfitter.actions.randomoutfit.RandomOutfitActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.dependencymanagement.bindings.UiBindingsImpl;
import com.silensoft.outfitter.ui.components.CustomImageSwitcher;
import com.silensoft.outfitter.ui.dialogs.NoMoreOutfitsToShowDialog;
import com.silensoft.outfitter.ui.presenters.AdPresenter;
import com.silensoft.outfitter.model.outfit.Outfit;
import com.silensoft.outfitter.ui.presenters.RandomOutfitPresenter;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.model.outfit.OutfitsState;
import com.silensoft.outfitter.ui.menus.RandomOutfitActivityMenu;

@SuppressWarnings("MethodParameterOfConcreteClass") // UI View object
public class RandomOutfitPresenterImpl implements RandomOutfitPresenter {
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitActions randomOutfitActions = DependencyManager.getInstance().getImplementationFor(RandomOutfitActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final RandomOutfitActivityMenu randomOutfitActivityMenu = DependencyManager.getInstance().getImplementationFor(RandomOutfitActivityMenu.class);
    @SuppressWarnings("LawOfDemeter")
    private final NoMoreOutfitsToShowDialog noMoreOutfitsToShowDialog = DependencyManager.getInstance().getImplementationFor(NoMoreOutfitsToShowDialog.class);
    @SuppressWarnings("LawOfDemeter")
    private final AdPresenter nextRandomOutfitAdPresenter = DependencyManager.getInstance().getImplementationFor(AdPresenter.class, UiBindingsImpl.NEXT_RANDOM_OUTFIT_AD_PRESENTER);
    @SuppressWarnings("LawOfDemeter")
    private final AdPresenter previousViewedOutfitAdPresenter =
            DependencyManager.getInstance().getImplementationFor(AdPresenter.class, UiBindingsImpl.PREVIOUS_VIEWED_OUTFIT_AD_PRESENTER);

    @SuppressWarnings("LawOfDemeter")
    @Override
    public void presentNextRandomOutfit(@NonNull final Context context, @NonNull final CustomImageSwitcher outfitImageSwitcher, @NonNull final View noOutfitsView) {
        if (randomOutfitActions.getOutfitCount(context) > 0 ) {
            final Optional<Outfit> nextRandomOutfit = randomOutfitActions.getNextRandomOutfit(context);
            updateUiForShowing(OutfitsState.HAS_OUTFITS, outfitImageSwitcher, noOutfitsView);
            nextRandomOutfit.ifPresentOrElse(outfit -> {
                nextRandomOutfitAdPresenter.presentAdWhenApplicable();
                outfitImageSwitcher.setImageURI(outfit.getPhotoUri(context));
            }, () -> {
                noMoreOutfitsToShowDialog.show(context);
                presentNextRandomOutfit(context, outfitImageSwitcher, noOutfitsView);
            });
        } else {
            updateUiForShowing(OutfitsState.HAS_NO_OUTFITS, outfitImageSwitcher, noOutfitsView);
        }
    }

    private void updateUiForShowing(@NonNull final OutfitsState outfitsState, @NonNull final CustomImageSwitcher outfitImageSwitcher, @NonNull final View noOutfitsView) {
        outfitImageSwitcher.setVisibility(outfitsState == OutfitsState.HAS_OUTFITS ? View.VISIBLE : View.GONE);
        noOutfitsView.setVisibility(outfitsState == OutfitsState.HAS_NO_OUTFITS ? View.VISIBLE : View.GONE);
        randomOutfitActivityMenu.setOutfitsState(outfitsState);
    }

    @SuppressWarnings("LawOfDemeter")
    @Override
    public void presentPreviousViewedOutfit(@NonNull final Context context, @NonNull final CustomImageSwitcher outfitImageSwitcher) {
        previousViewedOutfitAdPresenter.presentAdWhenApplicable();
        final Optional<Outfit> previousViewedOutfit = randomOutfitActions.getPreviousViewedOutfit(context);
        previousViewedOutfit.ifPresent(outfit -> outfitImageSwitcher.setImageURI(outfit.getPhotoUri(context)));
    }

}
