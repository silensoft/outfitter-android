package com.silensoft.outfitter.ui.presenters.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.GridView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.silensoft.outfitter.actions.inappbilling.InAppBillingActions;
import com.silensoft.outfitter.dependencymanagement.DependencyManager;
import com.silensoft.outfitter.model.inappbilling.InAppBillingSkuType;
import com.silensoft.outfitter.actions.weatherforecast.WeatherForecastActions;
import com.silensoft.outfitter.model.weatherforecast.ThreeHourWeatherForecast;
import com.silensoft.outfitter.types.Action;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.ui.adapters.WeatherForecastArrayAdapterImpl;
import com.silensoft.outfitter.ui.presenters.WeatherForecastPresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WeatherForecastPresenterImpl implements WeatherForecastPresenter {
    private static final long MILLI_SECS_IN_THREE_HOURS = 3L * 60L * 60L * 1000L;

    @SuppressWarnings("LawOfDemeter")
    private final InAppBillingActions inAppBillingActions = DependencyManager.getInstance().getImplementationFor(InAppBillingActions.class);
    @SuppressWarnings("LawOfDemeter")
    private final WeatherForecastActions weatherForecastActions = DependencyManager.getInstance().getImplementationFor(WeatherForecastActions.class);

    private boolean initialized;
    private Context context;
    private GridView gridView;
    private FirebaseAnalytics firebaseAnalytics;
    private long lastWeatherForecastUpdateTimestampInMilliSecs;
    private List<ThreeHourWeatherForecast> latestThreeHourWeatherForecasts = new ArrayList<>(0);

    public void initialize(@NonNull final Context context_, @NonNull final FirebaseAnalytics firebaseAnalytics_, @NonNull final GridView gridView_) {
        context = context_;
        gridView = gridView_;
        firebaseAnalytics = firebaseAnalytics_;
        initialized = true;
    }

    @Override
    public void hideWeatherForecast() {
        if (initialized) {
            gridView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void presentNewWeatherForecastIfApplicable() {
        if (initialized) {
            final List<InAppBillingSkuType> purchases = new ArrayList<>(Collections.singletonList(InAppBillingSkuType.PRO_SUBSCRIPTION));
            inAppBillingActions.hasPurchasedAnyOf(purchases, OptionalImpl.of(() ->
                    updateWeatherForecastAndThen(this::presentLatestWeatherForecast)), OptionalImpl.empty());
        }
    }

    @Override
    public void presentUpdatedWeatherForecastIfApplicable() {
        if (initialized) {
            if (lastWeatherForecastUpdateTimestampInMilliSecs > 0L) {
                final long currentTimestampInMilliSecs = System.currentTimeMillis();
                if (currentTimestampInMilliSecs - lastWeatherForecastUpdateTimestampInMilliSecs >= MILLI_SECS_IN_THREE_HOURS) {
                    presentNewWeatherForecastIfApplicable();
                } else {
                    presentLatestWeatherForecast();
                }
            }

            presentNewWeatherForecastIfApplicable();
        }
    }

    private void updateWeatherForecastAndThen(@NonNull final Action afterUpdateAction) {
        weatherForecastActions.fetchThreeHourWeatherForecastsForNext15Hours(context, firebaseAnalytics, threeHourWeatherForecasts -> {
            lastWeatherForecastUpdateTimestampInMilliSecs = System.currentTimeMillis();
            latestThreeHourWeatherForecasts = new ArrayList<>(threeHourWeatherForecasts);
            afterUpdateAction.perform();
        });
    }

    @SuppressWarnings("LocalVariableOfConcreteClass")
    private void presentLatestWeatherForecast() {
        final WeatherForecastArrayAdapterImpl weatherForecastArrayAdapter = new WeatherForecastArrayAdapterImpl(context, latestThreeHourWeatherForecasts);
        gridView.setAdapter(weatherForecastArrayAdapter);
        gridView.setVisibility(View.VISIBLE);
    }
}
