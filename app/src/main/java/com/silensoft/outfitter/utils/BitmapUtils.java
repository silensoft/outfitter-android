package com.silensoft.outfitter.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Impl.SizeImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.types.Size;

import java.io.InputStream;

public final class BitmapUtils {
    private BitmapUtils() {}

    public static Optional<Bitmap> scalePhotoBitmapToScreenSize(@NonNull final Context context, @NonNull final String photoPath) {
        final BitmapFactory.Options decodeBoundsOptions = getDecodeBoundsOptions();
        BitmapFactory.decodeFile(photoPath, decodeBoundsOptions);
        return OptionalImpl.ofNullable(BitmapFactory.decodeFile(photoPath, getScaleOptions(context, new SizeImpl(decodeBoundsOptions.outWidth, decodeBoundsOptions.outHeight))));
    }

    public static Bitmap scalePhotoBitmapToScreenSize(@NonNull final Context context, @NonNull final InputStream inputStream) {
        final BitmapFactory.Options decodeBoundsOptions = getDecodeBoundsOptions();
        BitmapFactory.decodeStream(inputStream, null, decodeBoundsOptions);
        return BitmapFactory.decodeStream(inputStream, null, getScaleOptions(context, new SizeImpl(decodeBoundsOptions.outWidth, decodeBoundsOptions.outHeight)));
    }

    @NonNull
    private static BitmapFactory.Options getDecodeBoundsOptions() {
       final  BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
        bitmapFactoryOptions.inJustDecodeBounds = true;
        return bitmapFactoryOptions;
    }

    @SuppressWarnings("FeatureEnvy") // POJO access
    @NonNull
    private static BitmapFactory.Options getScaleOptions(@NonNull final Context context, @NonNull final Size imageSize) {
        final BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
        final Size displaySize = getScreenSize(context);
        bitmapFactoryOptions.inSampleSize = Math.min(imageSize.getWidth() / displaySize.getWidth(), imageSize.getHeight() / displaySize.getHeight());
        return bitmapFactoryOptions;
    }

    @NonNull private static Size getScreenSize(@NonNull final Context context) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return new SizeImpl(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }
}
