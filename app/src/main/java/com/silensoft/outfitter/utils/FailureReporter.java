package com.silensoft.outfitter.utils;


import android.support.annotation.NonNull;

public interface FailureReporter {
    void reportFailure(@NonNull final Throwable throwable);
}
