package com.silensoft.outfitter.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;

import com.silensoft.outfitter.model.persistableimage.PersistableImage;
import com.silensoft.outfitter.types.Impl.OptionalImpl;
import com.silensoft.outfitter.types.Optional;
import com.silensoft.outfitter.utils.impl.FailureReporterImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class FileUtils {
    public static final String FILE_NAME_PREFIX = "JPEG_";
    public static final String FILE_EXTENSION = ".jpg";
    public static final String FILE_PROVIDER_AUTHORITY = "com.silensoft.fileprovider";

    private FileUtils() {
    }

    public static Uri getUri(@NonNull final Context context, @NonNull final String filePath) {
        final File imageFile = new File(filePath);
        return FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY, imageFile);
    }

    public static Optional<File> createTempFile(@NonNull final Context context) {
        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.getDefault()).format(new Date());
        final String fileName = FILE_NAME_PREFIX  + timeStamp + FILE_EXTENSION;
        final File storageDirectory = context.getExternalCacheDir();
        try {
            return OptionalImpl.ofNullable(File.createTempFile(fileName, FILE_EXTENSION, storageDirectory));
        } catch (final IOException exception) {
            new FailureReporterImpl().reportFailure(exception);
            return OptionalImpl.empty();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void deleteFile(final String filePath) {
        final File file = new File(filePath);
        file.delete();
    }

    public static boolean saveImageToFile(@NonNull final PersistableImage persistableImage, @NonNull final File file) {
        try {
            final OutputStream fileOutputStream = new FileOutputStream(file);
            persistableImage.persistImage(fileOutputStream);
            fileOutputStream.close();
            return true;
        } catch (final Exception exception) {
            new FailureReporterImpl().reportFailure(exception);
        }

        return false;
    }

    public static Optional<String> saveImage(@NonNull final PersistableImage persistableImage, @NonNull final String directory) {
        final File storageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + directory);
        final boolean isStorageDirectoryCreationSuccessful = storageDirectory.exists() || storageDirectory.mkdirs();

        Optional<String> savedImagePath = OptionalImpl.empty();
        if (isStorageDirectoryCreationSuccessful) {
            final String imageFileName = FILE_NAME_PREFIX + persistableImage.getUuidStr() + FILE_EXTENSION;
            try {
                final File imageFile = new File(storageDirectory, imageFileName);
                final OutputStream fileOutputStream = new FileOutputStream(imageFile);
                persistableImage.persistImage(fileOutputStream);
                fileOutputStream.close();
                savedImagePath = OptionalImpl.of(imageFile.getAbsolutePath());
            } catch (final Exception exception) {
                new FailureReporterImpl().reportFailure(exception);
            }
        }

        return savedImagePath;
    }

    public static Optional<Bitmap> loadImage(@NonNull final String imageFilePath) {
        return OptionalImpl.ofNullable(BitmapFactory.decodeFile(imageFilePath));
    }

    public static Optional<Bitmap> loadImage(@NonNull final PersistableImage persistableImage, @NonNull final String directory) {
        final File storageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + directory);
        final boolean isStorageDirectoryCreationSuccessful = storageDirectory.exists() || storageDirectory.mkdirs();

        if (isStorageDirectoryCreationSuccessful) {
            final String imageFileName = FILE_NAME_PREFIX + persistableImage.getUuidStr() + FILE_EXTENSION;
            final File imageFile = new File(storageDirectory, imageFileName);
            return OptionalImpl.ofNullable(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
        }

        return OptionalImpl.empty();
    }
}
