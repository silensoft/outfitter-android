package com.silensoft.outfitter.utils;

import android.support.annotation.NonNull;

import java.util.List;

public final class ListUtils {
    @NonNull
    public static boolean[] getHasItemAtIndexArrayFromIndexes(@NonNull final List<Integer> outfitOccasionIndexes) {
        final boolean[] hasItemAtIndex = new boolean[7];
        for (final Integer outfitOccasionIndex : outfitOccasionIndexes) {
            if (outfitOccasionIndex != null && outfitOccasionIndex < 7) {
                hasItemAtIndex[outfitOccasionIndex] = true;
            }
        }
        return hasItemAtIndex;
    }

    private ListUtils() {}
}
