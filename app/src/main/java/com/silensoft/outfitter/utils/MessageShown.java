package com.silensoft.outfitter.utils;

public class MessageShown {
    private boolean isMessageShown;

    public MessageShown() {
        isMessageShown = false;
    }

    public void setMessageShown() {
        isMessageShown = true;
    }

    public boolean isMessageShown() {
        return isMessageShown;
    }
}
