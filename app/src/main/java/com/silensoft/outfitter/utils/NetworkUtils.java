package com.silensoft.outfitter.utils;

import android.content.Context;
import android.support.annotation.NonNull;

public interface NetworkUtils {
    boolean hasInternetConnection(@NonNull final Context context);
}
