package com.silensoft.outfitter.utils.impl;

import android.support.annotation.NonNull;

import com.google.firebase.crash.FirebaseCrash;
import com.silensoft.outfitter.utils.FailureReporter;

import timber.log.Timber;

public class FailureReporterImpl implements FailureReporter {

    @Override
    public void reportFailure(@NonNull final Throwable throwable) {
        Timber.e(throwable);
        FirebaseCrash.report(throwable);
    }
}
