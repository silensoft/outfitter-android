package com.silensoft.outfitter.utils.impl;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.utils.NetworkUtils;

public class NetworkUtilsImpl implements NetworkUtils {
    @SuppressWarnings("LawOfDemeter")
    @Override
    public boolean hasInternetConnection(@NonNull final Context context) {
        final ConnectivityManager connectivityManager =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
