package com.silensoft.outfitter.utils.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import com.silensoft.outfitter.utils.NetworkUtils;

public class NoConnectionNetworkUtilsImpl implements NetworkUtils {
    @Override
    public boolean hasInternetConnection(@NonNull final Context context) {
        return false;
    }
}
