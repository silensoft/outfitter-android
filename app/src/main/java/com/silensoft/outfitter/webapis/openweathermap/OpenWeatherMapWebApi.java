package com.silensoft.outfitter.webapis.openweathermap;

import com.silensoft.outfitter.webapis.openweathermap.model.WeatherForecastData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

@SuppressWarnings("InterfaceNeverImplemented")
public interface OpenWeatherMapWebApi {
    @GET("data/2.5/forecast")
    Call<WeatherForecastData> getWeatherForecastData(@Query("lat") String latitude, @Query("lon") String longitude);
}
