package com.silensoft.outfitter.webapis.openweathermap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id_) {
        this.id = id_;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name_) {
        this.name = name_;
    }

}