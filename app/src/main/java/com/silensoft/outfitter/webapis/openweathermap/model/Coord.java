package com.silensoft.outfitter.webapis.openweathermap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coord {

    @SerializedName("lon")
    @Expose
    private Double lon;

    @SerializedName("lat")
    @Expose
    private Double lat;

    public Double getLon() {
        return lon;
    }

    public void setLon(final Double lon_) {
        lon = lon_;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(final Double lat_) {
        lat = lat_;
    }

}
