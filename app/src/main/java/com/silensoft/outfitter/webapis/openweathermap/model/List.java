package com.silensoft.outfitter.webapis.openweathermap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings({"ReturnOfCollectionOrArrayField", "AssignmentToCollectionOrArrayFieldFromParameter", "MethodReturnOfConcreteClass", "MethodParameterOfConcreteClass"})
public class List {

    @SerializedName("dt")
    @Expose
    private Integer dt;

    @SerializedName("main")
    @Expose
    private Main main;

    @SerializedName("weather")
    @Expose
    private java.util.List<Weather> weather = null;

    @SerializedName("clouds")
    @Expose
    private Clouds clouds;

    @SerializedName("wind")
    @Expose
    private Wind wind;

    @SerializedName("sys")
    @Expose
    private Sys sys;

    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;

    public Integer getDt() {
        return dt;
    }

    public void setDt(final Integer dt_) {
        dt = dt_;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(final Main main_) {
        main = main_;
    }

    public java.util.List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(final java.util.List<Weather> weather_) {
        weather = weather_;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(final Clouds clouds_) {
        clouds = clouds_;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(final Wind wind_) {
        this.wind = wind;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(final Sys sys_) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(final String dtTxt_) {
        dtTxt = dtTxt_;
    }

}
