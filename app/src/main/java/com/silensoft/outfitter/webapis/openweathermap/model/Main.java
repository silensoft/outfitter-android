package com.silensoft.outfitter.webapis.openweathermap.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("temp")
    @Expose
    private Double temp;

    @SerializedName("temp_min")
    @Expose
    private Double tempMin;

    @SerializedName("temp_max")
    @Expose
    private Double tempMax;

    @SerializedName("pressure")
    @Expose
    private Double pressure;

    @SerializedName("sea_level")
    @Expose
    private Double seaLevel;

    @SerializedName("grnd_level")
    @Expose
    private Double grndLevel;

    @SerializedName("humidity")
    @Expose
    private Integer humidity;

    @SerializedName("temp_kf")
    @Expose
    private Double tempKf;

    public Double getTemp() {
        return temp;
    }

    public void setTemp(final Double temp_) {
        temp = temp_;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public void setTempMin(final Double tempMin_) {
        tempMin = tempMin_;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public void setTempMax(final Double tempMax_) {
        tempMax = tempMax_;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(final Double pressure_) {
        pressure = pressure_;
    }

    public Double getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(final Double seaLevel_) {
        seaLevel = seaLevel_;
    }

    public Double getGrndLevel() {
        return grndLevel;
    }

    public void setGrndLevel(final Double grndLevel_) {
        grndLevel = grndLevel_;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(final Integer humidity_) {
        humidity = humidity_;
    }

    public Double getTempKf() {
        return tempKf;
    }

    public void setTempKf(final Double tempKf_) {
        tempKf = tempKf_;
    }

}
