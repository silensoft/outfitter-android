package com.silensoft.outfitter.webapis.openweathermap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("main")
    @Expose
    private String main;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("icon")
    @Expose
    private String icon;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id_) {
        id = id_;
    }

    public String getMain() {
        return main;
    }

    public void setMain(final String main_) {
        main = main_;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description_) {
        description = description_;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(final String icon_) {
        icon = icon_;
    }

}
