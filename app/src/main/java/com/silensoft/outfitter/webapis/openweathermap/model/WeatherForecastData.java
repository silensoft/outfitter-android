package com.silensoft.outfitter.webapis.openweathermap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings({"MethodReturnOfConcreteClass", "MethodParameterOfConcreteClass", "AssignmentToCollectionOrArrayFieldFromParameter", "ReturnOfCollectionOrArrayField"})
public class WeatherForecastData {
    @SerializedName("city")
    @Expose
    private City city;

    @SerializedName("coord")
    @Expose
    private Coord coord;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("cod")
    @Expose
    private String cod;

    @SerializedName("message")
    @Expose
    private Double message;

    @SerializedName("cnt")
    @Expose
    private Integer cnt;

    @SerializedName("list")
    @Expose
    private java.util.List<List> list = null;

    public City getCity() {
        return city;
    }

    public void setCity(final City city_) {
        city = city_;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(final Coord coord) {
        this.coord = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country_) {
        country = country_;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(final String cod_) {
        cod = cod_;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(final Double message_) {
        message = message_;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(final Integer cnt_) {
        cnt = cnt_;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(final java.util.List<List> list_) {
        list = list_;
    }

}

