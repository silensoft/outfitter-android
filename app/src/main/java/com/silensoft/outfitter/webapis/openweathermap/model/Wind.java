package com.silensoft.outfitter.webapis.openweathermap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed")
    @Expose
    private Double speed;

    @SerializedName("deg")
    @Expose
    private Double deg;

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(final Double speed_) {
        speed = speed_;
    }

    public Double getDeg() {
        return deg;
    }

    public void setDeg(final Double deg_) {
        deg = deg_;
    }

}
